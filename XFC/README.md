Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of IIT Madras  nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

**How to execute and interpret input and output**

To execute run the following command:

python xfc_xxxx.py input_file.bcs

e.g. for AES the command will be as following:

python xfc_spn.py spec_aes.bcs

--input_file.txt will contain the specification of the block cipher. 

--fault spn ciphers use xfc_spn.py and for Feistel cipher use xfc_feistel.py

-- output will be:: round - number of keys retrieved - corresponding complexity

e.g. ::

 when fault location is:  ['F32' , 1] i.e. fault is injected in 1st byte of 32th function. 

output: [('38', [1, 2, 3, 4], 256)] 

which means key associated with 38th non linear function is retrieved, key byte [1,2,3,4] are retrieved with complexity 256. 


**system requirement**

python version :  Python 2.7.6 or above

The codes are ran and tested on a ubuntu system with specification as : Intel® Core™ i7-2600 CPU @ 3.40GHz × 8 

If "pyparsing" is not installed in the system install using the command "pip install pyparsing" 

**How to write BCS**

Following is the template for Block Cipher Specification

〈begin〉

	//Add cipher functions
		···
	//〈function〉〈linear/nonlinear〉〈
	〈F2〉〈linear〉〈
		//< subfuction : { Dependant subfuctions separated by comma  } >
		< F2[1] : { F1[1]  } >
		···
	/〉

	··· ··· ···
〈end〉

Notes: 
1) The cipher function is divided into the subfunctions. The size of the subfunction will be the minimum size of non-linear function.

2) Each operation in the block cipher is represented as a fucntion in specification

3) Every function should be written in form of "F"<function index> e.g. First function will be F1

4) Every subfuction should be written in form of "F"<function index>[<subfuction index>] e.g. for fuction F1, it will be like F1[1], F1[2]... 

5) If there is no dependent subfuction add empty space. 

6) Folder **Specifications** contains the BCS of various ciphers. 
