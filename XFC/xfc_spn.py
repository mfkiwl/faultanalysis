"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details: This code takes block cipher specification and particular location as input and outputs the vulnerability of that location. It that location is vulnerable it will give the number of keys retrieved and the corresponding complexity as output. This works for SPN ciphers.
--------------------------------------------------------------------------------------------------
"""






from __future__ import division
import sys
import re
import itertools
from itertools import islice
import time
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from collections import Counter




m = 0
pt = 0
w = 8
UsedColor = ["C0"]
H1=2**1.36
H2=2**1.02
H=2.63
R = set()
K = set()
spec_color = []


#------------------------------------ Storing BCS into func data structure---------------------------
# func stores the specification func[0] -> function , func[1] -> linear/non linear , func[2] -> parts and their dependency

def BlockCipherSpecification():


	flag = 0
	fname = ''
	ftype = ''
	partname = ''
	dep = ''
	temp = ''
	dependency = []
	func_part = []
	func = []
	func_backward = []
	dstart=0
	global m
	global pt
	old_pt = 0

	file1 = open(input_file , "r")

	for line in file1:
		#print "Inside for"
		words= line.split()
		# print words
		for wrd in words: #wrd is every words in specification
			# print "wrd" , wrd
			if wrd == "begin":
				flag=1 # flag=1 means that we found "begin"
				# print flag
				break
			if flag == 1 and wrd not in [">" ,"<", ":" , "{", "}" , "/>"]: ##checking not end of line
				temp = temp + wrd
				# print "temp" , temp
			if wrd in [ ">" , ":" , "}" ]: ##End of line
				if fname == "":			##if function name is not yet found then it means start of function, so store temp in fname
					fname = temp
				elif ftype == "": 		##if function type is not yet found then it means start of function, so store temp in ftype
					ftype = temp
				elif partname == "": #partname
					partname = temp
				elif dep == "" :   #dependency of each part starts
					dstart=1
					dep = temp

				temp = ''

				if wrd == "}":
					d=dep.split(",")
					for i in d:
						dependency.append(i)

				if  wrd == ">" and flag ==1  and dstart ==1:
					func_part.append((partname, list(dependency))) ##Stores each part details of each round
					old_pt = old_pt +1
					dstart=0
					partname=''
					dep = ''
					del dependency[:]

			if wrd == "/>":
				func.append((fname, ftype, list(func_part))) ## Stores one whole function and adds
				m=m+1
				pt= old_pt
				old_pt = 0
				fname=''
				ftype=''
				del func_part[:]

			if wrd == "end":
				flag=0
				break
	file1.close()
	#print "m = ",m
	#print "pt = ",pt
	# print "old_pt = ",old_pt
	return func





#--------------------------------------------Fault Propagation----------------------------

# clear all colors i.e. set everything to "C0"
def ResetColor(color):
	for i in range(1,m+1):
		for j in range(1,pt+1):

			ptt="F"+str(i)+"["+str(j)+"]"

			color[ptt] = "C0"
			ptt = ''


# Add newcolor to fault location/ function value

def NewColor( UsedColor) :
	maxindx = 0
	for i, val in enumerate(UsedColor):
	 	s = val
	 	exp = re.search('\d+', s).group()
		num = int(exp)
		if num > maxindx:
			maxindx=num
	maxindx+=1
	nc = "C" + str(maxindx)
	UsedColor.append(nc)
	return nc

# Count number of dependent colors

def CountColor ( var ,color ):
	Zg = []
	c = len( var[1])
	#print c
	for x in range(1, c+1):

		temp = var[1][x-1]
		if var[1][0] != "":

			if color[temp] not in Zg and color[temp] != "C0":
				Zg.append(color[temp])

	return Zg



def FaultPropagation ( func , fault_list) :

	# print "----------------Fault Propagation--------------"

	color = {}
	ResetColor(color)
	# print faultfunc
	UsedColor = ["C0"]
	for aa in fault_list:  # in case of multiple encryption  iterate over list of faults in different encryption ( for XFC its 1 )
		ResetColor(color)
		minnum = []
		# print "Encryption: ", aa
		for bb in aa: #Iterate over all the fault location in a particular encryption (for XFC its 1)
			faultfunc = bb[0]
			faultpart = bb[1]
			# print "faultfunc" , faultfunc
			# print "faultpart" , faultpart
			faultinduced = faultfunc +"[" +faultpart +"]"
			# print "faultinduced" ,faultinduced
			if faultinduced in color:
				del color[faultinduced]
				color[faultinduced] = NewColor(UsedColor)
				# print color[faultinduced]
			# pnum = num
			exp = re.search('\d+', faultfunc).group()
			num = int(exp)
			minnum.append(num)


		#gives the location which is farthest from the ciphertext and fault affected, we start fault propagation from here
		num = min(minnum)
		for x in range(num,m+1):
			fn = "F"+ str(x)

			# If function is nonlinear assign new color to the output
			if func[x-1][1] == "nonlinear":
				for j in range(1,pt+1):
					if color[func[x-1][2][j-1][0]] != "C0":
						continue
					t=func[x-1][2][j-1][1][0]
					if t != "":
						if color[t] != "C0":
							curpt =func[x-1][2][j-1][0]
							del color[curpt]
							color[curpt] = NewColor(UsedColor)
							# print curpt , "   ", color[curpt]

			#If the function is linear assign colors based on input colors
			if func[x-1][1] == "linear":
				for j in range(1,pt+1):
					if color[func[x-1][2][j-1][0]] != "C0":
						continue
					Zg = CountColor (func[x-1][2][j-1], color)
					cnum = len(Zg)
					if cnum > 1:
						curpt = func[x-1][2][j-1][0]
						del color[curpt]
						color[curpt] = NewColor(UsedColor)
						# print curpt , "   ", color[curpt]
					elif cnum ==1 :
						curpt = func[x-1][2][j-1][0]
						del color[curpt]
						color[curpt] = Zg[0]
						# print curpt , "   ", color[curpt]

		# color_func will store the functions which are colored. "This is not required in XFC"
		color_func = set()
		for x in xrange(1,m+1):
			for y in xrange(1,pt+1):
				if color[func[x-1][2][y-1][0]] != "C0":
					color_func.add(func[x-1][2][y-1][0])


		# In case of XFC spec_color will contain only one dictionary. It will contain multiple values in case of multiple encryption
		spec_color.append(dict(color))

	return color_func




#-------------------------------------------------Key Determination----------------------------------


def check_c2(var, x, y):
	# print var[0]
	i=x
	flag=0
	flag_match=0
	func_dep = []
	temp_func=[]
	func_dep.append(var[0])
	while flag != 1:
		for a in func_dep:
			for b in xrange(1,pt+1):
				if len(func[i][2][b-1][1]) > 2 or i==m-1:
					flag=1
				for c in xrange(1,len(func[i][2][b-1][1])+1):
					if a == func[i][2][b-1][1][c-1]:
						temp_func.append(func[i][2][b-1][0])
						flag_match=1
						break


		del func_dep[:]
		func_dep= temp_func[:]
		del temp_func[:]

		i=i+1



	return func_dep

def find_min_fault_loc(flt , fault_list):
	lst = []
	for bb in fault_list:
		exp = re.search('\d+', bb[0]).group()
		num = int(exp)
		lst.append(num)
	return min(lst)


def DetermineKeyParts (color , fault_list) :

	# print "----------------Determine Keys--------------"

	E = {}  #Stores all colors which are already estimated.
	E = set()
	r_count = dict()
	key_back =1
	unknown_spec = dict()
	used_spec = set()
	index = 0
	derived = []
	theta = {}
	theta["C0"] =1
	# Initialise E with all colors in ciphertext.
	for ss in xrange(1,len(spec_color)+1):
		#
		for x in xrange(1,pt+1):
			prt = "F" + str(m) + "[" + str(x) + "]"
			# print "spec_color[ss][prt]" , spec_color[ss-1][prt]
			theta[spec_color[ss-1][prt]] = 1
			if spec_color[ss-1][prt] != "C0":
				E.add(spec_color[ss-1][prt])
	# print E
	# Start iterating from ciphertext towards the plaintext
	for x in xrange(m, 0, -1):

		#  If the function is linear then continue
		if func[x-1][1] == "linear":
			continue

		R.clear()
		K.clear()
		unknown_spec.clear()
		r_count.clear()
		for y in xrange(1,pt+1):

			prt_in = func[x-1][2][y-1][0] #this is x in algorithm (input)
			# print "Printing details of x:",  prt_in

			prt_out = func[x-1][2][y-1][1][0] #this is y in algorithm (output)
			# print "Printing details of y:",  prt_out

			if prt_in == "":
				continue

			spec_multterm = []
 			prod_term = []
			for ss in xrange(1,len(spec_color)+1):
				if ss-1 not in unknown_spec:
					unknown_spec[ss-1] = set()
 				tem_theta=1
				C1=0
				C2=0

				# If the input color is already estimated i.e in E
				if spec_color[ss-1][prt_in] in E  :
					C1=1

				func_dep = check_c2(func[x-1][2][y-1], x, y)
				color_dep = []
				for a in func_dep:
					color_dep.append(spec_color[ss-1][a])

				# If the color of input can be expressed in terms of already estimated colors.
				if set(color_dep) < set(E) and len(color_dep) > 0:
					C2=1


				if C1 != 1 and C2 !=1:
	 				continue

				elif C1!=1 and C2 ==1 and spec_color[ss-1][prt_in] != "C0":
					tem_theta = 1
					for xx in color_dep:
						tem_theta = tem_theta * theta[xx]
					prod_term.append((ss, tem_theta ))

				else:
					tem_theta = tem_theta * theta[spec_color[ss-1][prt_in]]

				#multterm is theta k in algo
				if spec_color[ss-1][prt_in] != "C0" and spec_color[ss-1][prt_out] != "C0":
					ft = 2**w
					st= 1
					st = st * tem_theta
					st = st * H

					if spec_color[ss-1][prt_out] not in E:
						delta = 2**w
						st = st * delta
						unknown_spec[ss-1].add(spec_color[ss-1][prt_out])

					elif spec_color[ss-1][prt_out] in unknown_spec[ss-1] :
						st = st * 2**w
					else:
						st = st * theta[spec_color[ss-1][prt_out]]

					multterm = st
					spec_multterm.append((ss-1, multterm ))

			if len(spec_multterm)!= 0 :
				mt = spec_multterm[0]
				for xx in spec_multterm:
					if xx[1] == mt[1] and xx[0] != mt[0]:
						# print "inside if checking"
						if find_min_fault_loc(xx[0] , fault_list[xx[0]]) < find_min_fault_loc(mt[0] , fault_list[mt[0]]):
							mt=xx

					elif xx[1] < mt[1]:
						mt=xx

				min_term = mt
				index = min_term[0]
				for sss in xrange(1,len(spec_color)+1):
					if spec_color[sss-1][prt_out] != "C0":
						theta[spec_color[sss-1][prt_out]] = min_term[1]
						E.add(spec_color[sss-1][prt_out])
						if y not in K:
							K.add(y)
							used_spec.add(index)



							R.add(spec_color[index][prt_out])
							if spec_color[index][prt_out] not in r_count:
								r_count[spec_color[index][prt_out]] = 1
							elif y in K:
								r_count[spec_color[index][prt_out]] = r_count[spec_color[index][prt_out]] +1

		max_rcount = 0
		if r_count:

			for xx in r_count:
				if r_count[xx] > max_rcount:
					max_rcount = r_count[xx]
		t = "K" + str(x)
		q = 1
		for p in R:
			q = q * theta[p]
		ln=0

		for xx in used_spec:
			rcount_flag = 0
			for rc in unknown_spec[xx]:
				if rc in r_count and  r_count[rc] >= max_rcount:
					rcount_flag=1

			if rcount_flag==1:

				ln = ln+len(unknown_spec[xx])

		theta[t] =  min (q , 256**ln)


		K_l = list(K)
		brute_force = 2**w
		brute_force = brute_force**len(K_l)
		midvar = 0
		for term in derived:
			if term[0] == str(x+4) and theta[t] < brute_force:
				midvar = float(theta[t]) / float(256**max_rcount)
				if key_back == 1:
					temp_term1 = term[0]
					temp_term2= term[1]
					temp_term3 =  int(term[2] * midvar)
					derived.append((temp_term1, temp_term2, temp_term3))
					derived.remove(term)
					break


		if K and theta[t] < brute_force:

			derived.append((str(x), K_l, theta[t]))

		else:
			break

	return derived




#--------------------------------Main-------------------------------



input_file = sys.argv[1]
func = []
fault_model = []
fm = dict()
fm_color = list()
eq_set = dict()
func = BlockCipherSpecification()

# Stores single faults
sf = []
# Stores multiple faults
mf =[]

sf.append(("F32", "1"))
mf.append(sf)

color_func = FaultPropagation ( func , mf)
derived = DetermineKeyParts ( spec_color, mf )
print "Derived ---------------------------" , derived
