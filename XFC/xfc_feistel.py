"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details: This code takes block cipher specification and particular location as input and outputs the vulnerability of that location. It that location is vulnerable it will give the number of keys retrieved and the corresponding complexity as output. This works for SPN ciphers.

--------------------------------------------------------------------------------------------------
"""







from __future__ import division
import sys
import re
import itertools
from itertools import islice
import time
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from collections import Counter




m = 0
pt = 0
w = 8
UsedColor = ["C0"]
H1=2**1.36
H2=2**1.02
H=1
R = set()
K = set()
spec_color = []
paths = 2


#------------------------------------ Storing BCS into func data structure---------------------------
# func stores the specification func[0] -> function , func[1] -> linear/non linear , func[2] -> parts and their dependency

def BlockCipherSpecification():


	flag = 0
	fname = ''
	ftype = ''
	partname = ''
	dep = ''
	temp = ''
	dependency = []
	func_part = []
	func = []
	func_backward = []
	dstart=0
	global m
	global pt
	old_pt = 0

	file1 = open(input_file , "r")

	for line in file1:
		#print "Inside for"
		words= line.split()
		# print words
		for wrd in words: #wrd is every words in specification
			# print "wrd" , wrd
			if wrd == "begin":
				flag=1 # flag=1 means that we found "begin"
				# print flag
				break
			if flag == 1 and wrd not in [">" ,"<", ":" , "{", "}" , "/>"]: ##checking not end of line
				temp = temp + wrd
				# print "temp" , temp
			if wrd in [ ">" , ":" , "}" ]: ##End of line
				if fname == "":			##if function name is not yet found then it means start of function, so store temp in fname
					fname = temp
				elif ftype == "": 		##if function type is not yet found then it means start of function, so store temp in ftype
					ftype = temp
				elif partname == "": #partname
					partname = temp
				elif dep == "" :   #dependency of each part starts
					dstart=1
					dep = temp

				temp = ''

				if wrd == "}":
					d=dep.split(",")
					for i in d:
						dependency.append(i)

				if  wrd == ">" and flag ==1  and dstart ==1:
					func_part.append((partname, list(dependency))) ##Stores each part details of each round
					old_pt = old_pt +1
					dstart=0
					partname=''
					dep = ''
					del dependency[:]

			if wrd == "/>":
				func.append((fname, ftype, list(func_part))) ## Stores one whole function and adds
				m=m+1
				pt= old_pt
				old_pt = 0
				fname=''
				ftype=''
				del func_part[:]

			if wrd == "end":
				flag=0
				break
	file1.close()
	print m
	return func





#--------------------------------------------Fault Propagation----------------------------

def ResetColor(color):
	for i in range(1,m+1):
		for j in range(1,pt+1):

			ptt="F"+str(i)+"["+str(j)+"]"

			color[ptt] = "C0"
			ptt = ''



def NewColor( UsedColor) :
	maxindx = 0
	for i, val in enumerate(UsedColor):
	 	s = val
	 	exp = re.search('\d+', s).group()
		num = int(exp)
		if num > maxindx:
			maxindx=num
	maxindx+=1
	nc = "C" + str(maxindx)
	UsedColor.append(nc)
	return nc


def CountColor ( var ,color ):
	Zg = []
	c = len( var[1])
	#print c
	for x in range(1, c+1):

		temp = var[1][x-1]
		if var[1][0] != "":

			if color[temp] not in Zg and color[temp] != "C0":
				Zg.append(color[temp])

	return len(Zg)



def FaultPropagation ( func , fault_list) :

	# print "----------------Fault Propagation--------------"

	color = {}
	ResetColor(color)
	# print faultfunc
	UsedColor = ["C0"]
	for aa in fault_list:
		ResetColor(color)

		minnum = []
		for bb in aa:
			faultfunc = bb[0]
			faultpart = bb[1]
			# print "faultfunc" , faultfunc
			# print "faultpart" , faultpart
			faultinduced = faultfunc +"[" +faultpart +"]"
			if faultinduced in color:
				del color[faultinduced]
				color[faultinduced] = NewColor(UsedColor)
			exp = re.search('\d+', faultfunc).group()
			num = int(exp)
			minnum.append(num)


		num = min(minnum)
		for x in range(num,m+1):
			fn = "F"+ str(x)

			if func[x-1][1] == "nonlinear":
				for j in range(1,pt+1):
					if color[func[x-1][2][j-1][0]] != "C0":
						continue

					t=func[x-1][2][j-1][1][0]

					if t != "":
						if color[t] != "C0":
							curpt =func[x-1][2][j-1][0]

							del color[curpt]
							color[curpt] = NewColor(UsedColor)
							# print func[x-1][2][j-1][0]
							# print curpt , "   ", color[curpt]


			if func[x-1][1] == "linear":

				for j in range(1,pt+1):
					# print func[x-1][2][j-1]
					if color[func[x-1][2][j-1][0]] != "C0":
						continue
					cnum = CountColor (func[x-1][2][j-1], color)

					if cnum > 1:
						# curpt = fn + "[" + str(j) + "]"
						curpt = func[x-1][2][j-1][0]
						del color[curpt]
						color[curpt] = NewColor(UsedColor)
						# print func[x-1][2][j-1][0]
						# print curpt , "   ", color[curpt]

					elif cnum ==1 :

						c = len( func[x-1][2][j-1][1])
						#print c
						for cl in range(1, c+1):
							temp = func[x-1][2][j-1][1][cl-1]

							if color[temp] != "C0":
								nc = color[temp]


						# curpt = fn + "[" + str(j) + "]"
						curpt = func[x-1][2][j-1][0]

						del color[curpt]
						color[curpt] = nc
						# print func[x-1][2][j-1][0]
						# print curpt , "   ", color[curpt]


		color_func = set()
		# print "start "
		for x in xrange(1,m+1):
			for y in xrange(1,pt+1):
				# print func[x-1][2][y-1][0]
				if color[func[x-1][2][y-1][0]] != "C0":
					color_func.add(func[x-1][2][y-1][0])


		spec_color.append(dict(color))
	return color_func



#-------------------------------------------------Key Determination----------------------------------

# This determines input is linear function of which functions

def CheckC2( x , l_dep , r_dep ):
	ivar = x

	l_flag=0
	r_flag=0
	while (ivar % 4 ) != 1:
		l_flag=0
		for xx in l_dep:
			for yy in xrange(1,pt+1):
				var = func[ivar-1][2][yy-1]
				c = len( var[1])
				if var[1][0] != "" and var[0] == xx:
					for i in range(1, c+1):
						r_dep.append(var[1][i-1])
						l_dep.remove(xx)
						l_flag=1
		if l_flag==1:
			ivar= ivar-1
			continue
		for xx in r_dep:
			for yy in xrange(1,pt+1):
					var = func[ivar-1][2][yy-1]
					c = len( var[1])
					for i in range(1, c+1):
						if var[1][i-1] == xx:
							l_dep.append(var[0])
							r_dep.remove(xx)
		ivar= ivar-1
	if len(l_dep) != 0:
		r_dep = l_dep[:]
		del l_dep[:]
	match=0
	break_flag=0
	ivar= x-1
	tem_l_dep =[]
	old_r_dep = []
	old_l_dep=[]
	f_type_l=  "linear"
	f_type_r=  "linear"
	while  (f_type_l != "nonlinear" or f_type_r != "nonlinear") and break_flag!=1:
		match=0
		break_flag=0
		old_r_dep = r_dep[:]
		old_l_dep= l_dep[:]
		for xx in r_dep:
			ivar = x
			while ivar <= x+16 and ivar <= m:
				for yy in xrange(1,pt+1):
					var = func[ivar-1][2][yy-1]
					c = len( var[1])
					l_flag=0
					if var[1][0] != "" :
						for i in range(1, c+1):
							if var[1][i-1] == xx :
								match =1
								f_type_l=  func[ivar-1][1]
								if c > 1:
									l_flag=1
								r_dep.append(var[0])
								continue
							tem_l_dep.append(var[1][i-1])
					if l_flag==1:
						l_dep.extend(tem_l_dep)
					del tem_l_dep[:]
				ivar=ivar+1
			if match==1:
				r_dep.remove(xx)
		del tem_l_dep[:]
		match=0
		for xx in l_dep:
			ivar= x
			while ivar < x+16 and ivar <= m:
				for yy in xrange(1,pt+1):
					var = func[ivar-1][2][yy-1]
					c = len( var[1])
					l_flag=0
					if var[1][0] != "" and var[0]==xx:
							f_type_r=  func[ivar-1][1]
							if func[ivar-1][1] == "nonlinear":
								break
							match=1

							l_dep.remove(xx)
							for i in range(1, c+1):
								tem_l_dep.append(var[1][i-1])
					if len(tem_l_dep) != 0:
						l_dep.extend(tem_l_dep)
						del tem_l_dep[:]
				ivar=ivar+1
		if old_l_dep == l_dep and old_r_dep== r_dep:
			break_flag=1
	return l_dep , r_dep


# determines output is liner fuction of which functions (Only for feistel, not required for spn)
def FindOutputFunc(prt_out,x):
	funcelement = {}
	funcelement = set()
	flag_stop=0
	xx=x
	output = []
	new_list = []
	output.append(prt_out)
	exp1 = re.search('\d+', prt_out).group()
	ov = int(exp1)

	while flag_stop != 1 :

		for s in output:
			for yy in xrange(1,pt+1):
				var= func[xx][2][yy-1]
				c= len(var[1])
				for i in xrange(1,c+1):
					temp = var[1][i-1]
					if temp != "":
						exp = re.search('\d+', temp).group()
						nv = int(exp)
						if nv < ov:
							flag_stop=1
						if temp == s:
							new_list.append(var[0])

		del output[:]
		output = new_list [:]
		del new_list[:]
		xx= xx+1
	funcelement = set(output)


	for s in output:
		for  yy in xrange(1,pt+1):
			var= func[xx-1][2][yy-1]
			if var[0] == s:
				c= len(var[1])
				for i in xrange(1,c+1):
					temp = var[1][i-1]
					exp = re.search('\d+', temp).group()
					cv = int(exp)
					if cv != xx-1:
						funcelement.add(temp)
	return funcelement


def find_min_fault_loc(flt , fault_list):
	lst = []
	for bb in fault_list:
		exp = re.search('\d+', bb[0]).group()
		num = int(exp)
		lst.append(num)
	return min(lst)


def DetermineKeyParts (color , fault_list) :

	# print "----------------Determine Keys--------------"
	E = {}
	E = set()
	r_count = dict()
	key_back =1
	unknown_spec = dict()
	used_spec = set()
	index = 0
	derived = []
	theta = {}
	theta["C0"] =1
	for ss in xrange(1,len(spec_color)+1):

		p = paths
		ct = m
		while p :
			for x in xrange(1,pt+1):
				prt = "F" + str(ct) + "[" + str(x) + "]"
				theta[spec_color[ss-1][prt]] = 1
				if spec_color[ss-1][prt] != "C0":
					E.add(spec_color[ss-1][prt])
			ct =ct-1;
			p = p -1;


	for x in xrange(m, 0, -1):

		if func[x-1][1] == "linear":
			continue


		set_derived = 0
		if x >= m-(4*paths): #last round
			set_derived = 1
		for dd in derived:
			if x+4 == dd[0] or x+8 == dd[0]:
				set_derived = 1
		if set_derived == 0:
			continue

		R.clear()
		K.clear()
		unknown_spec.clear()
		r_count.clear()
		for y in xrange(1,pt+1):

			prt_out = func[x-1][2][y-1][0] #this is y in algorithm
			# print "Printing details of y:",  prt_out

			prt_in = func[x-1][2][y-1][1][0] #this is x in algorithm
			# print "Printing details of x:",  prt_in

			if prt_in == "":
				continue

			spec_multterm = []
 			prod_term = []
			for ss in xrange(1,len(spec_color)+1):
				if ss-1 not in unknown_spec:
					unknown_spec[ss-1] = set()
 				tem_theta=1
				C1=0
				C2=0
				# print "Input Color: ", spec_color[ss-1][prt_in]
				# print "Output Color: ", spec_color[ss-1][prt_out]
				if spec_color[ss-1][prt_in] in E  :
					C1=1

				l_dep = []
				r_dep =[]

				l_dep.append(prt_in)
				l_dep , r_dep = CheckC2(x, l_dep ,r_dep)
				color_dep = []
				for sss in xrange(0,len(spec_color))  :

					del color_dep[:]
					for xx in r_dep:
						color_dep.append(spec_color[sss][xx])
					for xx in l_dep:
						color_dep.append(spec_color[sss][xx])
					if set(color_dep) < set(E) and len(color_dep) > 0:
						C2=1


				if C1 != 1 and C2 !=1:
	 				continue


				elif C1!=1 and C2 ==1 and spec_color[ss-1][prt_in] != "C0":
					tem_theta = 1
					for xx in color_dep:
						tem_theta = tem_theta * theta[xx]
					prod_term.append((ss, tem_theta ))
				else:
					tem_theta = tem_theta * theta[spec_color[ss-1][prt_in]]

					#multterm is theta k in algo

				if spec_color[ss-1][prt_in] != "C0" and spec_color[ss-1][prt_out] != "C0":
					ft = 2**w
					st= 1
					st = st * tem_theta

					if y%2==0:
						st = st * H1
					else:
						st = st * H2

					theta_y = set()
					funcelement = FindOutputFunc(prt_out, x)
					if spec_color[ss-1][prt_out] not in E:
						flen = len(funcelement)
						for ii in xrange(1, flen+1):
							funcname = funcelement.pop()
							theta_y.add(spec_color[ss-1][funcname])
	 					for  c in theta_y:
	 						if (c  not in E and c!="C0"):
	 							delta = 2**w
	 							theta[c] =  delta
	 							unknown_spec[ss-1].add(c)

					else:

						st = st * theta[spec_color[ss-1][prt_out]]
 					for s in theta_y:
 						st= st * theta[s]
					multterm = st
					spec_multterm.append((ss-1, multterm ))
			if len(spec_multterm)!= 0 :
				mt = spec_multterm[0]
				for xx in spec_multterm:
					if xx[1] == mt[1] and xx[0] != mt[0]:
						if find_min_fault_loc(xx[0] , fault_list[xx[0]]) < find_min_fault_loc(mt[0] , fault_list[mt[0]]):
							mt=xx

					elif xx[1] < mt[1]:
						mt=xx
				min_term = mt
				index = min_term[0]
				for sss in xrange(1,len(spec_color)+1):
					if spec_color[sss-1][prt_out] != "C0":
						if C1 != 1 and C2 ==1:
							theta[spec_color[sss-1][prt_in]] =prod_term[index][1]
							E.add(spec_color[sss-1][prt_in])
						theta[spec_color[sss-1][prt_out]] = min_term[1]
						E.add(spec_color[sss-1][prt_out])
						if y not in K:
							K.add(y)



							R.add(spec_color[index][prt_out])
		t = "K" + str(x)
		q = 1
		for p in R:
			q = q * theta[p]
		K_l = list(K)
		brute_force = 2**w
		brute_force = brute_force**len(K_l)
		midvar = 0
 		ulen= len(unknown_spec[index])
 		ft = 2**w
 		ft = ft**ulen
 		if ulen ==0:
 			theta[t] =  q
 		else:
 			theta[t] = min(ft, q)


 		K_l = list(K)
 		if K and theta[t] < brute_force :
 			complexity = round(theta[t], 5)
 			derived.append((str(x), K_l, complexity))
	return derived








#--------------------------------Main-------------------------------

input_file = sys.argv[1]
func = []
fault_model = []
fm = dict()
fm_color = list()
eq_set = dict()
func = BlockCipherSpecification()

# Stores single faults
sf = []
# Stores multiple faults
mf =[]

sf.append(("F119", "1"))
mf.append(sf)

color_func = FaultPropagation ( func , mf)
derived = DetermineKeyParts ( spec_color, mf )
print "Derived ---------------------------" , derived
