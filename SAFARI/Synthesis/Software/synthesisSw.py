"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:

This code synthesise BCS to C
--------------------------------------------------------------------------------------------------
"""



import store_operands as so
import re

part_size = 8 #usually byte
parity_bits = 2 ## 1 parity bit for how many bits
symbol_dict = { "RS" : ">>" , "LS" : "<<" , "XOR" : "^" , "MUL" : "*" , "AND" : "&" , "OR" : "|"  }
# bits_dict = { 1 : "127:120" , 2 : "119:112" , 3: "111:104" , 4:"103:96" , 5: "95:88" , 6: "87:80" , 7:"79:72", 8:"71:64" , 9:"63:56" , 10: "55:48" , 11:"47:40" , 12:"39:32" , 13: "31:24" , 14: "23:16" , 15: "15:8" , 16:"7:0" }
parity_funcs = set()

############################################ Add extra declared function #######################################
############################################ begin #######################################
def processVar(n):
    # print >> f2 , "\nn" , n
    m = re.findall(r"'(.*?)'", n)
    o = re.search('(.+)_(.+)\w+' , n)
    # print >> f2 , "\no, " , o
    if m:
        # print >> f2 , m[0]
        return ( m[0])
    elif o:
        byt = o.group(1)
        p = map(int, re.findall(r'\d+', n))
        bt = str(p[0])
        # print >> f2 , "\n Matching", o.group(1), o.group(2)
        ret_string = "(" + byt + " >> " + bt + " & 1 )"
        # print >> f2 , ret_string
        return ret_string
    else:
        return n

def opHandler_func(f , tv , sv ,l):

    # print >> f2 , "f" , f
    # print >> f2 , f
    global last_pop
    if f in ('XOR' , 'RS' , 'LS' , 'MUL' , 'AND', 'OR'  ):
        a = tv.pop()
        b = tv.pop()
        c= tv.pop()
        sv.append(c)
        last_pop = c
        # print >> f2 , c ,"=" ,  processVar(b) , "^" , processVar(a) , ";"
        l.append(c + " = " +  processVar(b) + symbol_dict[f] + processVar(a) + ";")

    elif f in ('LKUP'):
        a = tv.pop()
        b = tv.pop()
        c = tv.pop()
        sv.append(c)
        last_pop = c
        l.append(c+ " = " + processVar(a) + "[" + processVar(b) + "]" + ";")

    elif f in so.new_function:

        istring = f + "("
        for indx in xrange(0, len(so.new_function[f])+1 ):
            if indx != len(so.new_function[f]):

                a = tv.pop()
                if indx != len(so.new_function[f])-1:
                    istring = istring + a + ","
                else:
                    istring = istring + a
            else:
                a = tv.pop()
                istring = a + "=" + istring + ") ;"
                last_pop = a
                sv.append(a)
        l.append(istring)
    else:
        # print >> f2 , "Inside.."
        istring = f + "("
        a = tv.pop()
        istring = istring + a
        b = tv.pop()
        istring = b + "=" + istring + ") ;"
        last_pop = a
        sv.append(a)
        l.append(istring)
        # for indx in xrange(0, len(so.new_function[f])+1 ):
        #     if indx != len(so.new_function[f]):
        #
        #         a = tv.pop()
        #         if indx != len(so.new_function[f])-1:
        #             istring = istring + a + ","
        #         else:
        #             istring = istring + a
        #     else:
        #         a = tv.pop()
        #         istring = a + "=" + istring + ") ;"
        #         last_pop = a
        #         sv.append(a)
        # l.append(istring)
        #
        f2.close()



def addNewFunc():
    f2 = open("./out/output.c" , 'a')
    for f in so.function_depstack:
        defination = "uint8_t "+ str(f) + "( "
        for v in xrange(0, len(so.new_function[f])):
            if v == len(so.new_function[f]) -1:
                defination = defination + "uint8_t " + so.new_function[f][v]
            else:
                defination = defination + "uint8_t " + so.new_function[f][v] + " ,"
        defination = defination + " )"
        print >> f2 , defination, "\n{"

        # print >> f2 , so.function_depstack[f]
        temp_varholder =[]
        stored_val = []
        lines = []
        for j in so.function_depstack[f]:
            # print >> f2 , "tuple ", j
            if j in so.defined_func or j in so.new_defined_func:
                opHandler_func(j,temp_varholder, stored_val, lines)
            else:
                # nz = processForOriginal(z)
                temp_varholder.append(j)

        s_var = ""
        for x in xrange(0, len(stored_val)):
            if x == len(stored_val) -1:
                s_var = s_var + stored_val[x]
            else:
                s_var = s_var + stored_val[x] + ","
        print >> f2 , "\tuint8_t" , s_var , " ;"
        for y in lines:
            print >> f2 , "\t", y
        if f == "ECMP":
            # print >> f2 , "compare" , last_pop,
            print >> f2 , "\tif(" , last_pop , "!=" , "0) exit(0);"
            print >> f2 , "\telse return 0;"
        else:
            print >> f2 , "\treturn" , last_pop , ";"
        print >> f2 , "}\n\n"
    # print >> f2 , so.function_depstack
    # print >> f2 , so.new_function
    f2.close()

############################################ end #######################################

############################################ Add round function #######################################

# ### Handle the operators and form expressions
def opHandler (var , var_list,func ):
    f2 = open("./out/output.c" , 'a')
    # print >> f2 , "ys" , ys
    # print >> f2 , func
    # print >> f2 , var_list
    if str(var) in symbol_dict.keys():


        a = var_list.pop()
        b = var_list.pop()
        # print >> f2 , "a" , a
        # print >> f2 , "b" , b
        n = map(int, re.findall(r'\d+', a))
        # print >> f2 , "n " ,n
        newstr = a + symbol_dict[str(var)] + b
        # print >> f2 , newstr
        var_list.append(newstr)
    elif str(var) in ('LKUP')  :
        # print >> f2 , func
    	a = var_list.pop()
        b = var_list.pop()
        # print >> f2 , "a ", a
        # print >> f2 , "b ", b
        if func == "KEYXOR":
            newstr = a + "[" + b + "]"
            var_list.append(newstr)
        elif func in ("SUBBYTE"):

            newstr = a + "[" + b + "]"
            # print >> f2 , newstr
            var_list.append(newstr)
        elif func in ("PREDICTPARITY"):
            n = map(int, re.findall(r'\d+', str(b)))
            ln = len(n)
            if ln==2 and a.startswith('PS') and parity_bits!=8:
                 if b.startswith("F") :
                    div = n[1]/ (part_size/parity_bits)
                    rem = n[1] % (part_size/parity_bits)
                    # print >> f2 , div , rem
                    newstr = a + "[" + "F" + str(n[0]) + "[" + str(div) + "]][" + str(rem) + "]"
                    # print >> f2 , "...", newstr
            else:
                newstr =  str(a) +"[" + str(b) + "]"
            var_list.append(newstr)
        else:
            newstr =  str(a) +"[" + str(b) + "]"
            var_list.append(newstr)

    else:
        # print >> f2 , so.new_function[var]
        # print >> f2 , "inside ophandler", so.new_function[var]
        ln = len(so.new_function[var])
        newstr = var + "( "
        tvar =[]
        for i in xrange (0, ln):

            a = var_list.pop()
            tvar.append(a)
            # if i == ln -1:
            #     newstr = newstr + a
            # else:
            #     newstr = newstr + a + ","
        tvar.reverse()
        newstr = newstr +  ', '.join(tvar)
        newstr = newstr + " )"
        var_list.append(newstr)
    f2.close()

########## This function process the variables i.e. tuples of subfuction based on the type of the fuction #####
def processForOriginal(num , fn):
    f2 = open("./out/output.c" , 'a')
    n = map(int, re.findall(r'\d+', num))
    ln = len(n)
    # If the fuction is normal round function
    if fn not in ("FINDPARITY", "CMP", "PREDICTPARITY"  ): # ****************
        # if the variable is only a digit. Return as it is..
        if num.isdigit() :
            c_var = str(n[0]-1) #check if we need minus 1
        # if any fuction variable
        elif ln == 2 and num.startswith("F"):
            c_var = "F"+str(int(n[0])) + "[" + str(n[1]-1) + "]"
        # elif ln == 4: #yet to write
        #     pass
        else:
            c_var = num
    # If the fuction is find parity
    elif fn in ("FINDPARITY" ): # ****************
        # if normal round fuction, single bit parity
        if ln == 2 and num.startswith("F"):
            c_var = c_var = "F"+str(int(n[0])) + "[" + str(int(n[1]-1)) + "]"
        # If multiple parity
        # elif ln ==3:
        #     num_bits = int(n[2])-int(n[1]) +1
        #     mask = (2**num_bits)-1
        #     c_var = c_var = "(" + "F"+str(int(n[0])) + "[" + str(int(n[1]-1)) + "]" + ">>" + str(n[2]) + ") & " + str(mask)
        elif ln == 4 :
            # print >> f2 , "Printing n" ,n
            num_bits = int(n[3])-int(n[2]) +1
            mask = (2**num_bits)-1
            c_var = c_var = "(" + "F"+str(int(n[0])) + "[" + str(int(n[1]-1)) + "]" + ">>" + str(n[2]) + ") & " + str(mask)
        else:
            c_var = num
    # If function is compare fuction
    elif fn in ("CMP"):  # ****************
        # print >> f2 , "Type: Compare"
        if ln == 2:
            c_var = "F"+str(int(n[0])) + "[" + str(n[1]-1) + "]"
        elif ln == 3:
            index = ( (n[1]-1) * (part_size/parity_bits) ) + n[2]
            c_var = "F"+str(int(n[0])) + "[" + str(index) + "]"
        # elif ln == 4: #yet to wite
        #     pass
        else:
            c_var = num
    # If function is predict parity
    elif fn  in ("PREDICTPARITY"):
        # print >> f2 , "inside predict ", num
        if num.isdigit() :
            c_var = int(num)-1
        elif ln == 2 and num.startswith("F"):
            c_var = c_var = "F"+str(int(n[0])) + "[" + str(int(n[1]-1)) + "]"
        elif ln == 2:
            c_var = str((n[0]-1)*(part_size/parity_bits) + n[1])
        elif ln ==3  :
            c_var = c_var = "F" + str(int(n[0])) + "[" + str((n[1]-1)*(part_size/parity_bits) + n[2]) + "]"
        else:
            c_var = num



    else: # **************** Others
        c_var = num

    # print >> f2 , "c_var ::" , c_var
    f2.close()
    return c_var


def addRoundFunction(dep_lst , dep_var , f_lin):
    f2 = open("./out/output.c" , 'a')
    print >> f2 , "void roundFunction(uint8_t F0[16])\n{"
    extraFunc = ''
    roundArrays = ''
    #
    for x in xrange(0, len(so.roundFunctions)):
        num = map(int, re.findall(r'\d+', so.roundFunctions[x]))

        so.roundFunctions[x]
        if x == len(so.roundFunctions) -1:
            if f_lin[1][num[0]] in ("FINDPARITY", "PREDICTPARITY", "CMP"  ):
                roundArrays = roundArrays + so.roundFunctions[x] + "[" + str(16*(part_size/parity_bits)) + "]"
            else:
                roundArrays = roundArrays + so.roundFunctions[x] +"[16]"
        else:
            if f_lin[1][num[0]] in ("FINDPARITY", "PREDICTPARITY", "CMP"  ):
                roundArrays = roundArrays + so.roundFunctions[x] + "[" + str(16*(part_size/parity_bits)) + "]" + " , "
            else:
                roundArrays = roundArrays + so.roundFunctions[x] + "[16] , "
    # Declaring variables  of round and parity
    print >> f2 , "\tuint8_t ", roundArrays , ";\n"
    # print >> f2 , dep_var
    #Iterating over all functions
    for f1 in dep_var:
        # print >> f2 , "printing subfuctions", dep_var[f1]
        #Iterating over all subfunctions of a function
        for subf in dep_var[f1]:
            # print >> f2 , "printing subfuctions", subf
            temp_varholder = []
            # Checking if the function and subfuction have any function associated or just swap function. This if for if any fuction assocoated.
            if f1 in dep_lst.keys() and subf in dep_lst[f1].keys() :
                # print >> f2 , dep_lst[f1][subf]
                # For each subfuction iterare over the tuples of that fucntion
                for tpl in dep_lst[f1][subf]:
                    #print >> f2 , "tpl" , tpl
                    # If the tuple is any operation like AND, XOR, etc.
                    if tpl in  so.defined_func or tpl in so.new_defined_func:
                        opHandler(tpl , temp_varholder,f_lin[1][f1] )
                    # If the tuple is any other variable.
                    else:
                        # print >> f2 , "Nonfunction"
                        nz = processForOriginal(tpl , f_lin[1][f1])
                        temp_varholder.append(nz)
                store_val = "F" + str(f1) + "[" + str(subf-1) + "]"
                print >> f2 , "\t",  store_val , "=" , temp_varholder[0] ,";"
            # For just swap like fuction where no dependent fuction is defined
            else:
                if f_lin[1][x] in ("FINDPARITY", "PREDICTPARITY" ,"CMP" ):
                    parity_funcs.add(str(f1))
                    store_val = "F" + str(f1) + "[" + str(subf-1) + "]"
                else:
                    store_val = "F" + str(f1) + "[" + str(subf-1) + "]"
                load_val = processForOriginal(dep_var[f1][subf][0] , f_lin[1][f1])
                print >> f2 , "\t",  store_val , "=" , load_val ,";"
        print >> f2 , "\n"
    print >> f2 , "}\n\n"
                # print >> f2 , "Yes"
    f2.close()


################################### Adding lookups i.e. external arrays used e.g. sbox, keys etc.###################################
def printLookups():
    f2 = open("./out/output.c" , 'a')
    # print >> f2 , "uint8_t",
    lookups = ''
    for lkp in xrange(0,len(so.lookup)):
        print >> f2 , "uint8_t",
        # print >> f2 , lkp
        print >> f2 , str(so.lookup[lkp]) ,
        if len(so.lookup_variables[so.lookup[lkp]]) != 0:
            print >> f2 , " = { ",
            for v in xrange (0, len(so.lookup_variables[so.lookup[lkp]])):
                val = '0x'+ so.lookup_variables[so.lookup[lkp]][v]
                print >> f2 , val ,
                comma = '}' if v == len(so.lookup_variables[so.lookup[lkp]])-1 else ", "
                print >> f2 ,  comma,
        # print >> f2 , "}",
        # lookups = lookups + str(so.lookup[lkp])
        # comma = ';\n' if lkp == len(so.lookup)-1 else ", "
        print >> f2 ,  ";"
        # lookups = lookups + str(comma)

        # print >> f2 , lkp
    print >> f2 , "\n"
    f2.close()

######################################## Main function ##################################################
def addMain(dep_lst , dep_var , f_lin):

    f2 = open("./out/output.c" , 'a')
    print >> f2 , "#include<stdio.h>"
    print >> f2 , "#include<stdlib.h>"
    print >> f2 , "#include<math.h>"
    print >> f2 , "#include <stdint.h>\n"
    f2.close()
    printLookups()
    addNewFunc()
    addRoundFunction(dep_lst , dep_var , f_lin)
    f2 = open("./out/output.c" , 'a')
    print >> f2 , "int main()\n{"
    print >> f2 , "\t uint8_t pt[16] = {0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a , 0x30 , 0x8d ,0x31, 0x31 , 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34}; "
    print >> f2 , "\t roundFunction(pt);"
    print >> f2 , "}"
    f2.close()
