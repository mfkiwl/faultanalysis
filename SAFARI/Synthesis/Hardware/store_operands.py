"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and / or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details: 

This code stores the different componets of BCS in different data structure while parsing. 
--------------------------------------------------------------------------------------------------
"""




# import generate_redundant as po


f_no = 0 #function number
p_no = 0 #part number
roundFunctions = []
depstack = dict() #store the functionality of the operations
dep_var_lst = dict() #store the list of dependent variables
func_linearity = dict()
func_linearity[0] = dict()
func_linearity[1] = dict()
defined_func = ["XOR" , "LKUP" , "LS" , "RS" ,"MUL" , "ECMP" , "AND"]
clookup = ''
lookup_variables = dict()
new_defined_func = set()
new_function = dict()
current_function_vars = []
lookup = []
function_depstack = dict()
curr_func = ''

#Add those functions to list of functions which are explicitly declared in specification
def pushNewFunc( strg, loc, toks ):
    global current_function_vars
    global curr_func
    new_defined_func.add(toks[1])
    new_function[toks[1]] = list(current_function_vars)
    current_function_vars = []
    function_depstack[toks[1]] = list()
    curr_func = toks[1]
    # print toks

def pushNewFuncVar(strg, loc, toks):
    global current_function_vars
    current_function_vars.append(toks[0])

def PushLinearity( strg, loc, toks ):
    # print "Calling linearity" , toks[0]
    func_linearity[0][f_no+1] = toks[0]


def PushType( strg, loc, toks ):
    # print "Calling type" , toks[0]
    func_linearity[1][f_no+1] = toks[0]



def AddLookup ( strg, loc, toks):
    global clookup
    # print "names"
    s=''
    for i in xrange(0,len(toks)):
        s = s + toks[i]
    lookup.append(s)
    lookup_variables[s] = []
    clookup = s


def AddLookupVariables (strg , loc, toks):
    # print "variables"
    # print "toks" , toks
    lookup_variables[clookup].append(toks[0])

def pushLines(strg, loc, toks):
    # print curr_func
    # print toks[1]
    function_depstack[curr_func].append(toks[1])

def pushFuncVariable (strg, loc, toks):
    # print curr_func
    # print toks
    s =''
    for i in xrange(0,len(toks)):
        s = s + toks[i]
    # print s
    function_depstack[curr_func].append(s)


def pushStoredVar (strg, loc, toks):
    # print curr_func
    # print toks
    s =''
    for i in xrange(0,len(toks)):
        s = s + toks[i]
    # print s
    function_depstack[curr_func].append(s)

def addRoundFunction (strg, loc, toks):
    # print toks[0]
    if toks[0] != "end":
        roundFunctions.append(toks[0])

#Add dependent variables in dep_var_lst. (Will be used in XFC/FaultDroid)
def pushDepVar( strg, loc, toks ):
    global p_no
    token = ''
    for x in toks:
        token = token + str(x)
    # print "Here" , token
    if f_no in dep_var_lst.keys():
        # print "Here1" , toks
        if p_no in dep_var_lst[f_no].keys():

            dep_var_lst[f_no][p_no].append( token )
            # print "Added1" , toks ,f_no , p_no
        else:
            dep_var_lst[f_no][int(p_no)] = []
            dep_var_lst[f_no][p_no].append( token )
            # print "Added2" , toks,f_no , p_no
    else:
        dep_var_lst[int(f_no)] = dict()
        if p_no in dep_var_lst[f_no].keys():
            dep_var_lst[f_no][p_no].append( token )
            # print "Added3" , toks,f_no , p_no
        else:
            dep_var_lst[f_no][int(p_no)] = []
            dep_var_lst[f_no][p_no].append( token )
            # print "Added4" , toks,f_no , p_no
    # print "Res " , dep_var_lst[f_no][p_no]




# Add function names in stack
def pushFuctions ( strg, loc, toks ):
    global p_no
    # print "Here" , toks[1]
    if f_no in depstack.keys():
        # print "Here1" , toks
        if p_no in depstack[f_no].keys():

            depstack[f_no][p_no].append( toks[1] )
            # print "Added"
        else:

            depstack[f_no][int(p_no)] = []
            depstack[f_no][p_no].append( toks[1] )
            # print "Added"
    else:
        depstack[int(f_no)] = dict()
        depstack[f_no][int(p_no)] = []
        depstack[f_no][p_no].append( toks[1] )
        # print "Added"

# Add variables to stack

def pushVariable( strg, loc, toks ):
    global p_no
    # print "Adding ",toks , len(toks)
    token = ''
    for x in toks:
        token = token + str(x)
    # print "Token :", token


    if f_no in depstack.keys():
        # print "Here1" , toks
        if p_no in depstack[f_no].keys():

            depstack[f_no][p_no].append( token )
        else:
            depstack[f_no][int(p_no)] = []
            depstack[f_no][p_no].append( token )
    else:
        depstack[int(f_no)] = dict()
        depstack[f_no][int(p_no)] = []
        depstack[f_no][p_no].append( token )



def increment_fno():
    # print "Calling increment"
    global f_no
    global p_no
    f_no = f_no+1
    p_no = 1

def increment_pno():
    global p_no
    p_no=p_no+1
