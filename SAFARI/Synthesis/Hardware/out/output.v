module Camellia_Core (din, dout, KEY, sel);
input  [127:0] din, KEY;
input          sel;
output [127:0] dout;

wire [127:0]  F1 , F6 , F10 , F14 , F18 , F22 ,   F0 ;
wire [ 63 :0]  F2 , F3 , F4 , F5 , F7 , F8 , F9 , F11 , F12 , F13 , F15 , F16 , F17 , F19 , F20 , F21 , F23 , F24 , F25  ;


assign  F0 =  din ;


assign F1[127:120] = KEY1[127:120]^F0[127:120] ;
assign F1[119:112] = KEY1[119:112]^F0[119:112] ;
assign F1[111:104] = KEY1[111:104]^F0[111:104] ;
assign F1[103:96] = KEY1[103:96]^F0[103:96] ;
assign F1[95:88] = KEY1[95:88]^F0[95:88] ;
assign F1[87:80] = KEY1[87:80]^F0[87:80] ;
assign F1[79:72] = KEY1[79:72]^F0[79:72] ;
assign F1[71:64] = KEY1[71:64]^F0[71:64] ;
assign F1[63:56] = KEY1[63:56]^F0[63:56] ;
assign F1[55:48] = KEY1[55:48]^F0[55:48] ;
assign F1[47:40] = KEY1[47:40]^F0[47:40] ;
assign F1[39:32] = KEY1[39:32]^F0[39:32] ;
assign F1[31:24] = KEY1[31:24]^F0[31:24] ;
assign F1[23:16] = KEY1[23:16]^F0[23:16] ;
assign F1[15:8] = KEY1[15:8]^F0[15:8] ;
assign F1[7:0] = KEY1[7:0]^F0[7:0] ;


assign F2[3 : 0] = PARITY( F0[127:120] ) ;
assign F2[7 : 4] = PARITY( F0[119:112] ) ;
assign F2[11 : 8] = PARITY( F0[111:104] ) ;
assign F2[15 : 12] = PARITY( F0[103:96] ) ;
assign F2[19 : 16] = PARITY( F0[95:88] ) ;
assign F2[23 : 20] = PARITY( F0[87:80] ) ;
assign F2[27 : 24] = PARITY( F0[79:72] ) ;
assign F2[31 : 28] = PARITY( F0[71:64] ) ;
assign F2[35 : 32] = PARITY( F0[63:56] ) ;
assign F2[39 : 36] = PARITY( F0[55:48] ) ;
assign F2[43 : 40] = PARITY( F0[47:40] ) ;
assign F2[47 : 44] = PARITY( F0[39:32] ) ;
assign F2[51 : 48] = PARITY( F0[31:24] ) ;
assign F2[55 : 52] = PARITY( F0[23:16] ) ;
assign F2[59 : 56] = PARITY( F0[15:8] ) ;
assign F2[63 : 60] = PARITY( F0[7:0] ) ;


assign F3[3 : 0] = PARITY( KEY1[ 127:120 ]  )^F2[3 : 0] ;
assign F3[7 : 4] = PARITY( KEY1[ 119:112 ]  )^F2[7 : 4] ;
assign F3[11 : 8] = PARITY( KEY1[ 111:104 ]  )^F2[11 : 8] ;
assign F3[15 : 12] = PARITY( KEY1[ 103:96 ]  )^F2[15 : 12] ;
assign F3[19 : 16] = PARITY( KEY1[ 95:88 ]  )^F2[19 : 16] ;
assign F3[23 : 20] = PARITY( KEY1[ 87:80 ]  )^F2[23 : 20] ;
assign F3[27 : 24] = PARITY( KEY1[ 79:72 ]  )^F2[27 : 24] ;
assign F3[31 : 28] = PARITY( KEY1[ 71:64 ]  )^F2[31 : 28] ;
assign F3[35 : 32] = PARITY( KEY1[ 63:56 ]  )^F2[35 : 32] ;
assign F3[39 : 36] = PARITY( KEY1[ 55:48 ]  )^F2[39 : 36] ;
assign F3[43 : 40] = PARITY( KEY1[ 47:40 ]  )^F2[43 : 40] ;
assign F3[47 : 44] = PARITY( KEY1[ 39:32 ]  )^F2[47 : 44] ;
assign F3[51 : 48] = PARITY( KEY1[ 31:24 ]  )^F2[51 : 48] ;
assign F3[55 : 52] = PARITY( KEY1[ 23:16 ]  )^F2[55 : 52] ;
assign F3[59 : 56] = PARITY( KEY1[ 15:8 ]  )^F2[59 : 56] ;
assign F3[63 : 60] = PARITY( KEY1[ 7:0 ]  )^F2[63 : 60] ;


assign F4[3 : 0] = PARITY( F1[127:120] ) ;
assign F4[7 : 4] = PARITY( F1[119:112] ) ;
assign F4[11 : 8] = PARITY( F1[111:104] ) ;
assign F4[15 : 12] = PARITY( F1[103:96] ) ;
assign F4[19 : 16] = PARITY( F1[95:88] ) ;
assign F4[23 : 20] = PARITY( F1[87:80] ) ;
assign F4[27 : 24] = PARITY( F1[79:72] ) ;
assign F4[31 : 28] = PARITY( F1[71:64] ) ;
assign F4[35 : 32] = PARITY( F1[63:56] ) ;
assign F4[39 : 36] = PARITY( F1[55:48] ) ;
assign F4[43 : 40] = PARITY( F1[47:40] ) ;
assign F4[47 : 44] = PARITY( F1[39:32] ) ;
assign F4[51 : 48] = PARITY( F1[31:24] ) ;
assign F4[55 : 52] = PARITY( F1[23:16] ) ;
assign F4[59 : 56] = PARITY( F1[15:8] ) ;
assign F4[63 : 60] = PARITY( F1[7:0] ) ;


assign F5[3 : 0] = ECMP( F3[3 : 0],F4[3 : 0] ) ;
assign F5[7 : 4] = ECMP( F3[7 : 4],F4[7 : 4] ) ;
assign F5[11 : 8] = ECMP( F3[11 : 8],F4[11 : 8] ) ;
assign F5[15 : 12] = ECMP( F3[15 : 12],F4[15 : 12] ) ;
assign F5[19 : 16] = ECMP( F3[19 : 16],F4[19 : 16] ) ;
assign F5[23 : 20] = ECMP( F3[23 : 20],F4[23 : 20] ) ;
assign F5[27 : 24] = ECMP( F3[27 : 24],F4[27 : 24] ) ;
assign F5[31 : 28] = ECMP( F3[31 : 28],F4[31 : 28] ) ;
assign F5[35 : 32] = ECMP( F3[35 : 32],F4[35 : 32] ) ;
assign F5[39 : 36] = ECMP( F3[39 : 36],F4[39 : 36] ) ;
assign F5[43 : 40] = ECMP( F3[43 : 40],F4[43 : 40] ) ;
assign F5[47 : 44] = ECMP( F3[47 : 44],F4[47 : 44] ) ;
assign F5[51 : 48] = ECMP( F3[51 : 48],F4[51 : 48] ) ;
assign F5[55 : 52] = ECMP( F3[55 : 52],F4[55 : 52] ) ;
assign F5[59 : 56] = ECMP( F3[59 : 56],F4[59 : 56] ) ;
assign F5[63 : 60] = ECMP( F3[63 : 60],F4[63 : 60] ) ;


assign F6[63:56] = KEY2[63:56]^F1[127:120] ;
assign F6[55:48] = KEY2[55:48]^F1[119:112] ;
assign F6[47:40] = KEY2[47:40]^F1[111:104] ;
assign F6[39:32] = KEY2[39:32]^F1[103:96] ;
assign F6[31:24] = KEY2[31:24]^F1[95:88] ;
assign F6[23:16] = KEY2[23:16]^F1[87:80] ;
assign F6[15:8] = KEY2[15:8]^F1[79:72] ;
assign F6[7:0] = KEY2[7:0]^F1[71:64] ;


assign F7[3 : 0] = PARITY( KEY2[ 63:56 ]  )^F4[3 : 0] ;
assign F7[7 : 4] = PARITY( KEY2[ 55:48 ]  )^F4[7 : 4] ;
assign F7[11 : 8] = PARITY( KEY2[ 47:40 ]  )^F4[11 : 8] ;
assign F7[15 : 12] = PARITY( KEY2[ 39:32 ]  )^F4[15 : 12] ;
assign F7[19 : 16] = PARITY( KEY2[ 31:24 ]  )^F4[19 : 16] ;
assign F7[23 : 20] = PARITY( KEY2[ 23:16 ]  )^F4[23 : 20] ;
assign F7[27 : 24] = PARITY( KEY2[ 15:8 ]  )^F4[27 : 24] ;
assign F7[31 : 28] = PARITY( KEY2[ 7:0 ]  )^F4[31 : 28] ;


assign F8[3 : 0] = PARITY( F6[63:56] ) ;
assign F8[7 : 4] = PARITY( F6[55:48] ) ;
assign F8[11 : 8] = PARITY( F6[47:40] ) ;
assign F8[15 : 12] = PARITY( F6[39:32] ) ;
assign F8[19 : 16] = PARITY( F6[31:24] ) ;
assign F8[23 : 20] = PARITY( F6[23:16] ) ;
assign F8[27 : 24] = PARITY( F6[15:8] ) ;
assign F8[31 : 28] = PARITY( F6[7:0] ) ;


assign F9[3 : 0] = ECMP( F7[3 : 0],F8[3 : 0] ) ;
assign F9[7 : 4] = ECMP( F7[7 : 4],F8[7 : 4] ) ;
assign F9[11 : 8] = ECMP( F7[11 : 8],F8[11 : 8] ) ;
assign F9[15 : 12] = ECMP( F7[15 : 12],F8[15 : 12] ) ;
assign F9[19 : 16] = ECMP( F7[19 : 16],F8[19 : 16] ) ;
assign F9[23 : 20] = ECMP( F7[23 : 20],F8[23 : 20] ) ;
assign F9[27 : 24] = ECMP( F7[27 : 24],F8[27 : 24] ) ;
assign F9[31 : 28] = ECMP( F7[31 : 28],F8[31 : 28] ) ;


SubByte  SB1 ( F6[63:56] , F10[63:56] );
SubByte  SB2 ( F6[55:48] , F10[55:48] );
SubByte  SB3 ( F6[47:40] , F10[47:40] );
SubByte  SB4 ( F6[39:32] , F10[39:32] );
SubByte  SB5 ( F6[31:24] , F10[31:24] );
SubByte  SB6 ( F6[23:16] , F10[23:16] );
SubByte  SB7 ( F6[15:8] , F10[15:8] );
SubByte  SB8 ( F6[7:0] , F10[7:0] );


assign F11[3 : 0] = BIT( F6[63:56],PSBOX1 )^F8[3 : 0] ;
assign F11[7 : 4] = BIT( F6[55:48],PSBOX2 )^F8[7 : 4] ;
assign F11[11 : 8] = BIT( F6[47:40],PSBOX3 )^F8[11 : 8] ;
assign F11[15 : 12] = BIT( F6[39:32],PSBOX4 )^F8[15 : 12] ;
assign F11[19 : 16] = BIT( F6[31:24],PSBOX4 )^F8[19 : 16] ;
assign F11[23 : 20] = BIT( F6[23:16],PSBOX3 )^F8[23 : 20] ;
assign F11[27 : 24] = BIT( F6[15:8],PSBOX2 )^F8[27 : 24] ;
assign F11[31 : 28] = BIT( F6[7:0],PSBOX1 )^F8[31 : 28] ;


assign F12[3 : 0] = PARITY( F10[63:56] ) ;
assign F12[7 : 4] = PARITY( F10[55:48] ) ;
assign F12[11 : 8] = PARITY( F10[47:40] ) ;
assign F12[15 : 12] = PARITY( F10[39:32] ) ;
assign F12[19 : 16] = PARITY( F10[31:24] ) ;
assign F12[23 : 20] = PARITY( F10[23:16] ) ;
assign F12[27 : 24] = PARITY( F10[15:8] ) ;
assign F12[31 : 28] = PARITY( F10[7:0] ) ;


assign F13[3 : 0] = ECMP( F11[3 : 0],F12[3 : 0] ) ;
assign F13[7 : 4] = ECMP( F11[7 : 4],F12[7 : 4] ) ;
assign F13[11 : 8] = ECMP( F11[11 : 8],F12[11 : 8] ) ;
assign F13[15 : 12] = ECMP( F11[15 : 12],F12[15 : 12] ) ;
assign F13[19 : 16] = ECMP( F11[19 : 16],F12[19 : 16] ) ;
assign F13[23 : 20] = ECMP( F11[23 : 20],F12[23 : 20] ) ;
assign F13[27 : 24] = ECMP( F11[27 : 24],F12[27 : 24] ) ;
assign F13[31 : 28] = ECMP( F11[31 : 28],F12[31 : 28] ) ;


assign F14[63:56] = F10[7:0]^F10[15:8]^F10[23:16]^F10[39:32]^F10[47:40]^F10[63:56] ;
assign F14[55:48] = F10[7:0]^F10[15:8]^F10[31:24]^F10[39:32]^F10[55:48]^F10[63:56] ;
assign F14[47:40] = F10[7:0]^F10[23:16]^F10[31:24]^F10[47:40]^F10[55:48]^F10[63:56] ;
assign F14[39:32] = F10[15:8]^F10[23:16]^F10[31:24]^F10[39:32]^F10[47:40]^F10[55:48] ;
assign F14[31:24] = F10[7:0]^F10[15:8]^F10[23:16]^F10[55:48]^F10[63:56] ;
assign F14[23:16] = F10[7:0]^F10[15:8]^F10[31:24]^F10[47:40]^F10[55:48] ;
assign F14[15:8] = F10[7:0]^F10[23:16]^F10[31:24]^F10[39:32]^F10[47:40] ;
assign F14[7:0] = F10[15:8]^F10[23:16]^F10[31:24]^F10[39:32]^F10[63:56] ;


assign F15[3 : 0] = F12[31 : 28]^F12[27 : 24]^F12[23 : 20]^F12[15 : 12]^F12[11 : 8]^F12[3 : 0] ;
assign F15[7 : 4] = F12[31 : 28]^F12[27 : 24]^F12[19 : 16]^F12[15 : 12]^F12[7 : 4]^F12[3 : 0] ;
assign F15[11 : 8] = F12[31 : 28]^F12[23 : 20]^F12[19 : 16]^F12[11 : 8]^F12[7 : 4]^F12[3 : 0] ;
assign F15[15 : 12] = F12[27 : 24]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[11 : 8]^F12[7 : 4] ;
assign F15[19 : 16] = F12[31 : 28]^F12[27 : 24]^F12[23 : 20]^F12[7 : 4]^F12[3 : 0] ;
assign F15[23 : 20] = F12[31 : 28]^F12[27 : 24]^F12[19 : 16]^F12[11 : 8]^F12[7 : 4] ;
assign F15[27 : 24] = F12[31 : 28]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[11 : 8] ;
assign F15[31 : 28] = F12[27 : 24]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[3 : 0] ;


assign F16[3 : 0] = PARITY( F14[63:56] ) ;
assign F16[7 : 4] = PARITY( F14[55:48] ) ;
assign F16[11 : 8] = PARITY( F14[47:40] ) ;
assign F16[15 : 12] = PARITY( F14[39:32] ) ;
assign F16[19 : 16] = PARITY( F14[31:24] ) ;
assign F16[23 : 20] = PARITY( F14[23:16] ) ;
assign F16[27 : 24] = PARITY( F14[15:8] ) ;
assign F16[31 : 28] = PARITY( F14[7:0] ) ;


assign F17[3 : 0] = ECMP( F15[3 : 0],F16[3 : 0] ) ;
assign F17[7 : 4] = ECMP( F15[7 : 4],F16[7 : 4] ) ;
assign F17[11 : 8] = ECMP( F15[11 : 8],F16[11 : 8] ) ;
assign F17[15 : 12] = ECMP( F15[15 : 12],F16[15 : 12] ) ;
assign F17[19 : 16] = ECMP( F15[19 : 16],F16[19 : 16] ) ;
assign F17[23 : 20] = ECMP( F15[23 : 20],F16[23 : 20] ) ;
assign F17[27 : 24] = ECMP( F15[27 : 24],F16[27 : 24] ) ;
assign F17[31 : 28] = ECMP( F15[31 : 28],F16[31 : 28] ) ;


assign F18[63:56] = F1[63:56]^F14[63:56] ;
assign F18[55:48] = F1[55:48]^F14[55:48] ;
assign F18[47:40] = F1[47:40]^F14[47:40] ;
assign F18[39:32] = F1[39:32]^F14[39:32] ;
assign F18[31:24] = F1[31:24]^F14[31:24] ;
assign F18[23:16] = F1[23:16]^F14[23:16] ;
assign F18[15:8] = F1[15:8]^F14[15:8] ;
assign F18[7:0] = F1[7:0]^F14[7:0] ;


assign F19[3 : 0] = F3[35 : 32]^F16[3 : 0] ;
assign F19[7 : 4] = F3[39 : 36]^F16[7 : 4] ;
assign F19[11 : 8] = F3[43 : 40]^F16[11 : 8] ;
assign F19[15 : 12] = F3[47 : 44]^F16[15 : 12] ;
assign F19[19 : 16] = F3[51 : 48]^F16[19 : 16] ;
assign F19[23 : 20] = F3[55 : 52]^F16[23 : 20] ;
assign F19[27 : 24] = F3[59 : 56]^F16[27 : 24] ;
assign F19[31 : 28] = F3[63 : 60]^F16[31 : 28] ;


assign F20[3 : 0] = PARITY( F18[63:56] ) ;
assign F20[7 : 4] = PARITY( F18[55:48] ) ;
assign F20[11 : 8] = PARITY( F18[47:40] ) ;
assign F20[15 : 12] = PARITY( F18[39:32] ) ;
assign F20[19 : 16] = PARITY( F18[31:24] ) ;
assign F20[23 : 20] = PARITY( F18[23:16] ) ;
assign F20[27 : 24] = PARITY( F18[15:8] ) ;
assign F20[31 : 28] = PARITY( F18[7:0] ) ;


assign F21[3 : 0] = ECMP( F19[3 : 0],F20[3 : 0] ) ;
assign F21[7 : 4] = ECMP( F19[7 : 4],F20[7 : 4] ) ;
assign F21[11 : 8] = ECMP( F19[11 : 8],F20[11 : 8] ) ;
assign F21[15 : 12] = ECMP( F19[15 : 12],F20[15 : 12] ) ;
assign F21[19 : 16] = ECMP( F19[19 : 16],F20[19 : 16] ) ;
assign F21[23 : 20] = ECMP( F19[23 : 20],F20[23 : 20] ) ;
assign F21[27 : 24] = ECMP( F19[27 : 24],F20[27 : 24] ) ;
assign F21[31 : 28] = ECMP( F19[31 : 28],F20[31 : 28] ) ;


assign F22[127:120] = F18[63:56] ;
assign F22[119:112] = F18[55:48] ;
assign F22[111:104] = F18[47:40] ;
assign F22[103:96] = F18[39:32] ;
assign F22[95:88] = F18[31:24] ;
assign F22[87:80] = F18[23:16] ;
assign F22[79:72] = F18[15:8] ;
assign F22[71:64] = F18[7:0] ;
assign F22[63:56] = F1[127:120] ;
assign F22[55:48] = F1[119:112] ;
assign F22[47:40] = F1[111:104] ;
assign F22[39:32] = F1[103:96] ;
assign F22[31:24] = F1[95:88] ;
assign F22[23:16] = F1[87:80] ;
assign F22[15:8] = F1[79:72] ;
assign F22[7:0] = F1[71:64] ;


assign F23[3 : 0] = F20[3 : 0] ;
assign F23[7 : 4] = F20[7 : 4] ;
assign F23[11 : 8] = F20[11 : 8] ;
assign F23[15 : 12] = F20[15 : 12] ;
assign F23[19 : 16] = F20[19 : 16] ;
assign F23[23 : 20] = F20[23 : 20] ;
assign F23[27 : 24] = F20[27 : 24] ;
assign F23[31 : 28] = F20[31 : 28] ;
assign F23[35 : 32] = F3[3 : 0] ;
assign F23[39 : 36] = F3[7 : 4] ;
assign F23[43 : 40] = F3[11 : 8] ;
assign F23[47 : 44] = F3[15 : 12] ;
assign F23[51 : 48] = F3[19 : 16] ;
assign F23[55 : 52] = F3[23 : 20] ;
assign F23[59 : 56] = F3[27 : 24] ;
assign F23[63 : 60] = F3[31 : 28] ;


assign F24[3 : 0] = PARITY( F22[127:120] ) ;
assign F24[7 : 4] = PARITY( F22[119:112] ) ;
assign F24[11 : 8] = PARITY( F22[111:104] ) ;
assign F24[15 : 12] = PARITY( F22[103:96] ) ;
assign F24[19 : 16] = PARITY( F22[95:88] ) ;
assign F24[23 : 20] = PARITY( F22[87:80] ) ;
assign F24[27 : 24] = PARITY( F22[79:72] ) ;
assign F24[31 : 28] = PARITY( F22[71:64] ) ;
assign F24[35 : 32] = PARITY( F22[63:56] ) ;
assign F24[39 : 36] = PARITY( F22[55:48] ) ;
assign F24[43 : 40] = PARITY( F22[47:40] ) ;
assign F24[47 : 44] = PARITY( F22[39:32] ) ;
assign F24[51 : 48] = PARITY( F22[31:24] ) ;
assign F24[55 : 52] = PARITY( F22[23:16] ) ;
assign F24[59 : 56] = PARITY( F22[15:8] ) ;
assign F24[63 : 60] = PARITY( F22[7:0] ) ;


assign F25[3 : 0] = ECMP( F23[3 : 0],F24[3 : 0] ) ;
assign F25[7 : 4] = ECMP( F23[7 : 4],F24[7 : 4] ) ;
assign F25[11 : 8] = ECMP( F23[11 : 8],F24[11 : 8] ) ;
assign F25[15 : 12] = ECMP( F23[15 : 12],F24[15 : 12] ) ;
assign F25[19 : 16] = ECMP( F23[19 : 16],F24[19 : 16] ) ;
assign F25[23 : 20] = ECMP( F23[23 : 20],F24[23 : 20] ) ;
assign F25[27 : 24] = ECMP( F23[27 : 24],F24[27 : 24] ) ;
assign F25[31 : 28] = ECMP( F23[31 : 28],F24[31 : 28] ) ;
assign F25[35 : 32] = ECMP( F23[35 : 32],F24[35 : 32] ) ;
assign F25[39 : 36] = ECMP( F23[39 : 36],F24[39 : 36] ) ;
assign F25[43 : 40] = ECMP( F23[43 : 40],F24[43 : 40] ) ;
assign F25[47 : 44] = ECMP( F23[47 : 44],F24[47 : 44] ) ;
assign F25[51 : 48] = ECMP( F23[51 : 48],F24[51 : 48] ) ;
assign F25[55 : 52] = ECMP( F23[55 : 52],F24[55 : 52] ) ;
assign F25[59 : 56] = ECMP( F23[59 : 56],F24[59 : 56] ) ;
assign F25[63 : 60] = ECMP( F23[63 : 60],F24[63 : 60] ) ;


function [7:0]  PARITY ;
input [7:0]  a ;
reg [7:0]  b,c,d  ;
begin
b=a>>4;
c=a^b;
d=c&8'hf';
PARITY = d ;
end
endfunction




function [7:0]  BIT ;
input [7:0]  a,b ;
reg [7:0]  c,d,e,f,g,h  ;
begin
c=a>>8'h1';
d = b[c];
e=a&8'h1';
f=e<<8'h2';
g=d>>f;
h=g&8'hf';
BIT = h ;
end
endfunction




function [7:0]  ECMP ;
input [7:0]  a,b ;
reg [7:0]  r  ;
begin
r=a^b;
ECMP = r ;
end
endfunction


endmodule
module Camellia_Core (din, dout, KEY, sel);
input  [127:0] din, KEY;
input          sel;
output [127:0] dout;

wire [127:0]  F1 , F6 , F10 , F14 , F18 , F22 ,   F0 ;
wire [ 63 :0]  F2 , F3 , F4 , F5 , F7 , F8 , F9 , F11 , F12 , F13 , F15 , F16 , F17 , F19 , F20 , F21 , F23 , F24 , F25  ;


assign  F0 =  din ;


assign F1[127:120] = KEY1[127:120]^F0[127:120] ;
assign F1[119:112] = KEY1[119:112]^F0[119:112] ;
assign F1[111:104] = KEY1[111:104]^F0[111:104] ;
assign F1[103:96] = KEY1[103:96]^F0[103:96] ;
assign F1[95:88] = KEY1[95:88]^F0[95:88] ;
assign F1[87:80] = KEY1[87:80]^F0[87:80] ;
assign F1[79:72] = KEY1[79:72]^F0[79:72] ;
assign F1[71:64] = KEY1[71:64]^F0[71:64] ;
assign F1[63:56] = KEY1[63:56]^F0[63:56] ;
assign F1[55:48] = KEY1[55:48]^F0[55:48] ;
assign F1[47:40] = KEY1[47:40]^F0[47:40] ;
assign F1[39:32] = KEY1[39:32]^F0[39:32] ;
assign F1[31:24] = KEY1[31:24]^F0[31:24] ;
assign F1[23:16] = KEY1[23:16]^F0[23:16] ;
assign F1[15:8] = KEY1[15:8]^F0[15:8] ;
assign F1[7:0] = KEY1[7:0]^F0[7:0] ;


assign F2[3 : 0] = PARITY( F0[127:120] ) ;
assign F2[7 : 4] = PARITY( F0[119:112] ) ;
assign F2[11 : 8] = PARITY( F0[111:104] ) ;
assign F2[15 : 12] = PARITY( F0[103:96] ) ;
assign F2[19 : 16] = PARITY( F0[95:88] ) ;
assign F2[23 : 20] = PARITY( F0[87:80] ) ;
assign F2[27 : 24] = PARITY( F0[79:72] ) ;
assign F2[31 : 28] = PARITY( F0[71:64] ) ;
assign F2[35 : 32] = PARITY( F0[63:56] ) ;
assign F2[39 : 36] = PARITY( F0[55:48] ) ;
assign F2[43 : 40] = PARITY( F0[47:40] ) ;
assign F2[47 : 44] = PARITY( F0[39:32] ) ;
assign F2[51 : 48] = PARITY( F0[31:24] ) ;
assign F2[55 : 52] = PARITY( F0[23:16] ) ;
assign F2[59 : 56] = PARITY( F0[15:8] ) ;
assign F2[63 : 60] = PARITY( F0[7:0] ) ;


assign F3[3 : 0] = PARITY( KEY1[ 127:120 ]  )^F2[3 : 0] ;
assign F3[7 : 4] = PARITY( KEY1[ 119:112 ]  )^F2[7 : 4] ;
assign F3[11 : 8] = PARITY( KEY1[ 111:104 ]  )^F2[11 : 8] ;
assign F3[15 : 12] = PARITY( KEY1[ 103:96 ]  )^F2[15 : 12] ;
assign F3[19 : 16] = PARITY( KEY1[ 95:88 ]  )^F2[19 : 16] ;
assign F3[23 : 20] = PARITY( KEY1[ 87:80 ]  )^F2[23 : 20] ;
assign F3[27 : 24] = PARITY( KEY1[ 79:72 ]  )^F2[27 : 24] ;
assign F3[31 : 28] = PARITY( KEY1[ 71:64 ]  )^F2[31 : 28] ;
assign F3[35 : 32] = PARITY( KEY1[ 63:56 ]  )^F2[35 : 32] ;
assign F3[39 : 36] = PARITY( KEY1[ 55:48 ]  )^F2[39 : 36] ;
assign F3[43 : 40] = PARITY( KEY1[ 47:40 ]  )^F2[43 : 40] ;
assign F3[47 : 44] = PARITY( KEY1[ 39:32 ]  )^F2[47 : 44] ;
assign F3[51 : 48] = PARITY( KEY1[ 31:24 ]  )^F2[51 : 48] ;
assign F3[55 : 52] = PARITY( KEY1[ 23:16 ]  )^F2[55 : 52] ;
assign F3[59 : 56] = PARITY( KEY1[ 15:8 ]  )^F2[59 : 56] ;
assign F3[63 : 60] = PARITY( KEY1[ 7:0 ]  )^F2[63 : 60] ;


assign F4[3 : 0] = PARITY( F1[127:120] ) ;
assign F4[7 : 4] = PARITY( F1[119:112] ) ;
assign F4[11 : 8] = PARITY( F1[111:104] ) ;
assign F4[15 : 12] = PARITY( F1[103:96] ) ;
assign F4[19 : 16] = PARITY( F1[95:88] ) ;
assign F4[23 : 20] = PARITY( F1[87:80] ) ;
assign F4[27 : 24] = PARITY( F1[79:72] ) ;
assign F4[31 : 28] = PARITY( F1[71:64] ) ;
assign F4[35 : 32] = PARITY( F1[63:56] ) ;
assign F4[39 : 36] = PARITY( F1[55:48] ) ;
assign F4[43 : 40] = PARITY( F1[47:40] ) ;
assign F4[47 : 44] = PARITY( F1[39:32] ) ;
assign F4[51 : 48] = PARITY( F1[31:24] ) ;
assign F4[55 : 52] = PARITY( F1[23:16] ) ;
assign F4[59 : 56] = PARITY( F1[15:8] ) ;
assign F4[63 : 60] = PARITY( F1[7:0] ) ;


assign F5[3 : 0] = ECMP( F3[3 : 0],F4[3 : 0] ) ;
assign F5[7 : 4] = ECMP( F3[7 : 4],F4[7 : 4] ) ;
assign F5[11 : 8] = ECMP( F3[11 : 8],F4[11 : 8] ) ;
assign F5[15 : 12] = ECMP( F3[15 : 12],F4[15 : 12] ) ;
assign F5[19 : 16] = ECMP( F3[19 : 16],F4[19 : 16] ) ;
assign F5[23 : 20] = ECMP( F3[23 : 20],F4[23 : 20] ) ;
assign F5[27 : 24] = ECMP( F3[27 : 24],F4[27 : 24] ) ;
assign F5[31 : 28] = ECMP( F3[31 : 28],F4[31 : 28] ) ;
assign F5[35 : 32] = ECMP( F3[35 : 32],F4[35 : 32] ) ;
assign F5[39 : 36] = ECMP( F3[39 : 36],F4[39 : 36] ) ;
assign F5[43 : 40] = ECMP( F3[43 : 40],F4[43 : 40] ) ;
assign F5[47 : 44] = ECMP( F3[47 : 44],F4[47 : 44] ) ;
assign F5[51 : 48] = ECMP( F3[51 : 48],F4[51 : 48] ) ;
assign F5[55 : 52] = ECMP( F3[55 : 52],F4[55 : 52] ) ;
assign F5[59 : 56] = ECMP( F3[59 : 56],F4[59 : 56] ) ;
assign F5[63 : 60] = ECMP( F3[63 : 60],F4[63 : 60] ) ;


assign F6[63:56] = KEY2[63:56]^F1[127:120] ;
assign F6[55:48] = KEY2[55:48]^F1[119:112] ;
assign F6[47:40] = KEY2[47:40]^F1[111:104] ;
assign F6[39:32] = KEY2[39:32]^F1[103:96] ;
assign F6[31:24] = KEY2[31:24]^F1[95:88] ;
assign F6[23:16] = KEY2[23:16]^F1[87:80] ;
assign F6[15:8] = KEY2[15:8]^F1[79:72] ;
assign F6[7:0] = KEY2[7:0]^F1[71:64] ;


assign F7[3 : 0] = PARITY( KEY2[ 63:56 ]  )^F4[3 : 0] ;
assign F7[7 : 4] = PARITY( KEY2[ 55:48 ]  )^F4[7 : 4] ;
assign F7[11 : 8] = PARITY( KEY2[ 47:40 ]  )^F4[11 : 8] ;
assign F7[15 : 12] = PARITY( KEY2[ 39:32 ]  )^F4[15 : 12] ;
assign F7[19 : 16] = PARITY( KEY2[ 31:24 ]  )^F4[19 : 16] ;
assign F7[23 : 20] = PARITY( KEY2[ 23:16 ]  )^F4[23 : 20] ;
assign F7[27 : 24] = PARITY( KEY2[ 15:8 ]  )^F4[27 : 24] ;
assign F7[31 : 28] = PARITY( KEY2[ 7:0 ]  )^F4[31 : 28] ;


assign F8[3 : 0] = PARITY( F6[63:56] ) ;
assign F8[7 : 4] = PARITY( F6[55:48] ) ;
assign F8[11 : 8] = PARITY( F6[47:40] ) ;
assign F8[15 : 12] = PARITY( F6[39:32] ) ;
assign F8[19 : 16] = PARITY( F6[31:24] ) ;
assign F8[23 : 20] = PARITY( F6[23:16] ) ;
assign F8[27 : 24] = PARITY( F6[15:8] ) ;
assign F8[31 : 28] = PARITY( F6[7:0] ) ;


assign F9[3 : 0] = ECMP( F7[3 : 0],F8[3 : 0] ) ;
assign F9[7 : 4] = ECMP( F7[7 : 4],F8[7 : 4] ) ;
assign F9[11 : 8] = ECMP( F7[11 : 8],F8[11 : 8] ) ;
assign F9[15 : 12] = ECMP( F7[15 : 12],F8[15 : 12] ) ;
assign F9[19 : 16] = ECMP( F7[19 : 16],F8[19 : 16] ) ;
assign F9[23 : 20] = ECMP( F7[23 : 20],F8[23 : 20] ) ;
assign F9[27 : 24] = ECMP( F7[27 : 24],F8[27 : 24] ) ;
assign F9[31 : 28] = ECMP( F7[31 : 28],F8[31 : 28] ) ;


SubByte  SB1 ( F6[63:56] , F10[63:56] );
SubByte  SB2 ( F6[55:48] , F10[55:48] );
SubByte  SB3 ( F6[47:40] , F10[47:40] );
SubByte  SB4 ( F6[39:32] , F10[39:32] );
SubByte  SB5 ( F6[31:24] , F10[31:24] );
SubByte  SB6 ( F6[23:16] , F10[23:16] );
SubByte  SB7 ( F6[15:8] , F10[15:8] );
SubByte  SB8 ( F6[7:0] , F10[7:0] );


assign F11[3 : 0] = BIT( F6[63:56],PSBOX1 )^F8[3 : 0] ;
assign F11[7 : 4] = BIT( F6[55:48],PSBOX2 )^F8[7 : 4] ;
assign F11[11 : 8] = BIT( F6[47:40],PSBOX3 )^F8[11 : 8] ;
assign F11[15 : 12] = BIT( F6[39:32],PSBOX4 )^F8[15 : 12] ;
assign F11[19 : 16] = BIT( F6[31:24],PSBOX4 )^F8[19 : 16] ;
assign F11[23 : 20] = BIT( F6[23:16],PSBOX3 )^F8[23 : 20] ;
assign F11[27 : 24] = BIT( F6[15:8],PSBOX2 )^F8[27 : 24] ;
assign F11[31 : 28] = BIT( F6[7:0],PSBOX1 )^F8[31 : 28] ;


assign F12[3 : 0] = PARITY( F10[63:56] ) ;
assign F12[7 : 4] = PARITY( F10[55:48] ) ;
assign F12[11 : 8] = PARITY( F10[47:40] ) ;
assign F12[15 : 12] = PARITY( F10[39:32] ) ;
assign F12[19 : 16] = PARITY( F10[31:24] ) ;
assign F12[23 : 20] = PARITY( F10[23:16] ) ;
assign F12[27 : 24] = PARITY( F10[15:8] ) ;
assign F12[31 : 28] = PARITY( F10[7:0] ) ;


assign F13[3 : 0] = ECMP( F11[3 : 0],F12[3 : 0] ) ;
assign F13[7 : 4] = ECMP( F11[7 : 4],F12[7 : 4] ) ;
assign F13[11 : 8] = ECMP( F11[11 : 8],F12[11 : 8] ) ;
assign F13[15 : 12] = ECMP( F11[15 : 12],F12[15 : 12] ) ;
assign F13[19 : 16] = ECMP( F11[19 : 16],F12[19 : 16] ) ;
assign F13[23 : 20] = ECMP( F11[23 : 20],F12[23 : 20] ) ;
assign F13[27 : 24] = ECMP( F11[27 : 24],F12[27 : 24] ) ;
assign F13[31 : 28] = ECMP( F11[31 : 28],F12[31 : 28] ) ;


assign F14[63:56] = F10[7:0]^F10[15:8]^F10[23:16]^F10[39:32]^F10[47:40]^F10[63:56] ;
assign F14[55:48] = F10[7:0]^F10[15:8]^F10[31:24]^F10[39:32]^F10[55:48]^F10[63:56] ;
assign F14[47:40] = F10[7:0]^F10[23:16]^F10[31:24]^F10[47:40]^F10[55:48]^F10[63:56] ;
assign F14[39:32] = F10[15:8]^F10[23:16]^F10[31:24]^F10[39:32]^F10[47:40]^F10[55:48] ;
assign F14[31:24] = F10[7:0]^F10[15:8]^F10[23:16]^F10[55:48]^F10[63:56] ;
assign F14[23:16] = F10[7:0]^F10[15:8]^F10[31:24]^F10[47:40]^F10[55:48] ;
assign F14[15:8] = F10[7:0]^F10[23:16]^F10[31:24]^F10[39:32]^F10[47:40] ;
assign F14[7:0] = F10[15:8]^F10[23:16]^F10[31:24]^F10[39:32]^F10[63:56] ;


assign F15[3 : 0] = F12[31 : 28]^F12[27 : 24]^F12[23 : 20]^F12[15 : 12]^F12[11 : 8]^F12[3 : 0] ;
assign F15[7 : 4] = F12[31 : 28]^F12[27 : 24]^F12[19 : 16]^F12[15 : 12]^F12[7 : 4]^F12[3 : 0] ;
assign F15[11 : 8] = F12[31 : 28]^F12[23 : 20]^F12[19 : 16]^F12[11 : 8]^F12[7 : 4]^F12[3 : 0] ;
assign F15[15 : 12] = F12[27 : 24]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[11 : 8]^F12[7 : 4] ;
assign F15[19 : 16] = F12[31 : 28]^F12[27 : 24]^F12[23 : 20]^F12[7 : 4]^F12[3 : 0] ;
assign F15[23 : 20] = F12[31 : 28]^F12[27 : 24]^F12[19 : 16]^F12[11 : 8]^F12[7 : 4] ;
assign F15[27 : 24] = F12[31 : 28]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[11 : 8] ;
assign F15[31 : 28] = F12[27 : 24]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[3 : 0] ;


assign F16[3 : 0] = PARITY( F14[63:56] ) ;
assign F16[7 : 4] = PARITY( F14[55:48] ) ;
assign F16[11 : 8] = PARITY( F14[47:40] ) ;
assign F16[15 : 12] = PARITY( F14[39:32] ) ;
assign F16[19 : 16] = PARITY( F14[31:24] ) ;
assign F16[23 : 20] = PARITY( F14[23:16] ) ;
assign F16[27 : 24] = PARITY( F14[15:8] ) ;
assign F16[31 : 28] = PARITY( F14[7:0] ) ;


assign F17[3 : 0] = ECMP( F15[3 : 0],F16[3 : 0] ) ;
assign F17[7 : 4] = ECMP( F15[7 : 4],F16[7 : 4] ) ;
assign F17[11 : 8] = ECMP( F15[11 : 8],F16[11 : 8] ) ;
assign F17[15 : 12] = ECMP( F15[15 : 12],F16[15 : 12] ) ;
assign F17[19 : 16] = ECMP( F15[19 : 16],F16[19 : 16] ) ;
assign F17[23 : 20] = ECMP( F15[23 : 20],F16[23 : 20] ) ;
assign F17[27 : 24] = ECMP( F15[27 : 24],F16[27 : 24] ) ;
assign F17[31 : 28] = ECMP( F15[31 : 28],F16[31 : 28] ) ;


assign F18[63:56] = F1[63:56]^F14[63:56] ;
assign F18[55:48] = F1[55:48]^F14[55:48] ;
assign F18[47:40] = F1[47:40]^F14[47:40] ;
assign F18[39:32] = F1[39:32]^F14[39:32] ;
assign F18[31:24] = F1[31:24]^F14[31:24] ;
assign F18[23:16] = F1[23:16]^F14[23:16] ;
assign F18[15:8] = F1[15:8]^F14[15:8] ;
assign F18[7:0] = F1[7:0]^F14[7:0] ;


assign F19[3 : 0] = F3[35 : 32]^F16[3 : 0] ;
assign F19[7 : 4] = F3[39 : 36]^F16[7 : 4] ;
assign F19[11 : 8] = F3[43 : 40]^F16[11 : 8] ;
assign F19[15 : 12] = F3[47 : 44]^F16[15 : 12] ;
assign F19[19 : 16] = F3[51 : 48]^F16[19 : 16] ;
assign F19[23 : 20] = F3[55 : 52]^F16[23 : 20] ;
assign F19[27 : 24] = F3[59 : 56]^F16[27 : 24] ;
assign F19[31 : 28] = F3[63 : 60]^F16[31 : 28] ;


assign F20[3 : 0] = PARITY( F18[63:56] ) ;
assign F20[7 : 4] = PARITY( F18[55:48] ) ;
assign F20[11 : 8] = PARITY( F18[47:40] ) ;
assign F20[15 : 12] = PARITY( F18[39:32] ) ;
assign F20[19 : 16] = PARITY( F18[31:24] ) ;
assign F20[23 : 20] = PARITY( F18[23:16] ) ;
assign F20[27 : 24] = PARITY( F18[15:8] ) ;
assign F20[31 : 28] = PARITY( F18[7:0] ) ;


assign F21[3 : 0] = ECMP( F19[3 : 0],F20[3 : 0] ) ;
assign F21[7 : 4] = ECMP( F19[7 : 4],F20[7 : 4] ) ;
assign F21[11 : 8] = ECMP( F19[11 : 8],F20[11 : 8] ) ;
assign F21[15 : 12] = ECMP( F19[15 : 12],F20[15 : 12] ) ;
assign F21[19 : 16] = ECMP( F19[19 : 16],F20[19 : 16] ) ;
assign F21[23 : 20] = ECMP( F19[23 : 20],F20[23 : 20] ) ;
assign F21[27 : 24] = ECMP( F19[27 : 24],F20[27 : 24] ) ;
assign F21[31 : 28] = ECMP( F19[31 : 28],F20[31 : 28] ) ;


assign F22[127:120] = F18[63:56] ;
assign F22[119:112] = F18[55:48] ;
assign F22[111:104] = F18[47:40] ;
assign F22[103:96] = F18[39:32] ;
assign F22[95:88] = F18[31:24] ;
assign F22[87:80] = F18[23:16] ;
assign F22[79:72] = F18[15:8] ;
assign F22[71:64] = F18[7:0] ;
assign F22[63:56] = F1[127:120] ;
assign F22[55:48] = F1[119:112] ;
assign F22[47:40] = F1[111:104] ;
assign F22[39:32] = F1[103:96] ;
assign F22[31:24] = F1[95:88] ;
assign F22[23:16] = F1[87:80] ;
assign F22[15:8] = F1[79:72] ;
assign F22[7:0] = F1[71:64] ;


assign F23[3 : 0] = F20[3 : 0] ;
assign F23[7 : 4] = F20[7 : 4] ;
assign F23[11 : 8] = F20[11 : 8] ;
assign F23[15 : 12] = F20[15 : 12] ;
assign F23[19 : 16] = F20[19 : 16] ;
assign F23[23 : 20] = F20[23 : 20] ;
assign F23[27 : 24] = F20[27 : 24] ;
assign F23[31 : 28] = F20[31 : 28] ;
assign F23[35 : 32] = F3[3 : 0] ;
assign F23[39 : 36] = F3[7 : 4] ;
assign F23[43 : 40] = F3[11 : 8] ;
assign F23[47 : 44] = F3[15 : 12] ;
assign F23[51 : 48] = F3[19 : 16] ;
assign F23[55 : 52] = F3[23 : 20] ;
assign F23[59 : 56] = F3[27 : 24] ;
assign F23[63 : 60] = F3[31 : 28] ;


assign F24[3 : 0] = PARITY( F22[127:120] ) ;
assign F24[7 : 4] = PARITY( F22[119:112] ) ;
assign F24[11 : 8] = PARITY( F22[111:104] ) ;
assign F24[15 : 12] = PARITY( F22[103:96] ) ;
assign F24[19 : 16] = PARITY( F22[95:88] ) ;
assign F24[23 : 20] = PARITY( F22[87:80] ) ;
assign F24[27 : 24] = PARITY( F22[79:72] ) ;
assign F24[31 : 28] = PARITY( F22[71:64] ) ;
assign F24[35 : 32] = PARITY( F22[63:56] ) ;
assign F24[39 : 36] = PARITY( F22[55:48] ) ;
assign F24[43 : 40] = PARITY( F22[47:40] ) ;
assign F24[47 : 44] = PARITY( F22[39:32] ) ;
assign F24[51 : 48] = PARITY( F22[31:24] ) ;
assign F24[55 : 52] = PARITY( F22[23:16] ) ;
assign F24[59 : 56] = PARITY( F22[15:8] ) ;
assign F24[63 : 60] = PARITY( F22[7:0] ) ;


assign F25[3 : 0] = ECMP( F23[3 : 0],F24[3 : 0] ) ;
assign F25[7 : 4] = ECMP( F23[7 : 4],F24[7 : 4] ) ;
assign F25[11 : 8] = ECMP( F23[11 : 8],F24[11 : 8] ) ;
assign F25[15 : 12] = ECMP( F23[15 : 12],F24[15 : 12] ) ;
assign F25[19 : 16] = ECMP( F23[19 : 16],F24[19 : 16] ) ;
assign F25[23 : 20] = ECMP( F23[23 : 20],F24[23 : 20] ) ;
assign F25[27 : 24] = ECMP( F23[27 : 24],F24[27 : 24] ) ;
assign F25[31 : 28] = ECMP( F23[31 : 28],F24[31 : 28] ) ;
assign F25[35 : 32] = ECMP( F23[35 : 32],F24[35 : 32] ) ;
assign F25[39 : 36] = ECMP( F23[39 : 36],F24[39 : 36] ) ;
assign F25[43 : 40] = ECMP( F23[43 : 40],F24[43 : 40] ) ;
assign F25[47 : 44] = ECMP( F23[47 : 44],F24[47 : 44] ) ;
assign F25[51 : 48] = ECMP( F23[51 : 48],F24[51 : 48] ) ;
assign F25[55 : 52] = ECMP( F23[55 : 52],F24[55 : 52] ) ;
assign F25[59 : 56] = ECMP( F23[59 : 56],F24[59 : 56] ) ;
assign F25[63 : 60] = ECMP( F23[63 : 60],F24[63 : 60] ) ;


function [7:0]  PARITY ;
input [7:0]  a ;
reg [7:0]  b,c,d  ;
begin
b=a>>4;
c=a^b;
d=c&8'hf';
PARITY = d ;
end
endfunction




function [7:0]  BIT ;
input [7:0]  a,b ;
reg [7:0]  c,d,e,f,g,h  ;
begin
c=a>>8'h1';
d = b[c];
e=a&8'h1';
f=e<<8'h2';
g=d>>f;
h=g&8'hf';
BIT = h ;
end
endfunction




function [7:0]  ECMP ;
input [7:0]  a,b ;
reg [7:0]  r  ;
begin
r=a^b;
ECMP = r ;
end
endfunction


endmodule
module Camellia_Core (din, dout, KEY, sel);
input  [127:0] din, KEY;
input          sel;
output [127:0] dout;

wire [127:0]  F1 , F6 , F10 , F14 , F18 , F22 ,   F0 ;
wire [ 63 :0]  F2 , F3 , F4 , F5 , F7 , F8 , F9 , F11 , F12 , F13 , F15 , F16 , F17 , F19 , F20 , F21 , F23 , F24 , F25  ;


assign  F0 =  din ;


assign F1[127:120] = KEY1[127:120]^F0[127:120] ;
assign F1[119:112] = KEY1[119:112]^F0[119:112] ;
assign F1[111:104] = KEY1[111:104]^F0[111:104] ;
assign F1[103:96] = KEY1[103:96]^F0[103:96] ;
assign F1[95:88] = KEY1[95:88]^F0[95:88] ;
assign F1[87:80] = KEY1[87:80]^F0[87:80] ;
assign F1[79:72] = KEY1[79:72]^F0[79:72] ;
assign F1[71:64] = KEY1[71:64]^F0[71:64] ;
assign F1[63:56] = KEY1[63:56]^F0[63:56] ;
assign F1[55:48] = KEY1[55:48]^F0[55:48] ;
assign F1[47:40] = KEY1[47:40]^F0[47:40] ;
assign F1[39:32] = KEY1[39:32]^F0[39:32] ;
assign F1[31:24] = KEY1[31:24]^F0[31:24] ;
assign F1[23:16] = KEY1[23:16]^F0[23:16] ;
assign F1[15:8] = KEY1[15:8]^F0[15:8] ;
assign F1[7:0] = KEY1[7:0]^F0[7:0] ;


assign F2[3 : 0] = PARITY( F0[127:120] ) ;
assign F2[7 : 4] = PARITY( F0[119:112] ) ;
assign F2[11 : 8] = PARITY( F0[111:104] ) ;
assign F2[15 : 12] = PARITY( F0[103:96] ) ;
assign F2[19 : 16] = PARITY( F0[95:88] ) ;
assign F2[23 : 20] = PARITY( F0[87:80] ) ;
assign F2[27 : 24] = PARITY( F0[79:72] ) ;
assign F2[31 : 28] = PARITY( F0[71:64] ) ;
assign F2[35 : 32] = PARITY( F0[63:56] ) ;
assign F2[39 : 36] = PARITY( F0[55:48] ) ;
assign F2[43 : 40] = PARITY( F0[47:40] ) ;
assign F2[47 : 44] = PARITY( F0[39:32] ) ;
assign F2[51 : 48] = PARITY( F0[31:24] ) ;
assign F2[55 : 52] = PARITY( F0[23:16] ) ;
assign F2[59 : 56] = PARITY( F0[15:8] ) ;
assign F2[63 : 60] = PARITY( F0[7:0] ) ;


assign F3[3 : 0] = PARITY( KEY1[ 127:120 ]  )^F2[3 : 0] ;
assign F3[7 : 4] = PARITY( KEY1[ 119:112 ]  )^F2[7 : 4] ;
assign F3[11 : 8] = PARITY( KEY1[ 111:104 ]  )^F2[11 : 8] ;
assign F3[15 : 12] = PARITY( KEY1[ 103:96 ]  )^F2[15 : 12] ;
assign F3[19 : 16] = PARITY( KEY1[ 95:88 ]  )^F2[19 : 16] ;
assign F3[23 : 20] = PARITY( KEY1[ 87:80 ]  )^F2[23 : 20] ;
assign F3[27 : 24] = PARITY( KEY1[ 79:72 ]  )^F2[27 : 24] ;
assign F3[31 : 28] = PARITY( KEY1[ 71:64 ]  )^F2[31 : 28] ;
assign F3[35 : 32] = PARITY( KEY1[ 63:56 ]  )^F2[35 : 32] ;
assign F3[39 : 36] = PARITY( KEY1[ 55:48 ]  )^F2[39 : 36] ;
assign F3[43 : 40] = PARITY( KEY1[ 47:40 ]  )^F2[43 : 40] ;
assign F3[47 : 44] = PARITY( KEY1[ 39:32 ]  )^F2[47 : 44] ;
assign F3[51 : 48] = PARITY( KEY1[ 31:24 ]  )^F2[51 : 48] ;
assign F3[55 : 52] = PARITY( KEY1[ 23:16 ]  )^F2[55 : 52] ;
assign F3[59 : 56] = PARITY( KEY1[ 15:8 ]  )^F2[59 : 56] ;
assign F3[63 : 60] = PARITY( KEY1[ 7:0 ]  )^F2[63 : 60] ;


assign F4[3 : 0] = PARITY( F1[127:120] ) ;
assign F4[7 : 4] = PARITY( F1[119:112] ) ;
assign F4[11 : 8] = PARITY( F1[111:104] ) ;
assign F4[15 : 12] = PARITY( F1[103:96] ) ;
assign F4[19 : 16] = PARITY( F1[95:88] ) ;
assign F4[23 : 20] = PARITY( F1[87:80] ) ;
assign F4[27 : 24] = PARITY( F1[79:72] ) ;
assign F4[31 : 28] = PARITY( F1[71:64] ) ;
assign F4[35 : 32] = PARITY( F1[63:56] ) ;
assign F4[39 : 36] = PARITY( F1[55:48] ) ;
assign F4[43 : 40] = PARITY( F1[47:40] ) ;
assign F4[47 : 44] = PARITY( F1[39:32] ) ;
assign F4[51 : 48] = PARITY( F1[31:24] ) ;
assign F4[55 : 52] = PARITY( F1[23:16] ) ;
assign F4[59 : 56] = PARITY( F1[15:8] ) ;
assign F4[63 : 60] = PARITY( F1[7:0] ) ;


assign F5[3 : 0] = ECMP( F3[3 : 0],F4[3 : 0] ) ;
assign F5[7 : 4] = ECMP( F3[7 : 4],F4[7 : 4] ) ;
assign F5[11 : 8] = ECMP( F3[11 : 8],F4[11 : 8] ) ;
assign F5[15 : 12] = ECMP( F3[15 : 12],F4[15 : 12] ) ;
assign F5[19 : 16] = ECMP( F3[19 : 16],F4[19 : 16] ) ;
assign F5[23 : 20] = ECMP( F3[23 : 20],F4[23 : 20] ) ;
assign F5[27 : 24] = ECMP( F3[27 : 24],F4[27 : 24] ) ;
assign F5[31 : 28] = ECMP( F3[31 : 28],F4[31 : 28] ) ;
assign F5[35 : 32] = ECMP( F3[35 : 32],F4[35 : 32] ) ;
assign F5[39 : 36] = ECMP( F3[39 : 36],F4[39 : 36] ) ;
assign F5[43 : 40] = ECMP( F3[43 : 40],F4[43 : 40] ) ;
assign F5[47 : 44] = ECMP( F3[47 : 44],F4[47 : 44] ) ;
assign F5[51 : 48] = ECMP( F3[51 : 48],F4[51 : 48] ) ;
assign F5[55 : 52] = ECMP( F3[55 : 52],F4[55 : 52] ) ;
assign F5[59 : 56] = ECMP( F3[59 : 56],F4[59 : 56] ) ;
assign F5[63 : 60] = ECMP( F3[63 : 60],F4[63 : 60] ) ;


assign F6[63:56] = KEY2[63:56]^F1[127:120] ;
assign F6[55:48] = KEY2[55:48]^F1[119:112] ;
assign F6[47:40] = KEY2[47:40]^F1[111:104] ;
assign F6[39:32] = KEY2[39:32]^F1[103:96] ;
assign F6[31:24] = KEY2[31:24]^F1[95:88] ;
assign F6[23:16] = KEY2[23:16]^F1[87:80] ;
assign F6[15:8] = KEY2[15:8]^F1[79:72] ;
assign F6[7:0] = KEY2[7:0]^F1[71:64] ;


assign F7[3 : 0] = PARITY( KEY2[ 63:56 ]  )^F4[3 : 0] ;
assign F7[7 : 4] = PARITY( KEY2[ 55:48 ]  )^F4[7 : 4] ;
assign F7[11 : 8] = PARITY( KEY2[ 47:40 ]  )^F4[11 : 8] ;
assign F7[15 : 12] = PARITY( KEY2[ 39:32 ]  )^F4[15 : 12] ;
assign F7[19 : 16] = PARITY( KEY2[ 31:24 ]  )^F4[19 : 16] ;
assign F7[23 : 20] = PARITY( KEY2[ 23:16 ]  )^F4[23 : 20] ;
assign F7[27 : 24] = PARITY( KEY2[ 15:8 ]  )^F4[27 : 24] ;
assign F7[31 : 28] = PARITY( KEY2[ 7:0 ]  )^F4[31 : 28] ;


assign F8[3 : 0] = PARITY( F6[63:56] ) ;
assign F8[7 : 4] = PARITY( F6[55:48] ) ;
assign F8[11 : 8] = PARITY( F6[47:40] ) ;
assign F8[15 : 12] = PARITY( F6[39:32] ) ;
assign F8[19 : 16] = PARITY( F6[31:24] ) ;
assign F8[23 : 20] = PARITY( F6[23:16] ) ;
assign F8[27 : 24] = PARITY( F6[15:8] ) ;
assign F8[31 : 28] = PARITY( F6[7:0] ) ;


assign F9[3 : 0] = ECMP( F7[3 : 0],F8[3 : 0] ) ;
assign F9[7 : 4] = ECMP( F7[7 : 4],F8[7 : 4] ) ;
assign F9[11 : 8] = ECMP( F7[11 : 8],F8[11 : 8] ) ;
assign F9[15 : 12] = ECMP( F7[15 : 12],F8[15 : 12] ) ;
assign F9[19 : 16] = ECMP( F7[19 : 16],F8[19 : 16] ) ;
assign F9[23 : 20] = ECMP( F7[23 : 20],F8[23 : 20] ) ;
assign F9[27 : 24] = ECMP( F7[27 : 24],F8[27 : 24] ) ;
assign F9[31 : 28] = ECMP( F7[31 : 28],F8[31 : 28] ) ;


SubByte  SB1 ( F6[63:56] , F10[63:56] );
SubByte  SB2 ( F6[55:48] , F10[55:48] );
SubByte  SB3 ( F6[47:40] , F10[47:40] );
SubByte  SB4 ( F6[39:32] , F10[39:32] );
SubByte  SB5 ( F6[31:24] , F10[31:24] );
SubByte  SB6 ( F6[23:16] , F10[23:16] );
SubByte  SB7 ( F6[15:8] , F10[15:8] );
SubByte  SB8 ( F6[7:0] , F10[7:0] );


assign F11[3 : 0] = BIT( F6[63:56],PSBOX1 )^F8[3 : 0] ;
assign F11[7 : 4] = BIT( F6[55:48],PSBOX2 )^F8[7 : 4] ;
assign F11[11 : 8] = BIT( F6[47:40],PSBOX3 )^F8[11 : 8] ;
assign F11[15 : 12] = BIT( F6[39:32],PSBOX4 )^F8[15 : 12] ;
assign F11[19 : 16] = BIT( F6[31:24],PSBOX4 )^F8[19 : 16] ;
assign F11[23 : 20] = BIT( F6[23:16],PSBOX3 )^F8[23 : 20] ;
assign F11[27 : 24] = BIT( F6[15:8],PSBOX2 )^F8[27 : 24] ;
assign F11[31 : 28] = BIT( F6[7:0],PSBOX1 )^F8[31 : 28] ;


assign F12[3 : 0] = PARITY( F10[63:56] ) ;
assign F12[7 : 4] = PARITY( F10[55:48] ) ;
assign F12[11 : 8] = PARITY( F10[47:40] ) ;
assign F12[15 : 12] = PARITY( F10[39:32] ) ;
assign F12[19 : 16] = PARITY( F10[31:24] ) ;
assign F12[23 : 20] = PARITY( F10[23:16] ) ;
assign F12[27 : 24] = PARITY( F10[15:8] ) ;
assign F12[31 : 28] = PARITY( F10[7:0] ) ;


assign F13[3 : 0] = ECMP( F11[3 : 0],F12[3 : 0] ) ;
assign F13[7 : 4] = ECMP( F11[7 : 4],F12[7 : 4] ) ;
assign F13[11 : 8] = ECMP( F11[11 : 8],F12[11 : 8] ) ;
assign F13[15 : 12] = ECMP( F11[15 : 12],F12[15 : 12] ) ;
assign F13[19 : 16] = ECMP( F11[19 : 16],F12[19 : 16] ) ;
assign F13[23 : 20] = ECMP( F11[23 : 20],F12[23 : 20] ) ;
assign F13[27 : 24] = ECMP( F11[27 : 24],F12[27 : 24] ) ;
assign F13[31 : 28] = ECMP( F11[31 : 28],F12[31 : 28] ) ;


assign F14[63:56] = F10[7:0]^F10[15:8]^F10[23:16]^F10[39:32]^F10[47:40]^F10[63:56] ;
assign F14[55:48] = F10[7:0]^F10[15:8]^F10[31:24]^F10[39:32]^F10[55:48]^F10[63:56] ;
assign F14[47:40] = F10[7:0]^F10[23:16]^F10[31:24]^F10[47:40]^F10[55:48]^F10[63:56] ;
assign F14[39:32] = F10[15:8]^F10[23:16]^F10[31:24]^F10[39:32]^F10[47:40]^F10[55:48] ;
assign F14[31:24] = F10[7:0]^F10[15:8]^F10[23:16]^F10[55:48]^F10[63:56] ;
assign F14[23:16] = F10[7:0]^F10[15:8]^F10[31:24]^F10[47:40]^F10[55:48] ;
assign F14[15:8] = F10[7:0]^F10[23:16]^F10[31:24]^F10[39:32]^F10[47:40] ;
assign F14[7:0] = F10[15:8]^F10[23:16]^F10[31:24]^F10[39:32]^F10[63:56] ;


assign F15[3 : 0] = F12[31 : 28]^F12[27 : 24]^F12[23 : 20]^F12[15 : 12]^F12[11 : 8]^F12[3 : 0] ;
assign F15[7 : 4] = F12[31 : 28]^F12[27 : 24]^F12[19 : 16]^F12[15 : 12]^F12[7 : 4]^F12[3 : 0] ;
assign F15[11 : 8] = F12[31 : 28]^F12[23 : 20]^F12[19 : 16]^F12[11 : 8]^F12[7 : 4]^F12[3 : 0] ;
assign F15[15 : 12] = F12[27 : 24]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[11 : 8]^F12[7 : 4] ;
assign F15[19 : 16] = F12[31 : 28]^F12[27 : 24]^F12[23 : 20]^F12[7 : 4]^F12[3 : 0] ;
assign F15[23 : 20] = F12[31 : 28]^F12[27 : 24]^F12[19 : 16]^F12[11 : 8]^F12[7 : 4] ;
assign F15[27 : 24] = F12[31 : 28]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[11 : 8] ;
assign F15[31 : 28] = F12[27 : 24]^F12[23 : 20]^F12[19 : 16]^F12[15 : 12]^F12[3 : 0] ;


assign F16[3 : 0] = PARITY( F14[63:56] ) ;
assign F16[7 : 4] = PARITY( F14[55:48] ) ;
assign F16[11 : 8] = PARITY( F14[47:40] ) ;
assign F16[15 : 12] = PARITY( F14[39:32] ) ;
assign F16[19 : 16] = PARITY( F14[31:24] ) ;
assign F16[23 : 20] = PARITY( F14[23:16] ) ;
assign F16[27 : 24] = PARITY( F14[15:8] ) ;
assign F16[31 : 28] = PARITY( F14[7:0] ) ;


assign F17[3 : 0] = ECMP( F15[3 : 0],F16[3 : 0] ) ;
assign F17[7 : 4] = ECMP( F15[7 : 4],F16[7 : 4] ) ;
assign F17[11 : 8] = ECMP( F15[11 : 8],F16[11 : 8] ) ;
assign F17[15 : 12] = ECMP( F15[15 : 12],F16[15 : 12] ) ;
assign F17[19 : 16] = ECMP( F15[19 : 16],F16[19 : 16] ) ;
assign F17[23 : 20] = ECMP( F15[23 : 20],F16[23 : 20] ) ;
assign F17[27 : 24] = ECMP( F15[27 : 24],F16[27 : 24] ) ;
assign F17[31 : 28] = ECMP( F15[31 : 28],F16[31 : 28] ) ;


assign F18[63:56] = F1[63:56]^F14[63:56] ;
assign F18[55:48] = F1[55:48]^F14[55:48] ;
assign F18[47:40] = F1[47:40]^F14[47:40] ;
assign F18[39:32] = F1[39:32]^F14[39:32] ;
assign F18[31:24] = F1[31:24]^F14[31:24] ;
assign F18[23:16] = F1[23:16]^F14[23:16] ;
assign F18[15:8] = F1[15:8]^F14[15:8] ;
assign F18[7:0] = F1[7:0]^F14[7:0] ;


assign F19[3 : 0] = F3[35 : 32]^F16[3 : 0] ;
assign F19[7 : 4] = F3[39 : 36]^F16[7 : 4] ;
assign F19[11 : 8] = F3[43 : 40]^F16[11 : 8] ;
assign F19[15 : 12] = F3[47 : 44]^F16[15 : 12] ;
assign F19[19 : 16] = F3[51 : 48]^F16[19 : 16] ;
assign F19[23 : 20] = F3[55 : 52]^F16[23 : 20] ;
assign F19[27 : 24] = F3[59 : 56]^F16[27 : 24] ;
assign F19[31 : 28] = F3[63 : 60]^F16[31 : 28] ;


assign F20[3 : 0] = PARITY( F18[63:56] ) ;
assign F20[7 : 4] = PARITY( F18[55:48] ) ;
assign F20[11 : 8] = PARITY( F18[47:40] ) ;
assign F20[15 : 12] = PARITY( F18[39:32] ) ;
assign F20[19 : 16] = PARITY( F18[31:24] ) ;
assign F20[23 : 20] = PARITY( F18[23:16] ) ;
assign F20[27 : 24] = PARITY( F18[15:8] ) ;
assign F20[31 : 28] = PARITY( F18[7:0] ) ;


assign F21[3 : 0] = ECMP( F19[3 : 0],F20[3 : 0] ) ;
assign F21[7 : 4] = ECMP( F19[7 : 4],F20[7 : 4] ) ;
assign F21[11 : 8] = ECMP( F19[11 : 8],F20[11 : 8] ) ;
assign F21[15 : 12] = ECMP( F19[15 : 12],F20[15 : 12] ) ;
assign F21[19 : 16] = ECMP( F19[19 : 16],F20[19 : 16] ) ;
assign F21[23 : 20] = ECMP( F19[23 : 20],F20[23 : 20] ) ;
assign F21[27 : 24] = ECMP( F19[27 : 24],F20[27 : 24] ) ;
assign F21[31 : 28] = ECMP( F19[31 : 28],F20[31 : 28] ) ;


assign F22[127:120] = F18[63:56] ;
assign F22[119:112] = F18[55:48] ;
assign F22[111:104] = F18[47:40] ;
assign F22[103:96] = F18[39:32] ;
assign F22[95:88] = F18[31:24] ;
assign F22[87:80] = F18[23:16] ;
assign F22[79:72] = F18[15:8] ;
assign F22[71:64] = F18[7:0] ;
assign F22[63:56] = F1[127:120] ;
assign F22[55:48] = F1[119:112] ;
assign F22[47:40] = F1[111:104] ;
assign F22[39:32] = F1[103:96] ;
assign F22[31:24] = F1[95:88] ;
assign F22[23:16] = F1[87:80] ;
assign F22[15:8] = F1[79:72] ;
assign F22[7:0] = F1[71:64] ;


assign F23[3 : 0] = F20[3 : 0] ;
assign F23[7 : 4] = F20[7 : 4] ;
assign F23[11 : 8] = F20[11 : 8] ;
assign F23[15 : 12] = F20[15 : 12] ;
assign F23[19 : 16] = F20[19 : 16] ;
assign F23[23 : 20] = F20[23 : 20] ;
assign F23[27 : 24] = F20[27 : 24] ;
assign F23[31 : 28] = F20[31 : 28] ;
assign F23[35 : 32] = F3[3 : 0] ;
assign F23[39 : 36] = F3[7 : 4] ;
assign F23[43 : 40] = F3[11 : 8] ;
assign F23[47 : 44] = F3[15 : 12] ;
assign F23[51 : 48] = F3[19 : 16] ;
assign F23[55 : 52] = F3[23 : 20] ;
assign F23[59 : 56] = F3[27 : 24] ;
assign F23[63 : 60] = F3[31 : 28] ;


assign F24[3 : 0] = PARITY( F22[127:120] ) ;
assign F24[7 : 4] = PARITY( F22[119:112] ) ;
assign F24[11 : 8] = PARITY( F22[111:104] ) ;
assign F24[15 : 12] = PARITY( F22[103:96] ) ;
assign F24[19 : 16] = PARITY( F22[95:88] ) ;
assign F24[23 : 20] = PARITY( F22[87:80] ) ;
assign F24[27 : 24] = PARITY( F22[79:72] ) ;
assign F24[31 : 28] = PARITY( F22[71:64] ) ;
assign F24[35 : 32] = PARITY( F22[63:56] ) ;
assign F24[39 : 36] = PARITY( F22[55:48] ) ;
assign F24[43 : 40] = PARITY( F22[47:40] ) ;
assign F24[47 : 44] = PARITY( F22[39:32] ) ;
assign F24[51 : 48] = PARITY( F22[31:24] ) ;
assign F24[55 : 52] = PARITY( F22[23:16] ) ;
assign F24[59 : 56] = PARITY( F22[15:8] ) ;
assign F24[63 : 60] = PARITY( F22[7:0] ) ;


assign F25[3 : 0] = ECMP( F23[3 : 0],F24[3 : 0] ) ;
assign F25[7 : 4] = ECMP( F23[7 : 4],F24[7 : 4] ) ;
assign F25[11 : 8] = ECMP( F23[11 : 8],F24[11 : 8] ) ;
assign F25[15 : 12] = ECMP( F23[15 : 12],F24[15 : 12] ) ;
assign F25[19 : 16] = ECMP( F23[19 : 16],F24[19 : 16] ) ;
assign F25[23 : 20] = ECMP( F23[23 : 20],F24[23 : 20] ) ;
assign F25[27 : 24] = ECMP( F23[27 : 24],F24[27 : 24] ) ;
assign F25[31 : 28] = ECMP( F23[31 : 28],F24[31 : 28] ) ;
assign F25[35 : 32] = ECMP( F23[35 : 32],F24[35 : 32] ) ;
assign F25[39 : 36] = ECMP( F23[39 : 36],F24[39 : 36] ) ;
assign F25[43 : 40] = ECMP( F23[43 : 40],F24[43 : 40] ) ;
assign F25[47 : 44] = ECMP( F23[47 : 44],F24[47 : 44] ) ;
assign F25[51 : 48] = ECMP( F23[51 : 48],F24[51 : 48] ) ;
assign F25[55 : 52] = ECMP( F23[55 : 52],F24[55 : 52] ) ;
assign F25[59 : 56] = ECMP( F23[59 : 56],F24[59 : 56] ) ;
assign F25[63 : 60] = ECMP( F23[63 : 60],F24[63 : 60] ) ;


function [7:0]  PARITY ;
input [7:0]  a ;
reg [7:0]  b,c,d  ;
begin
b=a>>4;
c=a^b;
d=c&8'hf';
PARITY = d ;
end
endfunction




function [7:0]  BIT ;
input [7:0]  a,b ;
reg [7:0]  c,d,e,f,g,h  ;
begin
c=a>>8'h1';
d = b[c];
e=a&8'h1';
f=e<<8'h2';
g=d>>f;
h=g&8'hf';
BIT = h ;
end
endfunction




function [7:0]  ECMP ;
input [7:0]  a,b ;
reg [7:0]  r  ;
begin
r=a^b;
ECMP = r ;
end
endfunction


endmodule
