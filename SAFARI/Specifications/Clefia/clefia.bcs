Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of IIT Madras  nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

//Remove this line and above copyright before executing the code




< begin >

  <declaration>

   SBOX0[256] {   57   49   d1   c6   2f   33   74   fb   95   6d   82   ea   0e   b0   a8   1c    28   d0   4b   92   5c   ee   85   b1   c4   0a   76   3d   63   f9   17   af    bf   a1   19   65   f7   7a   32   20   06   ce   e4   83   9d   5b   4c   d8    42   5d   2e   e8   d4   9b   0f   13   3c   89   67   c0   71   aa   b6   f5    a4   be   fd   8c   12   00   97   da   78   e1   cf   6b   39   43   55   26    30   98   cc   dd   eb   54   b3   8f   4e   16   fa   22   a5   77   09   61    d6   2a   53   37   45   c1   6c   ae   ef   70   08   99   8b   1d   f2   b4    e9   c7   9f   4a   31   25   fe   7c   d3   a2   bd   56   14   88   60   0b    cd   e2   34   50   9e   dc   11   05   2b   b7   a9   48   ff   66   8a   73    03   75   86   f1   6a   a7   40   c2   b9   2c   db   1f   58   94   3e   ed    fc   1b   a0   04   b8   8d   e6   59   62   93   35   7e   ca   21   df   47    15   f3   ba   7f   a6   69   c8   4d   87   3b   9c   01   e0   de   24   52    7b   0c   68   1e   80   b2   5a   e7   ad   d5   23   f4   46   3f   91   c9    6e   84   72   bb   0d   18   d9   96   f0   5f   41   ac   27   c5   e3   3a    81   6f   07   a3   79   f6   2d   38   1a   44   5e   b5   d2   ec   cb   90    9a   36   e5   29   c3   4f   ab   64   51   f8   10   d7   bc   02   7d   8e }

     SBOX1[256] {  6c   da   c3   e9   4e   9d   0a   3d   b8   36   b4   38   13   34   0c   d9    bf   74   94   8f   b7   9c   e5   dc   9e   07   49   4f   98   2c   b0   93    12   eb   cd   b3   92   e7   41   60   e3   21   27   3b   e6   19   d2   0e    91   11   c7   3f   2a   8e   a1   bc   2b   c8   c5   0f   5b   f3   87   8b    fb   f5   de   20   c6   a7   84   ce   d8   65   51   c9   a4   ef   43   53    25   5d   9b   31   e8   3e   0d   d7   80   ff   69   8a   ba   0b   73   5c    6e   54   15   62   f6   35   30   52   a3   16   d3   28   32   fa   aa   5e    cf   ea   ed   78   33   58   09   7b   63   c0   c1   46   1e   df   a9   99    55   04   c4   86   39   77   82   ec   40   18   90   97   59   dd   83   1f    9a   37   06   24   64   7c   a5   56   48   08   85   d0   61   26   ca   6f    7e   6a   b6   71   a0   70   05   d1   45   8c   23   1c   f0   ee   89   ad    7a   4b   c2   2f   db   5a   4d   76   67   17   2d   f4   cb   b1   4a   a8    b5   22   47   3a   d5   10   4c   72   cc   00   f9   e0   fd   e2   fe   ae    f8   5f   ab   f1   1b   42   81   d6   be   44   29   a6   57   b9   af   f2    d4   75   66   bb   68   9f   50   02   01   3c   7f   8d   1a   88   bd   ac    f7   e4   79   96   a2   fc   6d   b2   6b   03   e1   2e   7d   14   95   1d }

    KEY[8] {2b 7e 15 16 28 ae d2 a6 }

  </declaration>

  <operations>

      <func> < F_MUL2 ( a ) >
        < h : { a : F_RS ( a , 7 ) } >
        < t : { a : F_LS ( a , 1 ) } >
        < n : { h : F_MUL ( h , '0x1d' ) } >
        < m : { ( n , t ) : F_XOR ( n , t ) } >
        ret m
      </func>

      <func> < F_MUL4 ( a ) >
        < x : { a : F_MUL2 ( a ) } >
        < t : {  x : F_MUL2 ( x ) } >
        ret t
      </func>

     <func> < F_MUL6 ( a ) >
        < x : { a : F_MUL4 ( a ) } >
        < y : {  a : F_MUL2 ( a ) } >
	< t :  { ( x , y ) : F_XOR ( x ,y ) }>
        ret t
      </func>

      <func> < F_MUL8 ( a ) >
        < x : { a : F_MUL4 ( a ) } >
        < t : {  x : F_MUL2 ( x ) } >
        ret t
      </func>

	 <func> < F_MUL10 ( a ) >
        < x : { a : F_MUL8 ( a ) } >
        < y : {  a : F_MUL2 ( a ) } >
	< t :  { ( x , y ) : F_XOR ( x ,y ) }>
        ret t
      </func>



  </operations>




	< F1 > < linear > < KEYXOR > <

  		< F1[1] : { ( F0[5] ) : F_XOR ( F0[5] , F_LKUP( 1  , KEY ) )  } >
  		< F1[2] : { ( F0[6] ) : F_XOR ( F0[6] , F_LKUP( 2  , KEY ) )  } >
  		< F1[3] : { ( F0[7] ) : F_XOR ( F0[7] , F_LKUP( 3  , KEY ) )  } >
  		< F1[4] : { ( F0[8] ) : F_XOR ( F0[8] , F_LKUP( 4  , KEY ) )  } >
  		< F1[5] : { ( F0[13] ) : F_XOR ( F0[13] , F_LKUP( 5  , KEY ) )  } >
  		< F1[6] : { ( F0[14] ) : F_XOR ( F0[14] , F_LKUP( 6  , KEY ) )  } >
  		< F1[7] : { ( F0[15] ) : F_XOR ( F0[15] , F_LKUP( 7  , KEY ) )  } >
  		< F1[8] : { ( F0[16] ) : F_XOR ( F0[16] , F_LKUP( 8  , KEY ) )  } >

  	/>
      < F2 > < nonlinear > < SUBBYTE > <

     	 < F2[1] : { ( F1[1] ) : F_LKUP( F1[1]  , SBOX0 ) } >
    	  < F2[2] : { ( F1[2] ) : F_LKUP( F1[2]  , SBOX1 ) } >
  	    < F2[3] : { ( F1[3] ) : F_LKUP(  F1[3]  , SBOX0 ) } >
  	    < F2[4] : { ( F1[4] ) : F_LKUP( F1[4]  , SBOX1 ) } >
 	     < F2[5] : { ( F1[5] ) : F_LKUP( F1[5]  , SBOX1 ) } >
  	    < F2[6] : { ( F1[6] ) : F_LKUP( F1[6]  , SBOX0 ) } >
    	  < F2[7] : { ( F1[7] ) : F_LKUP( F1[7]  , SBOX1 ) } >
   	   < F2[8] : { ( F1[8] ) : F_LKUP( F1[8]  , SBOX0 ) } >

    />

< F3 > < linear > < MDS > <
		< F3[1] : { ( F2[1] , F2[2] , F2[3] , F2[4] ) : F_XOR ( F_XOR ( F_MUL6 ( F2[4] ) , F_MUL4 ( F2[3] ) ) , F_XOR ( F_MUL2 ( F2[2] )  ,  F2[1] ) )  } >
		< F3[2] : { ( F2[1] , F2[2] , F2[3] , F2[4] ) : F_XOR ( F_XOR ( F_MUL6 ( F2[3] ) , F_MUL4 ( F2[4] ) ) , F_XOR ( F_MUL2 ( F2[1] )  , F2[2] ) )  } >
		< F3[3] : { ( F2[1] , F2[2] , F2[3] , F2[4] ) :  F_XOR ( F_XOR ( F_MUL6 ( F2[2] ) , F_MUL4 ( F2[1] ) ) , F_XOR ( F_MUL2 ( F2[4] )  , F2[3] ) )    } >
		< F3[4] : { ( F2[1] , F2[2] , F2[3] , F2[4] ) :  F_XOR ( F_XOR ( F_MUL6 ( F2[1] ) , F_MUL4 ( F2[2] ) ) , F_XOR ( F_MUL2 ( F2[3] )  , F2[4] ) )  } >
		< F3[5] : { ( F2[5] , F2[6] , F2[7] , F2[8] ) : F_XOR ( F_XOR ( F_MUL10 ( F2[8] ) , F_MUL2 ( F2[7] ) ) , F_XOR ( F_MUL8 ( F2[6] )  , F2[5] ) )  } >
		< F3[6] : { ( F2[5] , F2[6] , F2[7] , F2[8] ) :  F_XOR ( F_XOR ( F_MUL10 ( F2[7] ) , F_MUL2 ( F2[8] ) ) , F_XOR ( F_MUL8 ( F2[5] )  , F2[6] ) )  } >
		< F3[7] : { ( F2[5] , F2[6] , F2[7] , F2[8] ) :  F_XOR ( F_XOR ( F_MUL10 ( F2[6] ) , F_MUL2 ( F2[5] ) ) , F_XOR ( F_MUL8 ( F2[8] )  , F2[7] ) )    } >
		< F3[8] : { ( F2[5] , F2[6] , F2[7] , F2[8] ) : F_XOR ( F_XOR ( F_MUL10 ( F2[5] ) , F_MUL2 ( F2[6] ) ) , F_XOR ( F_MUL8 ( F2[7] )  , F2[8] ) ) } >
	/>

< F4 > < linear > < DXOR > <
  		< F4[1] : { ( F3[1] , F0[5] ) : F_XOR ( F3[1] , F0[5] ) } >
  		< F4[2] : { ( F3[2] , F0[6] ) : F_XOR ( F3[2] , F0[6] ) } >
  		< F4[3] : { ( F3[3] , F0[7] ) : F_XOR ( F3[3] , F0[7] ) } >
  		< F4[4] : { ( F3[4] , F0[8] ) : F_XOR ( F3[4] , F0[8] ) } >
  		< F4[5] : { ( F3[5]  , F0[13]  ) : F_XOR ( F3[5] , F0[13] ) } >
  		< F4[6] : { ( F3[6] , F0[14]  ) : F_XOR ( F3[6] , F0[14] ) } >
  		< F4[7] : { ( F3[7] , F0[15]  ) : F_XOR ( F3[7] , F0[15] ) } >
  		< F4[8] : { ( F3[8] , F0[16]  ) : F_XOR ( F3[8] , F0[16] ) } >
  	/>

  < F5 > < linear > < SWAP > <
  		< F5[1] : { ( F4[1] )  } >
  		< F5[2] : { ( F4[2] )  } >
  		< F5[3] : { ( F4[3] ) } >
  		< F5[4] : { ( F4[4] ) } >
  		< F5[5] : { ( F0[9] ) } >
  		< F5[6] : { ( F0[10] ) } >
  		< F5[7] : { ( F0[11] ) } >
  		< F5[8] : { ( F0[12] ) } >
  		< F5[9] : { ( F4[5] ) } >
  		< F5[10] : { ( F4[6] ) } >
  		< F5[11] : { ( F4[7] } >
  		< F5[12] : { ( F4[8] ) } >
  		< F5[13] : { ( F0[1] ) } >
  		< F5[14] : { ( F0[2] ) } >
  		< F5[15] : { ( F0[3] ) } >
  		< F5[16] : { ( F0[4] ) } >
  	/>


  < end >
