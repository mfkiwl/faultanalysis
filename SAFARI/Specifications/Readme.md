Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of IIT Madras  nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------





This file contains some sample block cipher specifications (BCS) for three block 
ciphers: namely AES, CLEFIA, and CAMEILLIA. The BCS language is considerably inspired
by html and captures the various operations of the cipher at a high level.`

Use the following template to  write your own Block cipher Specification (BCS)


〈begin〉

	〈lookups〉

		//Add lookup tables e.g. S-Box, Keys etc.
		KEY[16] {2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c}
		···
	〈/lookups〉

	〈operations〉
		//Add user defined operations
		
		//<func> < operation_name ( arguments ) >
		<func> < F_MUL2 ( a ) >
			// expressions (in same format as given in the cipher body) 
			< h : { a : F_RS ( a , 7 ) } >
			...
			//return value
			ret m
		</func>
		···
		
	〈/operations〉

	//Add cipher functions
	//〈function〉〈linear/nonlinear〉〈TYPE〉〈
	〈F1〉〈linear〉〈KEYXOR〉〈
		//< subfuction : { ( Dependant subfuctions separated by comma ) : Expression } >
		< F1[1] : { ( F0[1] ) : F_XOR ( F0[1] , F_LKUP( 1  , KEY ) )  } >
		···
	/〉

	··· ··· ···
〈end〉

Notes: 
1) The cipher function is divided into the subfunctions. The size of the subfunction will be the minimum size of non-linear function.

2) Each operation in the block cipher is represented as a fucntion in specification

3) To Write expression premetive operations can be used. Premitive operations are :  XOR, AND, OR, LS, RS, MUL.  

4) Any fuction which is some complex combination of premitive operations can be defined in <operations> module. 

5) 	Function "TYPE" can be following:
	KEYXOR - XORing with Key;
	SUBBYTE - Nonlinear Substitution;
	SWAP - Changing of place of subfuctions;
	MDS - Multiplication with MDS matrix;
	DXOR - Linear transformation or diffusion i.e. XORing two subfuctions

6) Every function should be written in form of "F"<function index> e.g. First function will be F1

7) Every subfuction should be written in form of "F"<function index>[<subfuction index>] e.g. for fuction F1, it will be like F1[1], F1[2]... 

