"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:

This is the main function for adding different countermeasures to the specification (BCS)
--------------------------------------------------------------------------------------------------
"""


import sys
import os.path
import store_operands as so
import parse_specification as ps
# import synthesis_test as st
# import generate_parity as po
import generate_redundant as po
# import generate_parity_4bits as po
# import generate_parity_2bits as po
import copy_file as cf
#import check_mds as cm
import addExtraFunction_gen as ef

irr_poly = '0x1d'
# mds_matrix = [[[2,1,1,3], [3,2,1,1], [1,3,2,1], [1,1,3,2]]] #for AES
mds_matrix = [[[1,2,4,6] , [2,1,6,4] , [4,6,1,2] , [6,4,2,1]] , [[1,8,2,10] , [8,1,10,2] , [2,10,1,8] , [10,2,8,1]]] # For Clefia
# mds_matrix = []
if __name__ == '__main__':
    expr = ps.Syntax()
    if not os.path.exists("out"):
        os.makedirs("out")

    input_file = sys.argv[1]
    cf.copy_file()
    # # spec = open("/media/indrani/Indrani/Fault Attack/Countermeasure papers/Spec/modified_aes_redundancy_testfile.txt", "r")
    spec = open(input_file, "r")

    # spec = open("modified_aes_parity_2bit", "r")
    expr.parseFile( spec )
    # print spec, "->", expr.parseFile( spec )
    spec.close()
    # st.writeforRounds(so.depstack , so.dep_var_lst, so.func_linearity)

    ef.AddAllFunctions(mds_matrix)
    po.process(so.depstack , so.dep_var_lst, so.func_linearity)
    f = open("./out/out_spec.bcs" , 'a')
    print >> f , "\n< end >"
    f.close()
    # # cm.checkMDS(so.depstack , so.dep_var_lst, so.func_linearity)

    # print so.roundFunctions
    # print so.new_defined_func
    # for x in so.depstack:
    #     for y in so.depstack[x]:
    #         print x , y,  so.depstack[x][y]
