"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:

This code adds redundancy based countermeausre to the specification
--------------------------------------------------------------------------------------------------
"""




import re
import store_operands as so


redundant_dep = dict()
extra_function_added = 0
extra_function = dict()
vulnarable_function = [1,2,3,4,5]


# Handels dependency functions, prints functionality of dependency
def opHandler (var , var_list):
    # print var_list
    if str(var) in ( "XOR", "LKUP"):
        b = var_list.pop()
        a = var_list.pop()
        newstr = "F_" + var + " ( " + a + " , " + b + " )"
        # print newstr
        var_list.append(newstr)
    elif str(var) in  so.new_defined_func:
        a = var_list.pop()
        newstr = "F_" + var + " ( " + a + " )"
        # print newstr
        var_list.append(newstr)


# increment function number based on extra function added for redundant round
def processForRedundant(n_var,ex):
    n = map(int, re.findall(r'\d+', n_var))
    # print "Here" , n , n_var
    if len(n)>1 and extra_function_added >0:
        c_var = "F"+str(int(n[0])+extra_function[n[0]-1] +ex ) + "["+ str(n[1]) + "]"
        # print c_var
        return c_var
    else:
        return n_var

# increment function number based on extra function added on original round
def processForOriginal(n_var,ex):
    n = map(int, re.findall(r'\d+', n_var))
    # print "Here" , n , n_var
    if len(n)>1 and extra_function_added >0:
        if n[0] ==0:
            c_var = "F"+str(int(n[0])+extra_function[n[0]-1]+ex) + "["+ str(n[1]) + "]"
        else:
            c_var = "F"+str(int(n[0])+extra_function[n[0]-1]+ex+1) + "["+ str(n[1]) + "]"
        # print c_var
        return c_var
    else:
        return n_var


# Add redundant round and print
def addRedundant(x, dep_lst , dep_var , f_lin , ex):

    f2 = open("./out/out_spec.bcs" , 'a')

    temp_fname = "\n< F"+ str(int(x)+1+ extra_function_added ) + " > < " + f_lin[0][x] + " > < " + f_lin[1][x] + " > <"
    print >> f2, temp_fname
    for y in dep_var[x]:
        temp_varholder = []
        if x in dep_lst.keys():
            if y in dep_lst[x].keys():
                for z in dep_lst[x][y]:
                    if z in so.defined_func or z in so.new_defined_func:
                        opHandler(z , temp_varholder)
                    else:
                        n_z = processForRedundant (z ,ex)
                        temp_varholder.append(n_z)
        temp_string = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"
        # print temp_string
        var_string = '( '
        if len(dep_var[x][y]) > 1:
            for z in xrange(0,len(dep_var[x][y])):
                n_z = processForRedundant(dep_var[x][y][z] , ex )
                comma = ' ' if z == len(dep_var[x][y])-1 else ' ,'
                var_string = var_string + n_z + comma
        else:
            n_z = processForRedundant(dep_var[x][y][0],ex)
            var_string = var_string + n_z
        var_string = var_string + ' )'
        if len(temp_varholder) > 0:
            # print "here"
            print >> f2 , "\t< " , temp_string ,   " : { " , var_string , " : ", temp_varholder[0] , " } >"
        else:
            # print "here1"
            print  >> f2, "\t< " , temp_string ,   " : { " , var_string ,  " } >"
    print >> f2, "/>"
    f2.close()



# Add comparison between redundant and original
def addComparison(x , dep_var):
    f2 = open("./out/out_spec.bcs" , 'a')
    temp_fname = "\n< F"+ str(int(x)+1+ extra_function_added ) + " > <  linear > < CMP > <"
    print >> f2 , temp_fname
    for y in dep_var[x]:
        temp_string = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"
        var_string = '( F' + str(x+extra_function_added-1)+ "[" +  str(y) + "] , F" +  str(x+extra_function_added) + "[" +  str(y) + "] )"
        tem_func = " : F_ECMP " + var_string
        # print "< " , temp_string ,   " : { " , var_string , " : ", temp_varholder[0] , " } >"
        print >> f2,  "\t< " , temp_string ,   " : { " , var_string , tem_func,  " } >"
    print >> f2 , "/>"
    f2.close()



# print original round
def process(dep_lst , dep_var , f_lin):
    f3 = open("./out/out_spec.bcs" , 'a')
    # global defined_func
    global extra_function_added
    add_function = 0
    extra_function[0]=0
    extra_function[-1]=0
    # print defined_func
    for x in dep_var:
        extra_function[x] = 0
        function_between_rounds =0
        if x in vulnarable_function:
            add_function = 1
        if extra_function_added > 0:
            x_N = x + extra_function_added
        else:
            x_N = x
        temp_fname = "\n< F"+ str(int(x_N)) + " > < " + f_lin[0][x] + " > < " + f_lin[1][x] + " > <"
        f3 = open("./out/out_spec.bcs" , 'a')
        print >> f3 , temp_fname
        # print "Index: ",x_N
        for y in dep_var[x]:
            # print "SubIndex: ",y
            temp_varholder = []
            if x in dep_lst.keys():
                if y in dep_lst[x].keys():
                    # print "y is there" ,dep_lst[x][y]
                # print "Inside"
                    for z in dep_lst[x][y]:
                        # print z
                        if z in so.defined_func or z in so.new_defined_func:
                            opHandler(z , temp_varholder)

                            # print "match"
                        else:
                            n_z = processForOriginal (z, function_between_rounds)
                            temp_varholder.append(n_z)
            temp_string = "F" + str(x_N) + "[" + str(y) + "]"
            # print "Leftvariable ", temp_string
            # print "checking", dep_var[x][y]
            var_string = '( '
            if len(dep_var[x][y]) > 1:
                for z in xrange(0, len(dep_var[x][y])):
                    n_z = processForOriginal (dep_var[x][y][z], function_between_rounds)
                    comma = ' ' if z == len(dep_var[x][y])-1 else ' ,'
                    var_string = var_string + n_z + comma
            else:
                n_z = processForOriginal (dep_var[x][y][0],function_between_rounds)
                var_string = var_string + n_z
            var_string = var_string + ' )'
            if len(temp_varholder) > 0:
                # print "here"
                print >> f3, "\t< " , temp_string ,   " : { " , var_string , " : ", temp_varholder[0] , " } >"
            else:
                # print "here1"
                print >> f3, "\t< " , temp_string ,   " : { " , var_string ,  " } >"
        print >> f3, "\n/>"
        f3.close()
        if add_function == 1:
            add_function = 0
# # Commented redundancy added part.... To add those uncomment below lines
            addRedundant(x,dep_lst , dep_var , f_lin,function_between_rounds)
            function_between_rounds =function_between_rounds +1
            extra_function_added = extra_function_added +1
            addComparison(x, dep_var)
            function_between_rounds =function_between_rounds +1
            extra_function_added = extra_function_added +1
            extra_function[x]=extra_function_added

def AddExtraFunction():
    f1 = open("./out/out_spec.bcs" , 'a')
    print >> f1,  "\n\t<func> < F_ECMP ( a , b ) > "
    print >>  f1 ,  "\t  < r : { (a , b ) : F_XOR ( a , b ) } >"
    print >> f1 ,  "\t  < RET : { r : F_IZ ( r ) } >"
    print >> f1 , "\t</func>\n"
    print >> f1,  " </operations>\n"
    f1.close()
