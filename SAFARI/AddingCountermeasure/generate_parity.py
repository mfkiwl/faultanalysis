"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:

This code adds 1 but parity per 8 bits in the BCS
--------------------------------------------------------------------------------------------------
"""


import re
import store_operands as so
mds_matrix = [[2,1,1,3], [3,2,1,1], [1,3,2,1], [1,1,3,2]]
# mds_matrix = [[1,2,4,6], [2,1,6,4], [4,6,1,2], [6,4,2,1]]

extra_function_added = 0
extra_function = dict()
function_between_rounds = 0
vulnarable_function = [1,2,3,4,5,6]


# for dependent function expression
def opHandler (var , var_list):
    if str(var) in ( "XOR", "LKUP"):
        b = var_list.pop()
        a = var_list.pop()
        newstr = "F_" + var + " ( " + a + " , " + b + " )"
        var_list.append(newstr)
    elif str(var) in so.new_defined_func:
        a = var_list.pop()
        newstr = "F_" + var + " ( " + a + " )"
        var_list.append(newstr)



def opHandlerp (typ, var , var_list):
    print typ
    if str(var) in ( "XOR"):
        b = var_list.pop()
        a = var_list.pop()
        newstr = "F_" + var + " ( " + a + " , " + b + " )"
        var_list.append(newstr)
    elif str(var) in ("LKUP") :
        b = var_list.pop()
        a = var_list.pop()
        if  typ not in ("SUBBYTE"):
            newstr = "F_PARITY ( " + "F_" + var + " ( " + a + " , " + b + " ) )"
        else:
            newstr =  "F_BIT" +  " ( " + a + " , " + b + " )"
        var_list.append(newstr)
    elif str(var) in so.new_defined_func:
        a = var_list.pop()
        newstr = "F_" + var + " ( " + a + " )"
        var_list.append(newstr)



# set correct index for dependent function for prediction functions
def processIndex(x,n_var, ex, p):
    n = map(int, re.findall(r'\d+', n_var))
    if len(n)>1 and extra_function_added >0:
        # print x , n_var , ex , n[0]
        # print "here",x+extra_function_added+1- ex
        c_var = "F"+str(n[0]+ extra_function[n[0]-1]+ex) + "["+ str(n[1]) + "]"
        return c_var
    else :
        if n_var in so.lookup and p==1:
            return  "P"+n_var
        else:
            return n_var



def processIndexp(x,n_var, ex, p):
    n = map(int, re.findall(r'\d+', n_var))
    if len(n)>1 and extra_function_added >0:
        if p==1:
            c_var = "F"+str(n[0]+ extra_function[n[0]-1]+ex) + "["+ str(n[1]) + "]"#_" + subs
        else:
            c_var = "F"+str((x)+extra_function_added+1- ex) + "["+ str(n[1]) + "]"#_" + subs
        return c_var
    else :
        # print "Here" ,n_var,p , so.lookup
        if n_var in so.lookup and p==1:
            # print "Here1"
            return  "P"+n_var
        else:
            return n_var#+"_" + subs


# Add prediction parity for key xor functions
def addXorParity(x,dep_var,dep_lst,f_lin):
    f2 = open("./out/out_spec.bcs" , 'a')
    if x == 1 :
        extra = 2
    else:
        extra = 3
    e =0
    for y in dep_var[x]:
        temp_varholder = []
        store_val = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"
        for z in  dep_lst[x][y]:
            if z in so.defined_func:
                opHandlerp(f_lin[1][x], z , temp_varholder)
            else:
                n_z = processIndexp (x,z,extra,1)
                temp_varholder.append(n_z)
            # print z

        load_val = '( '
        if len(dep_var[x][y]) >= 1:
            for z in xrange(0, len(dep_var[x][y])):
                n_z = processIndexp (x,dep_var[x][y][z], extra , 1)
                comma = ' ' if z == len(dep_var[x][y])-1 else ' ,'
                load_val = load_val + n_z + comma
        else:
            n_z = processIndexp (x,dep_var[x][y][0], function_between_rounds , 0)
            load_val = load_val + n_z
        load_val = load_val + ' )'
        # subs = "_" + subscript[i]
        # load_val = '( F' + str(x+extra_function_added+1- extra)+ "[" +  str(y) + "] )"
        print >> f2,  "\t< " , store_val ,     " : { " , load_val, ":", temp_varholder[0] ,  " } >"
        # print >> f2,  "\t< " , store_val , "_[4:7]" ,  " : { " , load_val, ":", temp_varholder[0] ,  " } >"

    print >> f2, "/>"
    f2.close()


# Add prediction parity for subbyte functions
def addSubbyteParity(x,dep_var,dep_lst,f_lin):
    f2 = open("./out/out_spec.bcs" , 'a')
    if x == 1 :
        extra = 2
    else:
        extra = 6
    for y in dep_var[x]:
        temp_varholder = []
        store_val = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"
        for z in  dep_lst[x][y]:
            if z in so.defined_func:
                opHandlerp(f_lin[1][x],z , temp_varholder)
            else:
                n_z = processIndexp (x,z,0,1)
                temp_varholder.append(n_z)
            # print z
        # subs = "_" + subscript[i]
        load_val = '( F' + str(x+extra_function_added+1- 3)+ "[" +  str(y) + "]"  + " , F" + str(x+extra_function_added+1- extra) + "[" +  str(y) +  "]"  +" )"
        xor_var = 'F' + str(x+extra_function_added+1- 3)+ "[" +  str(y) + "]"

        print >> f2,  "\t< " , store_val  ,   " : { " , load_val, ":", "F_XOR ( ", xor_var , "," , temp_varholder[0] ,  ") } >"


    print >> f2, "/>"
    f2.close()


# Add prediction parity for swap functions
def addSwapParity(x,dep_var):

    f2 = open("./out/out_spec.bcs" , 'a')
    if x == 1 :
        extra = 1
    else:
        extra = 3
    for y in dep_var[x]:

        # print processIndex(x,  dep_var[x][y][0] , extra , 1)
        store_val = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"
        load_val = processIndexp(x,  dep_var[x][y][0] , function_between_rounds+2 , 1)
        print >> f2,  "\t< " , store_val ,  " : { " , load_val,  " } >"

    print >> f2, "/>"
    f2.close()


# Add prediction parity for mds functions


def arrange(arrlist,y):
    retarr = [0,0,0,0]
    used = set()
    # print "y = ", y ,arrlist
    if (y-1)%4 == 0:
        return arrlist
    else:
        # print arrlist ,y
        for i in xrange(0, 4):
            # print "Checking for", mds_matrix[0][i]
            for j in xrange(0,4):
                # print mds_matrix[i][0] , mds_matrix[j][(y-1)%4]
                # print "used" , used
                if int(mds_matrix[i][0]) == int(mds_matrix[j][(y-1)%4]) and j not in used:
                    # print "match"
                    retarr[i] = arrlist[j]
                    used.add(j)
                    # arrlist[j]=0
                    break
        # print "\n" , retarr
        return retarr




def addMdsParity(x,dep_var):
    f2 = open("./out/out_spec.bcs" , 'a')

    if x == 1 :
        extra = 1
    else:
        extra = 3
    for y in dep_var[x]:
        temp = "( "
        temp_arr=[]
        for z in xrange(0, len(dep_var[x][y])):
			temp_arr.append(processIndex(x,  dep_var[x][y][z] , function_between_rounds , 1) )
        temp_arr =  arrange(temp_arr,y)
        temp = temp +  ', '.join(temp_arr)
        temp_arr=[]
        for z in xrange(0, len(dep_var[x][y])):
            temp_arr.append(processIndexp(x,  dep_var[x][y][z] , function_between_rounds+2 , 1))
        temp_arr = arrange(temp_arr,y)
        temp = temp + "," +  ', '.join(temp_arr)

        temp = temp + " )"
        # print "temp ", temp
            # print processIndex(x,  z , extra , 1)
        store_val = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"
        load_val = temp
        #funcname = ['F_GENPARITYMDS4' , 'F_GENPARITYMDS1' , 'F_GENPARITYMDS2' , 'F_GENPARITYMDS3']
        funcname = 'F_GENPARITYMDS'
        index = (y%4)
        fname = funcname# +   str(i)

        tem_func = " : " + fname  + load_val
        print >> f2,  "\t< " , store_val ,    " : { " , load_val , tem_func,  " } >"



    print >> f2, "/>"
    f2.close()


def addDiffusionXor(x,dep_var,dep_lst,f_lin):
    # subscript = ['[0]' , '[1]' ]
    f2 = open("./out/out_spec.bcs" , 'a')
    if x == 1 :
        extra = 1
    else:
        extra = 2
    e =0
    for y in dep_var[x]:
        # for i in xrange(0,len(subscript)):
        temp_varholder = []
        store_val = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"
        for z in  dep_lst[x][y]:
            if z in so.defined_func:
                opHandlerp(f_lin[1][x], z , temp_varholder)
            else:
                n_z = processIndexp (x,z,extra,1)
                temp_varholder.append(n_z)
            # print z

        load_val = '( '
        if len(dep_var[x][y]) >= 1:
            for z in dep_var[x][y]:
                n_z = processIndexp (x,z, extra , 1)
                load_val = load_val + n_z + " , "
        else:
            n_z = processIndexp (x,dep_var[x][y][0], function_between_rounds , 0)
            load_val = load_val + n_z
        load_val = load_val + ' )'
        # subs = "_" + subscript[i]
        # load_val = '( F' + str(x+extra_function_added+1- extra)+ "[" +  str(y) + "] )"
        print >> f2,  "\t< " , store_val ,    " : { " , load_val, ":", temp_varholder[0] ,  " } >"
        # print >> f2,  "\t< " , store_val , "_[4:7]" ,  " : { " , load_val, ":", temp_varholder[0] ,  " } >"

    print >> f2, "/>"
    f2.close()

# Add prediction units for functions
def addPredictor(x,dep_lst , dep_var , f_lin):
    # print "Addpredictor"
    f2 = open("./out/out_spec.bcs" , 'a')
    temp_fname = "\n< F"+ str(int(x)+1+ extra_function_added ) + " > < " + "linear" + " > < " + "PREDICTPARITY" + " > <"
    print >> f2, temp_fname
    f2.close()

    if f_lin[1][x] == "KEYXOR":
        # print >> f2 , "MATCH"
        addXorParity(x,dep_var,dep_lst, f_lin)
    elif f_lin[1][x] == "SUBBYTE":
        # print  "MATCH SB"
        addSubbyteParity(x,dep_var,dep_lst,f_lin)
    elif  f_lin[1][x] == "SWAP":
        addSwapParity(x,dep_var)
        # print  "MATCH SWAP"
    elif  f_lin[1][x] == "MDS":
        addMdsParity(x,dep_var)
        # print  "MATCH MDS"
    elif f_lin[1][x] == "DXOR":
        addDiffusionXor(x,dep_var,dep_lst,f_lin)



# Add parity calculation functions for all bytes
def addFindParity(x,dep_lst , dep_var , f_lin):
    subscript = ['[0:3]' , '[4:7]']
    f2 = open("./out/out_spec.bcs" , 'a')
    if x>0:
        temp_fname = "\n< F"+ str(int(x)+1+ extra_function_added ) + " > < " + "linear" + " > < " + "FINDPARITY" + " > <"
        print >> f2, temp_fname
        for y in dep_var[x]:
            store_val = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"#_[" + str(i) + "]"
            var_string = '( F' + str(x+extra_function_added- function_between_rounds)+ "[" +  str(y) + "] )"#_" + subscript[i] + " ) "
            tem_func = " : F_PARITY " + var_string
            print >> f2,  "\t< " , store_val ,   " : { " , var_string , tem_func,  " } >"
    else:
        temp_fname = "\n< F"+ str(int(x)+2+ extra_function_added ) + " > < " + "linear" + " > < " + "FINDPARITY" + " > <"
        print >> f2, temp_fname
        for y in xrange(1,17):
            store_val = "F" + str(x+2+extra_function_added) + "[" + str(y) + "]"#_[" + str(i) + "]"
            var_string = '( F' + str(x+extra_function_added- function_between_rounds)+ "[" +  str(y) + "] )"#_" + subscript[i] + " ) "
            tem_func = " : F_PARITY " + var_string
            print >> f2,  "\t< " , store_val ,   " : { " , var_string , tem_func,  " } >"

    print >> f2, "/>"
    f2.close()


# Add comparision functions for all bytes
def addComparison(x , dep_var):
    subscript = ['[0:3]' , '[4:7]']
    f2 = open("./out/out_spec.bcs" , 'a')
    temp_fname = "\n< F"+ str(int(x)+1+ extra_function_added ) + " > <  linear > < CMP > <"
    print >> f2 , temp_fname
    for y in dep_var[x]:
        # for i in xrange(0,len(subscript)):
        store_val = "F" + str(x+1+extra_function_added) + "[" + str(y) + "]"#_[" + str(i) + "]"
        var_string = '( F' + str(x+extra_function_added-1)+ "[" +  str(y) + "] , F" +  str(x+extra_function_added) + "[" +  str(y) + "] )"
        tem_func = " : F_ECMP " + var_string
        # print "< " , store_val ,   " : { " , var_string , " : ", temp_varholder[0] , " } >"
        print >> f2,  "\t< " , store_val ,   " : { " , var_string , tem_func,  " } >"
    print >> f2 , "/>"
    f2.close()




# print original round
def process(dep_lst , dep_var , f_lin):
    f3 = open("./out/out_spec.bcs" , 'a')
    # global defined_func
    global extra_function_added
    global function_between_rounds
    add_function = 0
    # print defined_func
    extra_function[0]=0
    extra_function[-1]=0
    for x in dep_var:
        function_between_rounds = 0
        # print "x",x
        extra_function[x]=0
        if x in vulnarable_function:
            add_function = 1
        if extra_function_added > 0:
            x_N = x + extra_function_added
        else:
            x_N = x
        temp_fname = "\n< F"+ str(int(x_N)) + " > < " + f_lin[0][x] + " > < " + f_lin[1][x] + " > <"
        f3 = open("./out/out_spec.bcs" , 'a')
        print >> f3 , temp_fname
        for y in dep_var[x]:
            temp_varholder = []
            if x in dep_lst.keys() and y in dep_lst[x].keys():
                for z in dep_lst[x][y]:
                    if z in so.defined_func or z in so.new_defined_func:
                        opHandler(z , temp_varholder)
                    else:
                        # print "function b/w" , x , y, function_between_rounds ,extra_function_added
                        n_z = processIndex (x,z, function_between_rounds , 0)
                        temp_varholder.append(n_z)
            store_val = "F" + str(x_N) + "[" + str(y) + "]"
            load_val = '( '
            if len(dep_var[x][y]) > 1:
                for z in xrange(0, len(dep_var[x][y])):
                    n_z = processIndex (x,dep_var[x][y][z], function_between_rounds , 0)
                    comma = ' ' if z == len(dep_var[x][y])-1 else ' ,'
                    load_val = load_val + n_z + comma
            else:
                # print "Here comes swap"
                n_z = processIndex (x,dep_var[x][y][0], function_between_rounds , 0)
                load_val = load_val + n_z
            load_val = load_val + ' )'
            if len(temp_varholder) > 0:
                print >> f3, "\t< " , store_val ,   " : { " , load_val , " : ", temp_varholder[0] , " } >"
            else:
                print >> f3, "\t< " , store_val ,   " : { " , load_val ,  " } >"
        print >> f3, "\n/>"
        f3.close()

        if add_function == 1:
            add_function = 0

            if x ==1:
                addFindParity(x-1,dep_lst , dep_var , f_lin)
                extra_function_added = extra_function_added +1
                function_between_rounds = function_between_rounds + 1
            addPredictor(x,dep_lst , dep_var , f_lin)
            extra_function_added = extra_function_added +1
            function_between_rounds = function_between_rounds + 1
            addFindParity(x,dep_lst , dep_var , f_lin)
            extra_function_added = extra_function_added +1
            function_between_rounds = function_between_rounds + 1
            addComparison(x, dep_var)
            extra_function_added = extra_function_added +1
            function_between_rounds = function_between_rounds + 1
            extra_function[x]=extra_function_added

    # print extra_function
