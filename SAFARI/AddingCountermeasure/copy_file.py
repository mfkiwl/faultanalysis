
"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:

This code copies the part from the old specification to the new where no changes needs to be made
--------------------------------------------------------------------------------------------------
"""


import sys, traceback


def copy_file():
    set = 0
    # with open("/media/indrani/Indrani/Fault Attack/Countermeasure papers/Spec/modified_aes_redundancy_testfile.bcs") as f:
    with open("test_spec.bcs") as f:
        with open("./out/out_spec.bcs", "w") as f1:
            for line in f:
                words= line.split()
                # print words
                if len(words) > 0 and words[0] == "</operations>":
                    # print "match"
                    set = 1
                # for words in line:
                #     print words
                if set !=1:
                    f1.write(line)
                    if len(words) > 0 and words[0] == "<declaration>":
                        f1.write("\n\t\tPKEY[16]\n")
                        f1.write("\t\tPSBOX[256]")
                else:
                     break
    f.close()
    f1.close()
