###	Input-Output Description of FEDS 
###       All Rights Reserved        
---------------------------------------------------------------------------------

### Inputs to FEDS

1. clefia.c includes the SUT used in the analysis

2. bcs.c contain the synthesised code of BCS representation of AES


**Values Need to be Defined**

 - Define Equivalence Mapping Values as 58 (line number in bcs.c), 10 (MAX_A), 73(line number in IMP1.c), 110 (MAX_E) (These values are found empirically)

 - eg : **fm.equivalence_mapping(58,10,73,110,code,file1,type_imp)**
	

