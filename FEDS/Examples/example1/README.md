###	Input-Output Description of FEDS 
###       All Rights Reserved        
---------------------------------------------------------------------------------

### Inputs to FEDS

1. IMP1.c includes the SUT used in the analysis

2. bcs.c contain the synthesised code of BCS representation of AES

The BCS representation and synthesis from BCS to C is given in **SAFARI** framework

**Values Need to be Defined**

 - Define Equivalence Mapping Values as 31 (line number in bcs.c), 20 (MAX_A), 195(line number in IMP1.c), 110 (MAX_E) (These values are found empirically)

 - eg : **fm.equivalence_mapping(31,20,195,110,code,file1,type_imp)**
	
**outs**

 - Folder contains all the exploitable instructions for the given implementation


### Intermediate Outputs 

**Intermediate Outputs of Algorithm 1 (Fault Mapping)**

1. First, the algorithm finds the Information Flow Graph for the BCS (Refer Algorithm 1 for more details). The output is present in **info.txt**. Each line in info.txt has the format: **X <--- Y**, which means that information from Y flows to X.

	- **49 <--- : 33 :** denotes information from sub-operation in line 33 to line 49 of BCS.
      i.e, Information flow from F0[1] to F1[1]

	- **95 <--- : 78 : 77 : 76 : 79 :** the sub-operation in line 95 has information flow from sub-operations defined in line 78, 77, 76, 79 of BCS.

 
2. The output of Algorithm 1 is **E_Map.txt**

	- File shows mapping from sub-operations in BCS to program-expressions in SUT. Format : **sub-operations --> program-expression**. Some examples of mappings are shown below:

	- **One-to-One mapping**

	- eg. 
	**A61 --> {   E197   }**,
   	**A62 --> {   E198   }**,
  	**A63 --> {   E199   }**,
   	**A64 --> {   E200   }**.

   	This means A61 maps to E197. i.e. sub-operation in line 61 of bcs.c maps to expression in line 197 of IMPxx.c


   	- **Merged Sub-operations**

	- eg. **A149 --> {   E327   }**,
    	      **A131 --> {   E327   }**.

   	  Implies the sub-operation A149 and A131 maps to program expression E327. Hence these sub-operations are merged in SUT to program-expression E327.

   	- **Arbitrary order**

	- eg. **A98 --> {   E249   }**,
    	      **A99 --> {   E255   }**,
    	      **A100 --> {   E262   }**,
    	      **A101 --> {   E270   }**.

   	    Implies A98 mapsto E249, A99 mapsto E255, A100 mapsto E262 and A101 mapsto E270. Here mapping is one-to-one, but follows an arbitrary order in SUT.

**Model Checker**

   - FEME is determined by FEMA (output of the HLE Tool given in file FEMA.txt)
       contains the exploitable byte and key bytes obtained.


**Algorithm 2 (Fault Evaluation)**
 - Takes E_Map (output of Algorithm 1), SUT (IMP1.c) and FEME as input.

 - The algorithm is implemented as an LLVM compiler parser that identifies vulnerable instructions based on FEME.



**Intermediate Outputs of Fault Evaluation**

1. xx.ll file contains the intermediate representation used by LLVM

   	- Our LLVM parser will take the IR.ll file and construct customized CFG for analysis
   	- More Details can be found here: [LLVM](https://llvm.org/)


2. **outs** contains all the exploitable instructions output of Algorithm 2. The output includes the vertex number of the CFG followed by a flag denoting the exploitability (Exploitable is 1 and Non-exploitable is 0)

   	- **7108 :  0** implies instruction 7108 in IR cannot induce a successful fault for the given implementation.

	- **7122 :  1 --> %conv2 = zext i8 %a to i32, !dbg !59** implies instruction 7122 in IR can induce oa successful fault, hence this instruction is exploitable.

