#include <stdint.h>
#include <string.h>
#include <sys/time.h>
#include<stdio.h>
#define NR 10		/* no. of rounds */
#define NK 4		/* no. of 32-bit words in key */
#define NB 4		/* no. of 32-bit words as columns in state */

#define all_1 (~0ULL)  

#define CHK_AFTER_ITERS (1UL<<25)

#define STOP_TAG 20
#define KEY_TAG 28
#define PLAIN_TAG 29
#define CIPHER_TAG 30
#define INPUT_END_TAG 31

typedef unsigned long long ULONG ;

extern ULONG 	bskey[NR+1][128];

extern void fnSbox(ULONG a[8]) ;
extern void fnEncryptBulk(const ULONG *plain, ULONG *cipher) ;
extern void fnExpandKey(unsigned char key[]);
#define __DBG 1
int i,j;
/* The plaintext, key and cipher text is of the form
 * 	x0-7		x32-39		x64-71		x96-103
 * 	x8-15		x40-47		x72-79		x104-111
 * 	x16-23		x48-55		x80-87		x112-119
 * 	x24-31		x56-63		x88-95		x120-127
 */
/* The AES SBOX look up table */
static unsigned char sbox[256] = {
//00   01   02   03   04   05   06   07   08   09   0A   0B   0C   0D   0E   0F
0x63,0x7C,0x77,0x7B,0xF2,0x6B,0x6F,0xC5,0x30,0x01,0x67,0x2B,0xFE,0xD7,0xAB,0x76, /*0*/
0xCA,0x82,0xC9,0x7D,0xFA,0x59,0x47,0xF0,0xAD,0xD4,0xA2,0xAF,0x9C,0xA4,0x72,0xC0, /*1*/
0xB7,0xFD,0x93,0x26,0x36,0x3F,0xF7,0xCC,0x34,0xA5,0xE5,0xF1,0x71,0xD8,0x31,0x15, /*2*/
0x04,0xC7,0x23,0xC3,0x18,0x96,0x05,0x9A,0x07,0x12,0x80,0xE2,0xEB,0x27,0xB2,0x75, /*3*/
0x09,0x83,0x2C,0x1A,0x1B,0x6E,0x5A,0xA0,0x52,0x3B,0xD6,0xB3,0x29,0xE3,0x2F,0x84, /*4*/
0x53,0xD1,0x00,0xED,0x20,0xFC,0xB1,0x5B,0x6A,0xCB,0xBE,0x39,0x4A,0x4C,0x58,0xCF, /*5*/
0xD0,0xEF,0xAA,0xFB,0x43,0x4D,0x33,0x85,0x45,0xF9,0x02,0x7F,0x50,0x3C,0x9F,0xA8, /*6*/
0x51,0xA3,0x40,0x8F,0x92,0x9D,0x38,0xF5,0xBC,0xB6,0xDA,0x21,0x10,0xFF,0xF3,0xD2, /*7*/
0xCD,0x0C,0x13,0xEC,0x5F,0x97,0x44,0x17,0xC4,0xA7,0x7E,0x3D,0x64,0x5D,0x19,0x73, /*8*/
0x60,0x81,0x4F,0xDC,0x22,0x2A,0x90,0x88,0x46,0xEE,0xB8,0x14,0xDE,0x5E,0x0B,0xDB, /*9*/
0xE0,0x32,0x3A,0x0A,0x49,0x06,0x24,0x5C,0xC2,0xD3,0xAC,0x62,0x91,0x95,0xE4,0x79, /*A*/
0xE7,0xC8,0x37,0x6D,0x8D,0xD5,0x4E,0xA9,0x6C,0x56,0xF4,0xEA,0x65,0x7A,0xAE,0x08, /*B*/
0xBA,0x78,0x25,0x2E,0x1C,0xA6,0xB4,0xC6,0xE8,0xDD,0x74,0x1F,0x4B,0xBD,0x8B,0x8A, /*C*/
0x70,0x3E,0xB5,0x66,0x48,0x03,0xF6,0x0E,0x61,0x35,0x57,0xB9,0x86,0xC1,0x1D,0x9E, /*D*/
0xE1,0xF8,0x98,0x11,0x69,0xD9,0x8E,0x94,0x9B,0x1E,0x87,0xE9,0xCE,0x55,0x28,0xDF, /*E*/
0x8C,0xA1,0x89,0x0D,0xBF,0xE6,0x42,0x68,0x41,0x99,0x2D,0x0F,0xB0,0x54,0xBB,0x16  /*F*/
};

/* The Inverse SBOX Look up table */
static unsigned char inv_sbox[256] = {
//00   01   02   03   04   05   06   07   08   09   0A   0B   0C   0D   0E   0F
0x52,0x09,0x6A,0xD5,0x30,0x36,0xA5,0x38,0xBF,0x40,0xA3,0x9E,0x81,0xF3,0xD7,0xFB, /*0*/
0x7C,0xE3,0x39,0x82,0x9B,0x2F,0xFF,0x87,0x34,0x8E,0x43,0x44,0xC4,0xDE,0xE9,0xCB, /*1*/
0x54,0x7B,0x94,0x32,0xA6,0xC2,0x23,0x3D,0xEE,0x4C,0x95,0x0B,0x42,0xFA,0xC3,0x4E, /*2*/
0x08,0x2E,0xA1,0x66,0x28,0xD9,0x24,0xB2,0x76,0x5B,0xA2,0x49,0x6D,0x8B,0xD1,0x25, /*3*/
0x72,0xF8,0xF6,0x64,0x86,0x68,0x98,0x16,0xD4,0xA4,0x5C,0xCC,0x5D,0x65,0xB6,0x92, /*4*/
0x6C,0x70,0x48,0x50,0xFD,0xED,0xB9,0xDA,0x5E,0x15,0x46,0x57,0xA7,0x8D,0x9D,0x84, /*5*/
0x90,0xD8,0xAB,0x00,0x8C,0xBC,0xD3,0x0A,0xF7,0xE4,0x58,0x05,0xB8,0xB3,0x45,0x06, /*6*/
0xD0,0x2C,0x1E,0x8F,0xCA,0x3F,0x0F,0x02,0xC1,0xAF,0xBD,0x03,0x01,0x13,0x8A,0x6B, /*7*/
0x3A,0x91,0x11,0x41,0x4F,0x67,0xDC,0xEA,0x97,0xF2,0xCF,0xCE,0xF0,0xB4,0xE6,0x73, /*8*/
0x96,0xAC,0x74,0x22,0xE7,0xAD,0x35,0x85,0xE2,0xF9,0x37,0xE8,0x1C,0x75,0xDF,0x6E, /*9*/
0x47,0xF1,0x1A,0x71,0x1D,0x29,0xC5,0x89,0x6F,0xB7,0x62,0x0E,0xAA,0x18,0xBE,0x1B, /*A*/
0xFC,0x56,0x3E,0x4B,0xC6,0xD2,0x79,0x20,0x9A,0xDB,0xC0,0xFE,0x78,0xCD,0x5A,0xF4, /*B*/
0x1F,0xDD,0xA8,0x33,0x88,0x07,0xC7,0x31,0xB1,0x12,0x10,0x59,0x27,0x80,0xEC,0x5F, /*C*/
0x60,0x51,0x7F,0xA9,0x19,0xB5,0x4A,0x0D,0x2D,0xE5,0x7A,0x9F,0x93,0xC9,0x9C,0xEF, /*D*/
0xA0,0xE0,0x3B,0x4D,0xAE,0x2A,0xF5,0xB0,0xC8,0xEB,0xBB,0x3C,0x83,0x53,0x99,0x61, /*E*/ 
0x17,0x2B,0x04,0x7E,0xBA,0x77,0xD6,0x26,0xE1,0x69,0x14,0x63,0x55,0x21,0x0C,0x7D  /*F*/
};
uint8_t Exp = 0, Subop = 0;
/* Keys of all rounds unrolled */
/* bits are in the format 0,1,2,3,4,5,6,7 */
ULONG			bskey[NR+1][128];

/*---------------------------------------------------------------------------
 * Function Name	: fnUnrollKey
 * Synopsis			: Given a round of AES key, this function will unroll
 * 					  the key and place it into bskey corresponding to the
 * 					  round.
 * Parameters		
 * 	Input			: AES round key, the current round number
 * 	Output			: 
 *--------------------------------------------------------------------------*/
static void fnUnrollKey(unsigned char key[], int round)
{
	int i;

	for(i=0; i<128; ++i){
		if ((key[i/8] & (1 << (i % 8))) != 0)
			bskey[round][i] = ~0x0;
		else
			bskey[round][i] = 0x0;
	}
}

/*---------------------------------------------------------------------------
 * Function Name	: fnNextRndKey
 * Synopsis			: Given the round key, this function will
 *                    generate the next round key inplace.
 * Parameters		
 * 	Input			: previous round key, previous round number
 * 	Output			: current round key (inplace)
 *--------------------------------------------------------------------------*/
static void fnNextRndKey(unsigned char key[], int prevrnd)
{
	unsigned char rcon;
	int i;

	switch(prevrnd){
		case 8:
			rcon = 0x1B;
			break;
		case 9:
			rcon = 0x36;
			break;
		default:
			rcon = 1 << prevrnd;
			break;
	}
	
	/* Copy and rotate the last word */
	key[3] = sbox[key[12]] ^ key[3];
	key[0] = sbox[key[13]] ^ key[0] ^ rcon;
	key[1] = sbox[key[14]] ^ key[1];
	key[2] = sbox[key[15]] ^ key[2];

	for(i=4; i<16; ++i){
		key[i] ^= key[i-4];
	}
}



/*---------------------------------------------------------------------------
 * Function Name	: fnExpandKey
 * Synopsis			: Given an AES key, this function will generate all
 *                    rounds of key and expand the key to bitslice format.
 *                    The expanded key is placed in the global bskey
 * Parameters		
 * 	Input			: key
 * 	Output			: -
 *--------------------------------------------------------------------------*/
void fnExpandKey(unsigned char key[])
{
	int i, j;
	fnUnrollKey(key, 0);

	for(i=0; i<NR; ++i){
		fnNextRndKey(key, i);
		fnUnrollKey(key, i+1);
	}
	
}

static ULONG buf[128];		/* Used in each round of AES to store output or to as input */


void fnSbox(ULONG x[8])
{	
	int j;
	unsigned char hex = (x[0]&1) | (x[1]&2) | (x[2]&4) | (x[3]&8) | 
				    (x[4]&16) | (x[5]&32) | (x[6]&64) | (x[7]&128);
	for(j=0 ; j < 7 ; j++, hex >>=1 ) {
		x[j] = ( (hex&0x1) ? ~0ULL : 0ULL );  
	}
	unsigned char out,in;
        unsigned long i;
	//printf("%02X\n", ((x[0]&1)<<7)|((x[1]&1)<<6)|((x[2]&1)<<5)|((x[3]&1)<<4)|((x[4]&1)<<3)|((x[5]&1)<<2)|((x[6]&1)<<1)|((x[7]&1)<<0));
	ULONG a0, a1, a2, a3;
	ULONG b0, b1, b2, b3;
	ULONG d0, d1, d2, d3;
	ULONG e0, e1, e2, e3;
	ULONG s0, s1, s2, s3, s4, s5, s6, s7;
	ULONG v0, v1, v2, v3;
	ULONG A, B, C, D;
	
	/* Transform into composite form */
	//printf("%02X ", ((x[0]&1)<<7)|((x[1]&1)<<6)|((x[2]&1)<<5)|((x[3]&1)<<4)|((x[4]&1)<<3)|((x[5]&1)<<2)|((x[6]&1)<<1)|((x[7]&1)<<0));
	b1 = x[0] ^ x[5] ^ x[6];
	B = x[0] ^ x[1] ^ x[3];

	a0 = b1 ^ x[7];
	a1 = b1 ^ x[1];
	a2 = b1 ^ x[4];
	a3 = x[1] ^ x[2] ^ a0;
	b0 = B ^ x[2] ^ x[6];
	b2 = x[0];
	b3 = B ^ x[4] ^ x[7];
	//printf("%02X ", ((a3&1)<<7)|((a2&1)<<6)|((a1&1)<<5)|((a0&1)<<4)|((b3&1)<<3)|((b2&1)<<2)|((b1&1)<<1)|((b0&1)<<0));


	/* G16_mul(a, b)^G16_sq_scl(a^b) */	
	v0 = a3 ^ a1; 
	v1 = a2 ^ a0; 
	v2 = b3 ^ b1; 
	v3 = b2 ^ b0;
	A  = v0 & v2;
	B = (a2 & b3) ^ (a3 & b2);
	C = (a0 & b1) ^ (a1 & b0);
	D = a0 ^ b0;
	e0 = A ^ (v1 & v3);
	e1 = A ^ (v1 & v2) ^ (v0 & v3);
	d3 = B ^ e1 ^ D ^ (a2 | b2);
	d2 = B ^ e0 ^ (a3 | b3) ^ x[1];
	d1 = C ^ e1 ^ (a0 | b0) ^ x[1];
	d0 = C ^ (a1 & b1) ^ e0 ^ D;
	//printf("%02X ", ((d3&1)<<3)|((d2&1)<<2)|((d1&1)<<1)|((d0&1)<<0));

	/* G16_inv(d) */
	/* 17 operations */
	A = d3 & d1;
//	e3 = ((d2 & d1) | d0) ^ A ^ d1;
	e3 = ((d2 & d1) | d0) ^ (d1&~d3);
	e2 = (A | d0) ^ (d2 & (d1 ^ d0));
	e1 = ((d3 & d0) | d2) ^ (d3&~d1);
	e0 = (A | d2) ^ (d0 & (d3 ^ d2));
	//printf("%02X ", ((e3&1)<<3)|((e2&1)<<2)|((e1&1)<<1)|((e0&1)<<0));

	/* G16_mul(e, b) */
	A = e3 ^ e1; B = e2 ^ e0;
	d0 = A & v2;
	d1 = (e2 & b3) ^ (e3 & b2);
	d2 = (e0 & b1) ^ (e1 & b0);
	C = d0 ^ (B & v3);
	D = d0 ^ (B & v2) ^ (A & v3);
	s6 = d1 ^ (e3 & b3) ^ C;
	s7 = d1 ^ (e2 & b2) ^ D;
	s5 = d2 ^ (e0 & b0) ^ D;
	s4 = d2 ^ (e1 & b1) ^ C;

	/* G16_mul(e, a) */
	d0 = A & v0;
	d1 = (e2 & a3) ^ (e3 & a2);
	d2 = (e0 & a1) ^ (e1 & a0);
	C = d0 ^ (B & v1);
	D = d0 ^ (B & v0) ^ (A & v1);
	s2 = d1 ^ (e3 & a3) ^ C;
	s3 = d1 ^ (e2 & a2) ^ D;
	s1 = d2 ^ (e0 & a0) ^ D;
	s0 = d2 ^ (e1 & a1) ^ C;
	//printf("%02X ", ((s0&1)<<0)|((s1&1)<<1)|((s2&1)<<2)|((s3&1)<<3)|((s4&1)<<4)|((s5&1)<<5)|((s6&1)<<6)|((s7&1)<<7));

	/* Convert back to GF(2^8) */
	x[7] = s5 ^ s3;
	C = ~s4 ^ s1;
	D = s6 ^ s0;

	x[6] = ~s7 ^ s3;
	x[5] = ~D;
	x[4] = s7 ^ x[7];
	x[2] = D ^ x[7] ^ s2;
	x[1] = s5 ^ C;
	x[0] = s6 ^ C;
	x[3] = x[4] ^ s6 ^ s4;

}
void fnLoadState(ULONG *a,const  ULONG *b, const ULONG *k){

	a[0] = b[0] ^ k[0];				
	a[1] = b[2] ^ k[1];			
	a[2] = b[4] ^ k[2];					
	a[3] = b[6] ^ k[3];			
	a[4] = b[8] ^ k[4];     		
	a[5] = b[10] ^ k[5];     		
	a[6] = b[12] ^ k[6];     		
	a[7] = b[14] ^ k[7];     	
	a[8] = b[64] ^  k[8];		
	a[9] = b[66] ^  k[9];
	a[10] = b[68] ^  k[10];
	a[11] = b[70] ^  k[11];
	a[12] = b[72] ^  k[12];
	a[13] = b[74] ^  k[13];
	a[14] = b[76] ^  k[14];
	a[15] = b[78] ^  k[15];
	a[16] = b[1] ^  k[16];		
	a[17] = b[3] ^  k[17];
	a[18] = b[5] ^  k[18];
	a[19] = b[7] ^  k[19];
	a[20] = b[9] ^  k[20];
	a[21] = b[11] ^  k[21];
	a[22] = b[13] ^  k[22];
	a[23] = b[15] ^  k[23];
	a[24] = b[65] ^  k[24];		
	a[25] = b[67] ^  k[25];
	a[26] = b[69] ^  k[26];
	a[27] = b[71] ^  k[27];
	a[28] = b[73] ^  k[28];
	a[29] = b[75] ^  k[29];
	a[30] = b[77] ^  k[30];
	a[31] = b[79] ^  k[31];
	a[32] = b[16] ^  k[32];		
	a[33] = b[18] ^  k[33];
	a[34] = b[20] ^  k[34];
	a[35] = b[22] ^  k[35];
	a[36] = b[24] ^  k[36];
	a[37] = b[26] ^  k[37];
	a[38] = b[28] ^  k[38];
	a[39] = b[30] ^  k[39];
	a[40] = b[80] ^  k[40];			
	a[41] = b[82] ^  k[41];	
	a[42] = b[84] ^  k[42];	
	a[43] = b[86] ^  k[43];	
	a[44] = b[88] ^  k[44];	
	a[45] = b[90] ^  k[45];	
	a[46] = b[92] ^  k[46];	
	a[47] = b[94] ^  k[47];	
	a[48] = b[17] ^  k[48];		
	a[49] = b[19] ^  k[49];
	a[50] = b[21] ^  k[50];
	a[51] = b[23] ^  k[51];
	a[52] = b[25] ^  k[52];
	a[53] = b[27] ^  k[53];
	a[54] = b[29] ^  k[54];
	a[55] = b[31] ^  k[55];
	a[56] = b[81] ^  k[56];		
	a[57] = b[83] ^  k[57];
	a[58] = b[85] ^  k[58];
	a[59] = b[87] ^  k[59];
	a[60] = b[89] ^  k[60];
	a[61] = b[91] ^  k[61];
	a[62] = b[93] ^  k[62];
	a[63] = b[95] ^  k[63];
	a[64] = b[32] ^  k[64];		
	a[65] = b[34] ^  k[65];
	a[66] = b[36] ^  k[66];
	a[67] = b[38] ^  k[67];
	a[68] = b[40] ^  k[68];
	a[69] = b[42] ^  k[69];
	a[70] = b[44] ^  k[70];
	a[71] = b[46] ^  k[71];
	a[72] = b[96] ^  k[72];		
	a[73] = b[98] ^  k[73];
	a[74] = b[100] ^  k[74];
	a[75] = b[102] ^  k[75];
	a[76] = b[104] ^  k[76];
	a[77] = b[106] ^  k[77];
	a[78] = b[108] ^  k[78];
	a[79] = b[110] ^  k[79];
	a[80] = b[33] ^  k[80];		
	a[81] = b[35] ^  k[81];
	a[82] = b[37] ^  k[82];
	a[83] = b[39] ^  k[83];
	a[84] = b[41] ^  k[84];
	a[85] = b[43] ^  k[85];
	a[86] = b[45] ^  k[86];
	a[87] = b[47] ^  k[87];
	a[88] = b[97] ^  k[88];	
	a[89] = b[99] ^  k[89];
	a[90] = b[101] ^  k[90];
	a[91] = b[103] ^  k[91];
	a[92] = b[105] ^  k[92];
	a[93] = b[107] ^  k[93];
	a[94] = b[109] ^  k[94];
	a[95] = b[111] ^  k[95];
	a[96] = b[48] ^  k[96];		
	a[97] = b[50] ^  k[97];
	a[98] = b[52] ^  k[98];
	a[99] = b[54] ^  k[99];
	a[100] = b[56] ^  k[100];
	a[101] = b[58] ^  k[101];
	a[102] = b[60] ^  k[102];
	a[103] = b[62] ^  k[103];
	a[104] = b[112] ^  k[104];	
	a[105] = b[114] ^  k[105];
	a[106] = b[116] ^  k[106];
	a[107] = b[118] ^  k[107];
	a[108] = b[120] ^  k[108];
	a[109] = b[122] ^  k[109];
	a[110] = b[124] ^  k[110];
	a[111] = b[126] ^  k[111];
	a[112] = b[49] ^  k[112];	
	a[113] = b[51] ^  k[113];
	a[114] = b[53] ^  k[114];
	a[115] = b[55] ^  k[115];
	a[116] = b[57] ^  k[116];
	a[117] = b[59] ^  k[117];
	a[118] = b[61] ^  k[118];
	a[119] = b[63] ^  k[119];
	a[120] = b[113] ^  k[120];		
	a[121] = b[115] ^  k[121];	
	a[122] = b[117] ^  k[122];	
	a[123] = b[119] ^  k[123];	
	a[124] = b[121] ^  k[124];	
	a[125] = b[123] ^  k[125];	
	a[126] = b[125] ^  k[126];	
	a[127] = b[127] ^  k[127];	

	return ;
}
static void fnRound(ULONG *s1, ULONG *s2, const ULONG *r_key, const int round){
	ULONG *sp[2];			
	ULONG *tmp_s, *s;		
	ULONG xor ;
	ULONG x0, x1, x2, x3, x4, x5, x6, x7 ;
	unsigned char out;

	sp[round&1] = s1;
	sp[(~round)&1] = s2;

	tmp_s = sp[1];
	s = sp[0]; 
	fnSbox(s); 
	fnSbox(s +8);  
	fnSbox(s+16); 
	fnSbox(s+24);
    	fnSbox(s + 32);  
	fnSbox(s+40);  
	fnSbox(s+48); 
	fnSbox(s+56); 
	fnSbox(s + 64);  
	fnSbox(s+72);  
	fnSbox(s+80); 
	fnSbox(s+88);
    	fnSbox(s + 96);  
	fnSbox(s+104);  
	fnSbox(s+112); 
	fnSbox(s+120);
	x0 =        s[40] ^ s[80] ;
	x1 = s[0] ^ s[41] ^ s[81] ;
	x2 = s[1] ^ s[42] ^ s[82] ;
	x3 = s[2] ^ s[43] ^ s[83] ;
	x4 = s[3] ^ s[44] ^ s[84] ;
	x5 = s[4] ^ s[45] ^ s[85] ;
	x6 = s[5] ^ s[46] ^ s[86] ;
	x7 = s[6] ^ s[47] ^ s[87] ;
	
	/* first col -- byte 1 */
	xor = s[7] ^ s[47] ;
	tmp_s[0] = x0         ^ s[120] ^ r_key[0] ^ xor;
	tmp_s[1] = x1 ^ s[40] ^ s[121] ^ r_key[1] ^ xor;
	tmp_s[2] = x2 ^ s[41] ^ s[122] ^ r_key[2] ;
	tmp_s[3] = x3 ^ s[42] ^ s[123] ^ r_key[3] ^ xor ;
	tmp_s[4] = x4 ^ s[43] ^ s[124] ^ r_key[4] ^ xor;
	tmp_s[5] = x5 ^ s[44] ^ s[125] ^ r_key[5] ;
	tmp_s[6] = x6 ^ s[45] ^ s[126] ^ r_key[6] ;
	tmp_s[7] = x7 ^ s[46] ^ s[127] ^ r_key[7];

	/* first col -- byte 4 */
	xor =  s[7] ^ s[127] ;
	tmp_s[24] = x0 ^ s[0]          ^ r_key[24] ^ xor;
	tmp_s[25] = x1 ^ s[1] ^ s[120] ^ r_key[25] ^ xor;
	tmp_s[26] = x2 ^ s[2] ^ s[121] ^ r_key[26] ;
	tmp_s[27] = x3 ^ s[3] ^ s[122] ^ r_key[27] ^ xor;
	tmp_s[28] = x4 ^ s[4] ^ s[123] ^ r_key[28] ^ xor;
	tmp_s[29] = x5 ^ s[5] ^ s[124] ^ r_key[29];
	tmp_s[30] = x6 ^ s[6] ^ s[125] ^ r_key[30];
	tmp_s[31] = x7 ^ s[7] ^ s[126] ^ r_key[31];
	

	/* common for bytes 2 and 3 */
	x0 = s[0]         ^ s[120] ;
	x1 = s[1] ^ s[80] ^ s[121] ;
	x2 = s[2] ^ s[81] ^ s[122] ;
	x3 = s[3] ^ s[82] ^ s[123] ;
	x4 = s[4] ^ s[83] ^ s[124] ;
	x5 = s[5] ^ s[84] ^ s[125] ;
	x6 = s[6] ^ s[85] ^ s[126] ;
	x7 = s[7] ^ s[86] ^ s[127] ;

	/* first col -- byte 2 */
	xor =  s[47] ^ s[87] ;
	tmp_s[8 ] = x0         ^ s[80] ^ r_key[8] ^ xor;
	tmp_s[9 ] = x1 ^ s[40] ^ s[81] ^ r_key[9] ^ xor;
	tmp_s[10] = x2 ^ s[41] ^ s[82] ^ r_key[10] ;
	tmp_s[11] = x3 ^ s[42] ^ s[83] ^ r_key[11] ^ xor;
	tmp_s[12] = x4 ^ s[43] ^ s[84] ^ r_key[12] ^ xor;
	tmp_s[13] = x5 ^ s[44] ^ s[85] ^ r_key[13] ;
	tmp_s[14] = x6 ^ s[45] ^ s[86] ^ r_key[14] ;
	tmp_s[15] = x7 ^ s[46] ^ s[87] ^ r_key[15] ;

	/* first col -- byte 3 */
	xor =  s[87] ^ s[127] ;
	tmp_s[16] = x0 ^ s[40]          ^ r_key[16] ^ xor;
	tmp_s[17] = x1 ^ s[41] ^ s[120] ^ r_key[17] ^ xor;
	tmp_s[18] = x2 ^ s[42] ^ s[121] ^ r_key[18];
	tmp_s[19] = x3 ^ s[43] ^ s[122] ^ r_key[19] ^ xor;
	tmp_s[20] = x4 ^ s[44] ^ s[123] ^ r_key[20]^ xor;
	tmp_s[21] = x5 ^ s[45] ^ s[124] ^ r_key[21];
	tmp_s[22] = x6 ^ s[46] ^ s[125] ^ r_key[22];
	tmp_s[23] = x7 ^ s[47] ^ s[126] ^ r_key[23];

	/*---------------- second column------------------- */

	/* common for bytes 1 and 4 */
	x0 =         s[72] ^ s[112];
	x1 = s[32] ^ s[73] ^ s[113];
	x2 = s[33] ^ s[74] ^ s[114];
	x3 = s[34] ^ s[75] ^ s[115];
	x4 = s[35] ^ s[76] ^ s[116];
	x5 = s[36] ^ s[77] ^ s[117];
	x6 = s[37] ^ s[78] ^ s[118];
	x7 = s[38] ^ s[79] ^ s[119];

	/* second  col -- byte 1 */
	xor = s[39] ^ s[79] ;
	tmp_s[32] = x0         ^ s[24] ^ r_key[32]   ^ xor;
	tmp_s[33] = x1 ^ s[72] ^ s[25] ^ r_key[33]   ^ xor;
	tmp_s[34] = x2 ^ s[73] ^ s[26] ^ r_key[34];
	tmp_s[35] = x3 ^ s[74] ^ s[27] ^ r_key[35]  ^ xor ;
	tmp_s[36] = x4 ^ s[75] ^ s[28] ^ r_key[36]  ^ xor;
	tmp_s[37] = x5 ^ s[76] ^ s[29] ^ r_key[37];
	tmp_s[38] = x6 ^ s[77] ^ s[30] ^ r_key[38] ;
	tmp_s[39] = x7 ^ s[78] ^ s[31] ^ r_key[39] ;

	/* second col -- byte 4 */
	xor =  s[39] ^ s[31] ;
	tmp_s[56] = x0 ^ s[32]         ^ r_key[56] ^ xor;
	tmp_s[57] = x1 ^ s[33] ^ s[24] ^ r_key[57] ^ xor;
	tmp_s[58] = x2 ^ s[34] ^ s[25] ^ r_key[58] ;
	tmp_s[59] = x3 ^ s[35] ^ s[26] ^ r_key[59] ^ xor;
	tmp_s[60] = x4 ^ s[36] ^ s[27] ^ r_key[60] ^ xor;
	tmp_s[61] = x5 ^ s[37] ^ s[28] ^ r_key[61] ;
	tmp_s[62] = x6 ^ s[38] ^ s[29] ^ r_key[62] ;
	tmp_s[63] = x7 ^ s[39] ^ s[30] ^ r_key[63] ;

	/* common for bytes 2 and 3 */
	x0 = s[32]          ^ s[24] ;
	x1 = s[33] ^ s[112] ^ s[25] ;
	x2 = s[34] ^ s[113] ^ s[26] ;
	x3 = s[35] ^ s[114] ^ s[27] ;
	x4 = s[36] ^ s[115] ^ s[28] ;
	x5 = s[37] ^ s[116] ^ s[29] ;
	x6 = s[38] ^ s[117] ^ s[30] ;
	x7 = s[39] ^ s[118] ^ s[31] ;

	/* second col -- byte 2 */
	xor =  s[79] ^ s[119];
	tmp_s[40] = x0 ^ s[112]         ^ r_key[40] ^ xor;
	tmp_s[41] = x1 ^ s[113] ^ s[72] ^ r_key[41] ^ xor;
	tmp_s[42] = x2 ^ s[114] ^ s[73] ^ r_key[42];
	tmp_s[43] = x3 ^ s[115] ^ s[74] ^ r_key[43] ^ xor;
	tmp_s[44] = x4 ^ s[116] ^ s[75] ^ r_key[44] ^ xor;
	tmp_s[45] = x5 ^ s[117] ^ s[76] ^ r_key[45];
	tmp_s[46] = x6 ^ s[118] ^ s[77] ^ r_key[46];
	tmp_s[47] = x7 ^ s[119] ^ s[78] ^ r_key[47];

	/* second col -- byte 3 */
	xor =  s[119] ^ s[31] ;
	tmp_s[48] = x0 ^ s[72]         ^ r_key[48] ^ xor;
	tmp_s[49] = x1 ^ s[73] ^ s[24] ^ r_key[49] ^ xor;
	tmp_s[50] = x2 ^ s[74] ^ s[25] ^ r_key[50];
	tmp_s[51] = x3 ^ s[75] ^ s[26] ^ r_key[51] ^ xor;
	tmp_s[52] = x4 ^ s[76] ^ s[27] ^ r_key[52] ^ xor;
	tmp_s[53] = x5 ^ s[77] ^ s[28] ^ r_key[53];
	tmp_s[54] = x6 ^ s[78] ^ s[29] ^ r_key[54];
	tmp_s[55] = x7 ^ s[79] ^ s[30] ^ r_key[55];

	/*---------------- third column------------------- */
	x0 = s[16] ^ s[104]         ;
	x1 = s[17] ^ s[105] ^ s[64] ;
	x2 = s[18] ^ s[106] ^ s[65] ;
	x3 = s[19] ^ s[107] ^ s[66] ;
	x4 = s[20] ^ s[108] ^ s[67] ;
	x5 = s[21] ^ s[109] ^ s[68] ;
	x6 = s[22] ^ s[110] ^ s[69] ;
	x7 = s[23] ^ s[111] ^ s[70] ;

	/* third  col -- byte 1 */
	xor = s[111] ^ s[71] ;
	tmp_s[64] = x0 ^ s[56]          ^ r_key[64] ^ xor;
	tmp_s[65] = x1 ^ s[57] ^ s[104] ^ r_key[65] ^ xor;
	tmp_s[66] = x2 ^ s[58] ^ s[105] ^ r_key[66];
	tmp_s[67] = x3 ^ s[59] ^ s[106] ^ r_key[67] ^ xor ;
	tmp_s[68] = x4 ^ s[60] ^ s[107] ^ r_key[68] ^ xor;
	tmp_s[69] = x5 ^ s[61] ^ s[108] ^ r_key[69];
	tmp_s[70] = x6 ^ s[62] ^ s[109] ^ r_key[70];
	tmp_s[71] = x7 ^ s[63] ^ s[110] ^ r_key[71];
	
	/* third col -- byte 4 */
	xor =  s[71] ^ s[63] ;
	tmp_s[88] = x0 ^ s[64]         ^ r_key[88] ^ xor; 
	tmp_s[89] = x1 ^ s[65] ^ s[56] ^ r_key[89] ^ xor;
	tmp_s[90] = x2 ^ s[66] ^ s[57] ^ r_key[90] ;
	tmp_s[91] = x3 ^ s[67] ^ s[58] ^ r_key[91]  ^ xor;
	tmp_s[92] = x4 ^ s[68] ^ s[59] ^ r_key[92]  ^ xor;
	tmp_s[93] = x5 ^ s[69] ^ s[60] ^ r_key[93] ;
	tmp_s[94] = x6 ^ s[70] ^ s[61] ^ r_key[94] ;
	tmp_s[95] = x7 ^ s[71] ^ s[62] ^ r_key[95] ;

	x0 =         s[64] ^ s[56] ;
	x1 = s[16] ^ s[65] ^ s[57] ;
	x2 = s[17] ^ s[66] ^ s[58] ;
	x3 = s[18] ^ s[67] ^ s[59] ;
	x4 = s[19] ^ s[68] ^ s[60] ;
	x5 = s[20] ^ s[69] ^ s[61] ;
	x6 = s[21] ^ s[70] ^ s[62] ;
	x7 = s[22] ^ s[71] ^ s[63] ;

	/* third col -- byte 2 */
	xor =  s[23] ^ s[111] ;
	tmp_s[72] = x0 ^ s[16]          ^ r_key[72] ^ xor;
	tmp_s[73] = x1 ^ s[17] ^ s[104] ^ r_key[73] ^ xor;
	tmp_s[74] = x2 ^ s[18] ^ s[105] ^ r_key[74] ;
	tmp_s[75] = x3 ^ s[19] ^ s[106] ^ r_key[75] ^ xor;
	tmp_s[76] = x4 ^ s[20] ^ s[107] ^ r_key[76] ^ xor;
	tmp_s[77] = x5 ^ s[21] ^ s[108] ^ r_key[77] ;
	tmp_s[78] = x6 ^ s[22] ^ s[109] ^ r_key[78] ;
	tmp_s[79] = x7 ^ s[23] ^ s[110] ^ r_key[79] ;

	/* third col -- byte 3 */
	xor =   s[23] ^ s[63];
	tmp_s[80] = x0         ^ s[104] ^ r_key[80] ^ xor;
	tmp_s[81] = x1 ^ s[56] ^ s[105] ^ r_key[81] ^ xor;
	tmp_s[82] = x2 ^ s[57] ^ s[106] ^ r_key[82] ;
	tmp_s[83] = x3 ^ s[58] ^ s[107] ^ r_key[83] ^ xor;
	tmp_s[84] = x4 ^ s[59] ^ s[108] ^ r_key[84] ^ xor;
	tmp_s[85] = x5 ^ s[60] ^ s[109] ^ r_key[85] ;
	tmp_s[86] = x6 ^ s[61] ^ s[110] ^ r_key[86] ;
	tmp_s[87] = x7 ^ s[62] ^ s[111] ^ r_key[87] ;
	
	/*---------------- fourth column------------------- */

	x0 = s[8]  ^ s[48];
	x1 = s[9]  ^ s[49] ^ s[96] ;
	x2 = s[10] ^ s[50] ^ s[97] ;
	x3 = s[11] ^ s[51] ^ s[98] ;
	x4 = s[12] ^ s[52] ^ s[99] ;
	x5 = s[13] ^ s[53] ^ s[100] ;
	x6 = s[14] ^ s[54] ^ s[101] ;
	x7 = s[15] ^ s[55] ^ s[102] ;
	
	/* fourth  col -- byte 1 */
	xor = s[103] ^ s[15] ;

	tmp_s[96]  = x0         ^ s[88] ^ r_key[96]  ^ xor;
	tmp_s[97]  = x1 ^ s[8]  ^ s[89] ^ r_key[97]  ^ xor;
	tmp_s[98]  = x2 ^ s[9]  ^ s[90] ^ r_key[98];
	tmp_s[99]  = x3 ^ s[10] ^ s[91] ^ r_key[99]  ^ xor;
	tmp_s[100] = x4 ^ s[11] ^ s[92] ^ r_key[100] ^ xor;
	tmp_s[101] = x5 ^ s[12] ^ s[93] ^ r_key[101];
	tmp_s[102] = x6 ^ s[13] ^ s[94] ^ r_key[102];
	tmp_s[103] = x7 ^ s[14] ^ s[95] ^ r_key[103];

	/* fourth col -- byte 4 */
	xor = s[95] ^ s[103] ;
	tmp_s[120] = x0 ^ s[96]          ^ r_key[120] ^ xor; 
	tmp_s[121] = x1 ^ s[97]  ^ s[88] ^ r_key[121] ^ xor;
	tmp_s[122] = x2 ^ s[98]  ^ s[89] ^ r_key[122];
	tmp_s[123] = x3 ^ s[99]  ^ s[90] ^ r_key[123] ^ xor;
	tmp_s[124] = x4 ^ s[100] ^ s[91] ^ r_key[124] ^ xor;
	tmp_s[125] = x5 ^ s[101] ^ s[92] ^ r_key[125];
	tmp_s[126] = x6 ^ s[102] ^ s[93] ^ r_key[126];
	tmp_s[127] = x7 ^ s[103] ^ s[94] ^ r_key[127];
	
	x0 = s[96]  ^ s[88] ;
	x1 = s[97]  ^ s[89] ^ s[48] ;
	x2 = s[98]  ^ s[90] ^ s[49] ;
	x3 = s[99]  ^ s[91] ^ s[50] ;
	x4 = s[100] ^ s[92] ^ s[51] ;
	x5 = s[101] ^ s[93] ^ s[52] ;
	x6 = s[102] ^ s[94] ^ s[53] ;
	x7 = s[103]	^ s[95] ^ s[54] ;
	
	/* fourth col -- byte 2 */
	xor =  s[55] ^ s[15] ;
	tmp_s[104] = x0         ^ s[48] ^ r_key[104] ^ xor;
	tmp_s[105] = x1 ^ s[8]  ^ s[49] ^ r_key[105] ^ xor;
	tmp_s[106] = x2 ^ s[9]  ^ s[50] ^ r_key[106];
	tmp_s[107] = x3 ^ s[10] ^ s[51] ^ r_key[107] ^ xor;
	tmp_s[108] = x4 ^ s[11] ^ s[52] ^ r_key[108] ^ xor;
	tmp_s[109] = x5 ^ s[12] ^ s[53] ^ r_key[109];
	tmp_s[110] = x6 ^ s[13] ^ s[54] ^ r_key[110];
	tmp_s[111] = x7 ^ s[14] ^ s[55] ^ r_key[111];

	/* fourth col -- byte 3 */
	xor =  s[55] ^ s[95] ;
	tmp_s[112] = x0         ^ s[8]  ^ r_key[112] ^ xor;
	tmp_s[113] = x1 ^ s[88] ^ s[9]  ^ r_key[113] ^ xor;
	tmp_s[114] = x2 ^ s[89] ^ s[10] ^ r_key[114];
	tmp_s[115] = x3 ^ s[90] ^ s[11] ^ r_key[115] ^ xor;
	tmp_s[116] = x4 ^ s[91] ^ s[12] ^ r_key[116] ^ xor;
	tmp_s[117] = x5 ^ s[92] ^ s[13] ^ r_key[117];
	tmp_s[118] = x6 ^ s[93] ^ s[14] ^ r_key[118];
	tmp_s[119] = x7 ^ s[94] ^ s[15] ^ r_key[119];
	return ;
} 

static void fnLastRound(ULONG *cipher, ULONG *state, const ULONG *rnd_key)
{
	/* Byte wise s-box substitution for subBytes */
	fnSbox(state);      
	fnSbox(state +8); 
	fnSbox(state+16); 
	fnSbox(state+24);
	fnSbox(state + 32); 
	fnSbox(state+40);  
	fnSbox(state+48); 
	fnSbox(state+56); 
	fnSbox(state + 64);  
	fnSbox(state+72);  
	fnSbox(state+80); 
	fnSbox(state+88);
	fnSbox(state + 96);  
	fnSbox(state+104);  
	fnSbox(state+112); 
	fnSbox(state+120); 
	/* Column 0 */
	cipher[0] = state[0] ^  rnd_key[0];			/* c0-c7 */
	cipher[2] = state[1] ^  rnd_key[1];
	cipher[4] = state[2] ^  rnd_key[2];
	cipher[6] = state[3] ^  rnd_key[3];
	cipher[8] = state[4] ^  rnd_key[4];
	cipher[10] = state[5] ^  rnd_key[5];
	cipher[12] = state[6] ^  rnd_key[6];
	cipher[14] = state[7] ^  rnd_key[7];
	cipher[16] = state[40] ^  rnd_key[8];		/* c8-c15 */
	cipher[18] = state[41] ^  rnd_key[9];
	cipher[20] = state[42] ^  rnd_key[10];
	cipher[22] = state[43] ^  rnd_key[11];
	cipher[24] = state[44] ^  rnd_key[12];
	cipher[26] = state[45] ^  rnd_key[13];
	cipher[28] = state[46] ^  rnd_key[14];
	cipher[30] = state[47] ^  rnd_key[15];
	cipher[32] = state[80] ^  rnd_key[16];		/* c16-c23 */
	cipher[34] = state[81] ^  rnd_key[17];
	cipher[36] = state[82] ^  rnd_key[18];
	cipher[38] = state[83] ^  rnd_key[19];
	cipher[40] = state[84] ^  rnd_key[20];
	cipher[42] = state[85] ^  rnd_key[21];
	cipher[44] = state[86] ^  rnd_key[22];
	cipher[46] = state[87] ^  rnd_key[23];
	cipher[48] = state[120] ^  rnd_key[24];		/* c24-c31 */
	cipher[50] = state[121] ^  rnd_key[25];
	cipher[52] = state[122] ^  rnd_key[26];
	cipher[54] = state[123] ^  rnd_key[27];
	cipher[56] = state[124] ^  rnd_key[28];
	cipher[58] = state[125] ^  rnd_key[29];
	cipher[60] = state[126] ^  rnd_key[30];
	cipher[62] = state[127] ^  rnd_key[31];
	/* Column 1 */
	cipher[64] = state[32] ^  rnd_key[32];		/* c32-c39 */
	cipher[66] = state[33] ^  rnd_key[33];
	cipher[68] = state[34] ^  rnd_key[34];
	cipher[70] = state[35] ^  rnd_key[35];
	cipher[72] = state[36] ^  rnd_key[36];
	cipher[74] = state[37] ^  rnd_key[37];
	cipher[76] = state[38] ^  rnd_key[38];
	cipher[78] = state[39] ^  rnd_key[39];
	cipher[80] = state[72] ^  rnd_key[40];		/* c40-47 */
	cipher[82] = state[73] ^  rnd_key[41];
	cipher[84] = state[74] ^  rnd_key[42];
	cipher[86] = state[75] ^  rnd_key[43];
	cipher[88] = state[76] ^  rnd_key[44];
	cipher[90] = state[77] ^  rnd_key[45];
	cipher[92] = state[78] ^  rnd_key[46];
	cipher[94] = state[79] ^  rnd_key[47];
	cipher[96] = state[112] ^  rnd_key[48];		/* c48-c55 */
	cipher[98] = state[113] ^  rnd_key[49];
	cipher[100] = state[114] ^  rnd_key[50];
	cipher[102] = state[115] ^  rnd_key[51];
	cipher[104] = state[116] ^  rnd_key[52];
	cipher[106] = state[117] ^  rnd_key[53];
	cipher[108] = state[118] ^  rnd_key[54];
	cipher[110] = state[119] ^  rnd_key[55];
	cipher[112] = state[24] ^  rnd_key[56];		/* c56-c63 */
	cipher[114] = state[25] ^  rnd_key[57];
	cipher[116] = state[26] ^  rnd_key[58];
	cipher[118] = state[27] ^  rnd_key[59];
	cipher[120] = state[28] ^  rnd_key[60];
	cipher[122] = state[29] ^  rnd_key[61];
	cipher[124] = state[30] ^  rnd_key[62];
	cipher[126] = state[31] ^  rnd_key[63];
	/* Column 3 */
	cipher[1] = state[64] ^  rnd_key[64];		/* c64-c71 */
	cipher[3] = state[65] ^  rnd_key[65];
	cipher[5] = state[66] ^  rnd_key[66];
	cipher[7] = state[67] ^  rnd_key[67];
	cipher[9] = state[68] ^  rnd_key[68];
	cipher[11] = state[69] ^  rnd_key[69];
	cipher[13] = state[70] ^  rnd_key[70];
	cipher[15] = state[71] ^  rnd_key[71];
	cipher[17] = state[104] ^  rnd_key[72];		/* c72-c79	*/
	cipher[19] = state[105] ^  rnd_key[73];
	cipher[21] = state[106] ^  rnd_key[74];
	cipher[23] = state[107] ^  rnd_key[75];
	cipher[25] = state[108] ^  rnd_key[76];
	cipher[27] = state[109] ^  rnd_key[77];
	cipher[29] = state[110] ^  rnd_key[78];
	cipher[31] = state[111] ^  rnd_key[79];
	cipher[33] = state[16] ^  rnd_key[80];		/* c80-c87 */
	cipher[35] = state[17] ^  rnd_key[81];
	cipher[37] = state[18] ^  rnd_key[82];
	cipher[39] = state[19] ^  rnd_key[83];
	cipher[41] = state[20] ^  rnd_key[84];
	cipher[43] = state[21] ^  rnd_key[85];
	cipher[45] = state[22] ^  rnd_key[86];
	cipher[47] = state[23] ^  rnd_key[87];
	cipher[49] = state[56] ^  rnd_key[88];		/* c88-c95	*/
	cipher[51] = state[57] ^  rnd_key[89];
	cipher[53] = state[58] ^  rnd_key[90];
	cipher[55] = state[59] ^  rnd_key[91];
	cipher[57] = state[60] ^  rnd_key[92];
	cipher[59] = state[61] ^  rnd_key[93];
	cipher[61] = state[62] ^  rnd_key[94];
	cipher[63] = state[63] ^  rnd_key[95];
	/* Column 4 */
	cipher[65] = state[96] ^  rnd_key[96];		/* c96-c103	*/
	cipher[67] = state[97] ^  rnd_key[97];
	cipher[69] = state[98] ^  rnd_key[98];
	cipher[71] = state[99] ^  rnd_key[99];
	cipher[73] = state[100] ^  rnd_key[100];
	cipher[75] = state[101] ^  rnd_key[101];
	cipher[77] = state[102] ^  rnd_key[102];
	cipher[79] = state[103] ^  rnd_key[103];
	cipher[81] = state[8]  ^  rnd_key[104];		/* c104-c111 */
	cipher[83] = state[9]  ^  rnd_key[105];
	cipher[85] = state[10] ^  rnd_key[106];
	cipher[87] = state[11] ^  rnd_key[107];
	cipher[89] = state[12] ^  rnd_key[108];
	cipher[91] = state[13] ^  rnd_key[109];
	cipher[93] = state[14] ^  rnd_key[110];
	cipher[95] = state[15] ^  rnd_key[111];
	cipher[97] = state[48] ^  rnd_key[112];		/* c112-c119 */
	cipher[99] = state[49] ^  rnd_key[113];
	cipher[101] = state[50] ^  rnd_key[114];
	cipher[103] = state[51] ^  rnd_key[115];
	cipher[105] = state[52] ^  rnd_key[116];
	cipher[107] = state[53] ^  rnd_key[117];
	cipher[109] = state[54] ^  rnd_key[118];
	cipher[111] = state[55] ^  rnd_key[119];
	cipher[113] = state[88] ^  rnd_key[120];	/*c120-c127 */
	cipher[115] = state[89] ^  rnd_key[121];
	cipher[117] = state[90] ^  rnd_key[122];
	cipher[119] = state[91] ^  rnd_key[123];
	cipher[121] = state[92] ^  rnd_key[124];
	cipher[123] = state[93] ^  rnd_key[125];
	cipher[125] = state[94] ^  rnd_key[126];
	cipher[127] = state[95] ^  rnd_key[127];

	return ;	
}
/* }}} */		

/*---------------------------------------------------------------------------
 * Function Name	: fnEncryptBulk
 * Synopsis			: Bulk Encryption
 * Parameters		
 * 	Input			: plaintext
 * 	Output			: ciphertext
 *--------------------------------------------------------------------------*/
void fnEncryptBulk(const ULONG *plain, ULONG *cipher)
{
	int round;
	ULONG *s2=cipher, *s1=buf;
	

	fnLoadState(s2, plain, bskey[0]); 

	for(round=1 ; round < NR ; round++){
		fnRound(s1, s2, bskey[round],round); 
	}
	fnLastRound(s2, s1, bskey[NR]);	
	
	return ;
}
typedef unsigned long long ULONG;

#define BITLENGTH		64		/* Bitlength of the machine */
#define BITLENGTH_LOG2	6

/* Masks used for transpose */
#define M0		0x5555555555555555ULL
#define M1		0x3333333333333333ULL
#define M2		0x0F0F0F0F0F0F0F0FULL
#define M3		0x00FF00FF00FF00FFULL
#define M4		0x0000FFFF0000FFFFULL
#define M5		0x00000000FFFFFFFFULL


static unsigned char	key[16] = {0x2b,0x7e,0x15,0x16,0x28,0xae,0xd2,0xa6,0xab,0xf7,0x15,0x88,0x09,0xcf,0x4f,0x3c};	/* The AES key read from the file specified by -kf at command line */
const static ULONG	m[] = {M0, M1, M2, M3, M4, M5};	/* Mask array for transpose */

static ULONG pt[128];		/* Current plaintext block being encrypted */
static ULONG ct[128];		/* Corresponding encoded ciphertext		   */

/*---------------------------------------------------------------------------
 * Function Name	: fnPrintUsage
 * Synopsis			: Print the usage from command line
 * Parameters		
 * 	Input			: flag: what should be printed
 * 	Output			: 
 *--------------------------------------------------------------------------*/
static int fnPrintUsage(int i_nFlag)
{
	int t_nI ;
	char t_acBuf[8] ;

	if(i_nFlag == 1){
		printf("Usage: \n");
		printf("<PROGRAM> -if <input_file> -of <output_file> -kf <key_file>\n");
	}
	if(i_nFlag == 2){
		printf("\n In/Out/Key Files is not present Or contents of input file are not proper\n");
	}

	printf("See README for details\n");
	return -1 ;
}

/*---------------------------------------------------------------------------
 * Function Name	: fnProcessArgs
 * Synopsis			: Process command line arguments, Also copies the
 *                    key from file into an array
 * Parameters		
 * 	Input			: command line parameters, argc, and argv
 * 	Output			: 
 *--------------------------------------------------------------------------*/
static int fnProcessArgs(int argc, char* argv[])
{
	int i,j ; 
	FILE *t_fpKey ;
	unsigned char k;
	ULONG  kb;
	
	if((argc != 7) || strcmp(argv[1],"-if") != 0 || strcmp(argv[3], "-of") != 0 ||
			strcmp(argv[5], "-kf") != 0 ){
		return fnPrintUsage(1);
	}
	/* open key file */
	if ( (t_fpKey = fopen(argv[6], "r")) == NULL ){
		return fnPrintUsage(2);
	}

	for(i=0; i<16; ++i){
		fscanf(t_fpKey, "%02X", &key[i]);
	}
	fclose(t_fpKey);
}

/*---------------------------------------------------------------------------
 * Function Name	: fnTranspose
 * Synopsis			: Accepts a block of data (either plain text or 
 *                    cipher text) and takes the transpose of it. This 
 *                    puts the file in a bit slice fashion.
 *                    This alogorithm is based on the paper 'Leveraging 
 *                    the multiprocessing capabilities of modern network
 *                    processors for cryptographic acceleration'. by 
 *                    Gunner Gaubatz et. al.
 * Parameters		
 * 	Input			: A block of 128x64 matrix. 
 * 	Output			: Inplace transpose of the matrix
 *--------------------------------------------------------------------------*/
static void fnTranspose(ULONG *a)
{
	int i;
	int j;
	int k; 				/* 2^j      */
	int k2;			
	int r=0;			
	int l;				
	ULONG imask;		
	ULONG temp;			
	for(j=0; j<BITLENGTH_LOG2; ++j){
		k = 1 << j;
		k2 = k << 1;
		imask = ~m[j];
		for(i=0; i<BITLENGTH/2; i++){
			l = (2 * (i - (i&r)) + (i&r))*2;
			temp = (a[l] & m[j]) ^ ((a[l+k2] & m[j])<<k);
			a[l+k2] = (a[l+k2] & imask) ^((a[l] & imask)>>k);
			a[l] = temp;
			temp = (a[l+1] & m[j]) ^ ((a[l+k2+1] & m[j])<<k);
			a[l+k2+1] = (a[l+k2+1] & imask) ^((a[l+1] & imask)>>k);
			a[l+1] = temp;
		}
		r |= k;
	}
}

int main(int argc, char* argv[]) 
{
	char *if_file;		/* Pointer to plaintext filename in argv  */
	char *of_file;		/* Pointer to ciphertext filename in argv */
	FILE *ifd, *ofd;	/* Input and output file descriptors      */
	int i, t;
	ULONG n = 0;//29313*3;
	//ULONG n=1;
	struct timeval	start_tv, end_tv; /* Start and end time */
	double td;			/* Time difference */
	
	if_file = &argv[2][0];		/* set pointer to input file name */
	of_file = &argv[4][0];		/* set pointer to output file name */

	fnProcessArgs(argc, argv);

	fnExpandKey(key);

	if ((ifd = fopen(if_file, "rb")) == NULL)
	 	return fnPrintUsage(2);
	if ((ofd = fopen(of_file, "wb")) == NULL)
		return fnPrintUsage(2);
	
	fread((void *)pt, sizeof(ULONG), 128, ifd);
	/* Do a dummy encryption to load it into cache */
	fnTranspose(pt);
	fnEncryptBulk(pt, ct);
	fnTranspose(ct);
	
	fwrite((void *)ct, sizeof(ULONG), 128, ofd);
	fclose(ofd);
	fclose(ifd);
}

