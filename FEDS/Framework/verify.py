import sys, getopt
import array
import re
import os.path
from os import path
#import fun_boundary as fun_b
import IFG as fun_b


f = open("mapping_function.c", "w")
path1 = str(sys.argv[5])
path2 = str(sys.argv[6])

def fun_ttable():
		# open file in read mode 
	fn = open(path1, 'r') 
  
	# read the content of the file line by line 
	print "Reading the initial contents from Implementation\n"
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[3]):
        	  f.write(cont[i]) 
  
	# close the file 
	fn.close() 

	print "Extracting the Program Expressions \n"
	start_c = int(sys.argv[3])#181
	num_c = int(sys.argv[4])
	check1 = -1
	expression=0
	with open(path1) as f2:
    	    for i, linec in enumerate(f2):
        	if i >= start_c and i < start_c + num_c:
	    	  #print line.strip()
	    	  current_line = i
    	         # print current_line
	    	  line_strc = linec.strip()
	    	  if line_strc.find("/*")== -1 and line_strc.find("//")==-1 and line_strc.find("{")==-1 and line_strc.find("}")==-1:
			expression = i
			print line_strc
	    		check1 = line_strc.find('=')
	    		if check1 != -1 :
	    		    str1 = line_strc[0:line_strc.find('=')]
			    #print str1,str2
			   # print str4
	    	  f.write(linec)          
	f2.close()

	fr = open("result.txt", "a") 
	fr.write("E"+str(expression+1) + ":\n") # write the line number to file
	fr.close();

	#Adding copy statement only 
	if check1 != -1:
		f.write("\n\t ciphertext[0]  =(uint8_t)" + str1+";\n")
		f.write("\t ciphertext[1]  =(uint8_t)(" + str1+">>8);\n")
		f.write("\t ciphertext[2]  =(uint8_t)(" + str1+">>16);\n")
		f.write("\t ciphertext[3]  =(uint8_t)(" + str1+">>24);\n")

	#Closing the Function
	f.write("}\n");
	# open file in read mode 
	fn = open(path2, 'r') 
   
	# read the content of the file line by line 
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[1]):
	          f.write(cont[i]) 
 
	fn.close()

	start = int(sys.argv[1])
	num = int(sys.argv[2])
	check = -1
	with open(path2) as f1:
    	   for i, line in enumerate(f1):
    	       if i >= start and i < start + num:
		    #print line.strip()
		    line_str = line.strip()
		    print line_str
		    check = line_str.find('[')
		    if check != -1:
		    	str1 = line_str[0:line_str.find('[')]
			str2 = line_str[line_str.find('[')+1:line_str.find(']')]
		    f.write(line)
            
	f1.close();
	#Adding copy statement only 
	if check != -1:
	   f.write("\tSubop =" + str1 + "[" +str2+ "]; \n }\n")
	# End of the roundFunction
	return current_line

def fun_cbmc ():

	# open file in read mode 
	fn = open(path1, 'r') 
  
	# read the content of the file line by line 
	print "Reading the initial contents from Implementation\n"
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[3]):
        	  f.write(cont[i]) 
  
	# close the file 
	fn.close() 

	print "Extracting the Program Expressions \n"
	start_c = int(sys.argv[3])#181
	num_c = int(sys.argv[4])
	check1 = -1
	expression=0
	with open(path1) as f2:
    	    for i, linec in enumerate(f2):
        	if i >= start_c and i < start_c + num_c:
	    	  #print line.strip()
	    	  current_line = i
    	         # print current_line
	    	  line_strc = linec.strip()
	    	  if line_strc.find("/*")== -1 and line_strc.find("//")==-1 and line_strc.find("{")==-1 and line_strc.find("}")==-1:
			expression = i
			print line_strc
	    		check1 = line_strc.find('[')
			check2 = line_strc.find(']=')
			check3 = line_strc.find(']=')
	    		if check1 != -1 and check2!=-1:
	    		    str1 = line_strc[line_strc.find('[')+1:line_strc.find(']')]
			    str2 = line_strc[line_strc.find('][')+2:line_strc.find(']=')]
			    #print str1,str2
			   # print str4
	    	  f.write(linec)          
	f2.close()

	#Adding copy statement only 
	if check1 != -1 and check2!=-1:
	   f.write("\t Exp = state[" + str1 + "][" + str2+"]; \n")
	   print "\t Exp = state[" , str1 , "][" , str2,"]; \n"

	#Closing the Function
	f.write("}\n");

	fr = open("result.txt", "a") 
	fr.write("E"+str(expression+1) + ":\n") # write the line number to file
	fr.close();

	# open file in read mode 
	fn = open(path2, 'r') 
   
	# read the content of the file line by line 
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[1]):
	          f.write(cont[i]) 
 
	fn.close()

	start = int(sys.argv[1])
	num = int(sys.argv[2])
	check = -1
	with open(path2) as f1:
    	   for i, line in enumerate(f1):
    	       if i >= start and i < start + num:
		    #print line.strip()
		    line_str = line.strip()
		    print line_str
		    check = line_str.find('[')
		    if check != -1:
		    	str1 = line_str[0:line_str.find('[')]
			str2 = line_str[line_str.find('[')+1:line_str.find(']')]
		    f.write(line)
            
	f1.close();
	#Adding copy statement only 
	if check != -1:
	   f.write("\tSubop =" + str1 + "[" +str2+ "]; \n }\n")
	# End of the roundFunction
	return current_line

def fun_bitsliced ():

	# open file in read mode 
	fn = open(path1, 'r') 
  
	# read the content of the file line by line 
	print "Reading the initial contents from Implementation\n"
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[3]):
        	  f.write(cont[i]) 
  
	# close the file 
	fn.close() 

	print "Extracting the Program Expressions \n"
	start_c = int(sys.argv[3])#181
	num_c = int(sys.argv[2])
	check1 = -1
	check = -1
	check3 = -1
	expression=0
	with open(path1) as f2:
    	    for i, linec in enumerate(f2):
        	if i >= start_c and i < start_c + num_c:
	    	  #print linec.strip()
	    	  current_line = i
    	         # print current_line
	    	  line_strc = linec.strip()
	    	  if line_strc.find("/*")== -1 and line_strc.find("//")==-1 and line_strc.find("{")==-1 and line_strc.find("}")==-1:
			expression = i
			print line_strc
	    	 	check = line_strc.find('[')
	    		check1 = line_strc.find('(')
	    		if check != -1:
	    			str1 = line_strc[0:line_strc.find('[')]
				str2 = line_strc[line_strc.find('[')+1:line_strc.find(']')]
				#print str1, str2
	    		elif check1 != -1:
				strc = line_strc[line_strc.find('(')+1:line_strc.find(')')+2]
				str1 = line_strc[line_strc.find('(')+1:line_strc.find(')')]
				str2 = str(0) 
				check3 = strc.find('+')
				if check3 != -1:
		    			str1 = strc[strc.find('(')+1:strc.find('+')]
		    			str2 = strc[strc.find('+')+1:strc.find(')')]
			#print str1,"::", str2
	    		f.write(linec)
	f2.close()

	#Adding copy statement only 
	#Adding copy statement only 
	if (check != -1):
   		f.write("\n\t Exp = (" + str1 + "[" + str2 + "-7]&1) | (" + str1 + "[" + str2 + " -6]&2) | ("+ str1 + "[" + str2 + "-5]&4) | ("+ str1+ "[" + str2 + "-4]&8) | ("+ str1+ "[" + str2 + "-3]&16) | ("+ str1+ "[" + str2 + "-2]&32) | ("+ str1+ "[" + str2 + "-1]&64) | ("+ str1+ "[" + str2 + "]&128); ")
	elif (check1 != -1 or check3!=-1):
   		f.write("\n\t Exp = (" + str1 + "[" + str2 + "]&1) | (" + str1 + "[" + str2 + " +1]&2) | ("+ str1 + "[" + str2 + "+2]&4) | ("+ str1+ "[" + str2 + "+3]&8) | ("+ str1+ "[" + str2 + "+4]&16) | ("+ str1+ "[" + str2 + "+5]&32) | ("+ str1+ "[" + str2 + "+6]&64) | ("+ str1+ "[" + str2 + "+7]&128); ")

	#Closing the Function
	f.write("}\n");

	fr = open("result.txt", "a") 
	fr.write("E"+str(expression+1) + ":\n") # write the line number to file
	fr.close();

	# open file in read mode 
	fn = open(path2, 'r') 
   
	# read the content of the file line by line 
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[1]):
	          f.write(cont[i]) 
 
	fn.close()

	start = int(sys.argv[1])
	num = int(sys.argv[4])
	check = -1
	with open(path2) as f1:
    	   for i, line in enumerate(f1):
    	       if i >= start and i < start + num:
		    #print line.strip()
		    line_str = line.strip()
		    print line_str
		    check = line_str.find('[')
		    if check != -1:
		    	str1 = line_str[0:line_str.find('[')]
			str2 = line_str[line_str.find('[')+1:line_str.find(']')]
		    f.write(line)
            
	f1.close();
	#Adding copy statement only 
	if check != -1:
	   f.write("\tSubop =" + str1 + "[" +str2+ "]; \n }\n")
	# End of the roundFunction
	return current_line
def fun_cbmc_one ():

	# open file in read mode 
	fn = open(path1, 'r') 
  
	# read the content of the file line by line 
	print "Reading the initial contents from Implementation\n"
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[3]):
        	  f.write(cont[i]) 
  
	# close the file 
	fn.close() 

	print "Extracting the Program Expressions \n"
	start_c = int(sys.argv[3])#181
	num_c = int(sys.argv[4])
	check1 = -1
	expression=0
	check = -1
	check1 = -1
	with open(path1) as f1:
    	   for i, line in enumerate(f1):
    	       if i >= start_c and i < start_c + num_c:
		    #print line.strip()
		    line_str = line.strip()
		    print line_str
		    current_line = i
		    if line_str.find("/*")== -1 and line_str.find("//")==-1 and line_str.find("{")==-1 and line_str.find("}")==-1:
		    	check = line_str.find('[')
		    	check1 = line_str.find('] =')
		    	expression = i
		    	if check != -1 and check1 != -1:
		    		str1 = line_str[0:line_str.find('[')]
				str2 = line_str[line_str.find('[')+1:line_str.find(']')]
		    f.write(line)
            
	f1.close();
	#Adding copy statement only 
	if check != -1 and check1 != -1:
	   f.write("\t Exp =" + str1 + "[" +str2+ "]; \n")
	   print "\t Exp =",str1,"[",str2, "]; \n"
	   #Closing the Function
	   f.write("}\n");


	fr = open("result.txt", "a") 
	fr.write("E"+str(expression+1) + ":\n") # write the line number to file
	fr.close();

	# open file in read mode 
	fn = open(path2, 'r') 
   
	# read the content of the file line by line 
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)): 
		if i < int(sys.argv[1]):
	          f.write(cont[i]) 
 
	fn.close()

	start = int(sys.argv[1])
	num = int(sys.argv[2])
	check = -1
	with open(path2) as f1:
    	   for i, line in enumerate(f1):
    	       if i >= start and i < start + num:
		    #print line.strip()
		    line_str = line.strip()
		    print line_str
		    check = line_str.find('[')
		    if check != -1:
		    	str1 = line_str[0:line_str.find('[')]
			str2 = line_str[line_str.find('[')+1:line_str.find(']')]
		    f.write(line)
            
	f1.close();
	#Adding copy statement only 
	if check != -1:
	   f.write("\tSubop =" + str1 + "[" +str2+ "]; \n }\n")
	# End of the roundFunction
	return current_line

def one_dimesion(fun_list,line):
	print "Assume the order of bytes as :\n";
	#for i in range(16):
	#  print "state[",i,"] <--> plaintext[",i,"]\n"
	
    	string5 = """
int main(int argc, char **argv)
	{       
    	uint8_t plaintext[16];
   	uint8_t ciphertext[16]; 
	uint8_t roundkeys[176];
	uint8_t key[] = {
		0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c
	};
    	uint8_t state1[16];
    	uint8_t i, j;	
    	unsigned char nondet_uchar();
	"""
	f.write(string5+"\n")

	for i in range(16):
	  f.write("  state1["+ str(i) + "] = nondet_uchar() "+ ";\n")
 
	string6 = """  
      __CPROVER_assume(state1[0]==plaintext[0] && state1[1]==plaintext[1] && state1[2]==plaintext[2] && state1[3]==plaintext[3] && state1[4]==plaintext[4] && state1[5]==plaintext[5] && state1[6]==plaintext[6] && state1[7]==plaintext[7] && state1[8]==plaintext[8] && state1[9]==plaintext[9] && state1[10]==plaintext[10] && state1[11]==plaintext[11] && state1[12]==plaintext[12] && state1[13]==plaintext[13] && state1[14]==plaintext[14] && state1[15]==plaintext[15] );
	"""
	f.write(string6+"\n")
	f.write("\taes_key_schedule_128(key, roundkeys);\n")
	string7 = """ 
		aes_encrypt_128(roundkeys, plaintext, ciphertext);
		roundFunction(state1);
    	       assert(Subop==ciphertext[0] || Subop==ciphertext[1] || Subop==ciphertext[2] || Subop==ciphertext[3] );
		return 0;
	}
	"""
	f.write(string7+"\n")


def multi_dimension(fun_list,line):
	print "Assume the order of bytes as :\n";
	#for i in range(4):
	#  for j in range(4):
	#  	print "state[",i,"][",j,"] <--> plaintext[",i*4+j,"]\n"
	
    	string5 = """
  int main(int argc, char **argv)
	{       
    	uint8_t plaintext[16];
   	uint8_t ciphertext[16]; 
    	uint8_t state1[4][4];
    	uint8_t i, j;	
    	unsigned char nondet_uchar();
	"""
	f.write(string5+"\n")

	for i in range(16):
	  	f.write("\tplaintext["+ str(i) + "] = nondet_uchar() "+ ";\n")
 
	string6 = """  
  __CPROVER_assume(plaintext[0]==state[0][0] && plaintext[1]==state[1][0] && plaintext[2]==state[2][0] && plaintext[3]==state[3][0] && plaintext[4]==state[0][1] && plaintext[5]==state[1][1] && plaintext[6]==state[2][1] && plaintext[7]==state[3][1] && plaintext[8]==state[0][2] && plaintext[9]==state[1][2] && plaintext[10]==state[2][2] && plaintext[11]==state[3][2] && plaintext[12]==state[0][3] && plaintext[13]==state[1][3] && plaintext[14]==state[2][3] && plaintext[15]==state[3][3] );	"""
	f.write(string6+"\n")

	f.write("\tKeyExpansion();\n")
	#current_line = 192
	current_line = line
	# print current_line
	for i in range(len(fun_list)):
		x = fun_list[i]
		if current_line >= int(x[1]):
			f.write("\t"+x[0]+"\n")
	string7 = """ 
	    roundFunction(plaintext);
    	assert(Subop==Exp);
		return 0;
	}
	"""
	f.write(string7+"\n")
	
def multi_dimension_new(fun_list,line):
	print "Assume the order of bytes as :\n";
	#for i in range(4):
	#  for j in range(4):
	#  	print "state[",i,"][",j,"] <--> plaintext[",i*4+j,"]\n"
	
    	string5 = """
  int main(int argc, char **argv)
	{       
    	uint8_t plaintext[16];
   	uint8_t ciphertext[16]; 
    	uint8_t i, j;	
    	unsigned char nondet_uchar();
	"""
	f.write(string5+"\n")

	for i in range(16):
	  	f.write("\tplaintext["+ str(i) + "] = nondet_uchar() "+ ";\n")
 
	string6 = """  
  __CPROVER_assume(plaintext[0]==state[0] && plaintext[1]==state[1] && plaintext[2]==state[2] && plaintext[3]==state[3] && plaintext[4]==state[4] && plaintext[5]==state[5] && plaintext[6]==state[6] && plaintext[7]==state[7] && plaintext[8]==state[8] && plaintext[9]==state[9] && plaintext[10]==state[10] && plaintext[11]==state[11] && plaintext[12]==state[12] && plaintext[13]==state[13] && plaintext[14]==state[14] && plaintext[15]==state[15] );	"""
	f.write(string6+"\n")

	
	#current_line = 192
	current_line = line
	# print current_line
	for i in range(len(fun_list)):
		x = fun_list[i]
		if current_line >= int(x[1]):
			f.write("\t"+x[0]+"\n")
	string7 = """ 
	    roundFunction(plaintext);
    	assert(Subop==Exp);
		return 0;
	}
	"""
	f.write(string7+"\n")

#extract function for bitsliced implementations
def extract_input(fun_list,line):
	print "Assume the order of bytes as :\n";
	
    	string5 = """
  int main(int argc, char **argv)
	{       
    	uint8_t plaintext[16];
   	uint8_t ciphertext[16]; 
    	uint8_t state1[16];	
    	uint8_t i, j = 0, k = 0;
	static ULONG pt[128];		/* Current plaintext block being encrypted */
	static ULONG ct[128];
	unsigned char out; 
	unsigned char x = 0;
  	unsigned char count = 0;
	static unsigned char	key[16] ={0x2b,0x7e,0x15,0x16,0x28,0xae,0xd2,0xa6,0xab,0xf7,0x15,0x88,0x09,0xcf,0x4f,0x3c};		
    	unsigned char nondet_uchar();
	"""
	f.write(string5+"\n")

	for i in range(16):
	  	f.write("\tstate1["+ str(i) + "] = nondet_uchar() "+ ";\n")
 
	string6 = """  
   __CPROVER_assume(state1[0] < 127 && state1[1] < 127 && state1[2] < 127 && state1[3] < 127 && state1[4] < 127 && state1[5] < 127 && state1[6] < 127&& state1[7] < 127&& state1[8] < 127&& state1[9] < 127&& state1[10] < 127&& state1[11] < 127&& state1[12] < 127&& state1[13] < 127&& state1[14] < 127&& state1[15] < 127);
  
  for(i = 0; i<128;i+=16){
	   if (k >= 14){ k = 1;}
           out = state1[k];
	   for(x=0 ; x < 16; x+=2, out >>=1 ) {
		pt[i+x] = ( (out&0x1) ? ~0ULL : 0ULL );  
		}
       k+=4;
  }
    k = 2;
    for(i = 1; i<128;i+=16){
	   if (k >= 16){ k = 3;}
	   out = state1[k];
	   for(x=0 ; x < 14; x+=2, out >>=1 ) {
		pt[i+x] = ( (out&0x1) ? ~0ULL : 0ULL );  
		}
       k+=4;
  }"""
	f.write(string6+"\n")

	f.write("\tfnExpandKey(key);\n")
	#current_line = 192
	current_line = line
	# print current_line
	for i in range(len(fun_list)):
		x = fun_list[i]
		if current_line >= int(x[1]):
			f.write("\t"+x[0]+"\n")
	string7 = """ 
	    roundFunction(state1);
    	assert(Subop==Exp);
		return 0;
	}
	"""
	f.write(string7+"\n")

def initalize_conditions(fun_list,line,n):
	# print "Enter the state matrix format\n"
	# print "1. Array of size 16 bytes\n"
	# print "2. 4x4 Matrix \n"

	if n==1:
		print "Assume Array of 16 Bytes --> plaintext[16]\n";
		one_dimesion(fun_list,line)
	elif n == 2:
		print "Assume 4 x 4 Matrix --> state[4][4]\n";
		multi_dimension(fun_list,line)
	elif n == 3:
		print "Need to define the Extract Function\n";
		extract_input(fun_list,line)
	elif n == 4:
		multi_dimension_new(fun_list,line)

	#f.close();

if __name__ == '__main__':
	type1 = int(sys.argv[7])
	folder = str(sys.argv[8])

	fun_list = fun_b.convert_tuple(folder)
	assume_type = 0
	if type1 == 2:
		line = fun_ttable()
		assume_type = 1
	elif type1 == 1:
		line = fun_cbmc()
		assume_type = 2
	elif type1 == 3:
		line = fun_bitsliced()
		assume_type = 3
	elif type1 == 4:
		print "Need to Handle\n"
		line = fun_cbmc_one()
		assume_type = 4
	initalize_conditions(fun_list,line,assume_type)
	f.close()
