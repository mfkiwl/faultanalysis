import sys, getopt
f = open("Mapping.sh", "w")

#Generate fault mapping script based on implementations
def header_type(path1,path2,folder):
	string1 = """#!/bin/sh
#For verification of one function in C maps to how many lines in synthesised C
rm result.txt
check="VERIFICATION FAILED"
i=1
SYN_MAX=0 #maximum lines to check for synthesis
SYN_START=0
SYN_i=0
IMP_MAX=1
line_count=0
count=0
"""
	f.write(string1)
	f.write("PATH1="+path1+"\n")
	f.write("PATH2="+path2+"\n")
	f.write("FOLDER="+folder+"\n")

def byte_main(start_syn,max_s,start_imp,max_i,type_imp):
	f.write("start_line=" +str(start_syn)+"\n")
   	f.write("start_imp=" +str(start_imp)+"\n")
   	f.write("type_imp=" +str(type_imp)+"\n")
	f.write("end_line=$start_line"+"\n")
	f.write("while [ $IMP_MAX -le " + str(max_i)+" ]\n")
	f.write("do\n")
	f.write("\twhile [ \"$check\" != \"VERIFICATION SUCCESSFUL\" -a $SYN_MAX -le " + str(max_s) + " ]")

	string2 = """
 	do # add the lines from synthesised C
   		echo "Looping ... number $i"
   		python verify.py $start_line $i $start_imp $IMP_MAX $PATH1 $PATH2 $type_imp $FOLDER
   		cbmc mapping_function.c --unwind 130 > check.txt
   		echo $(grep -hr "FAILED" check.txt)
   		check=$(grep -hr "SUCCESSFUL" check.txt)
   		echo $check
   		i=$((i=i+1))
   		SYN_MAX=$((SYN_MAX=SYN_MAX+1))	
 	done
	if [ "$check" != "VERIFICATION FAILED" -a "$check" != "" ]
	then
		echo $check
   		echo "start line : $end_line"
		line_count=$end_line
   		end_line=$start_line
   		if [ "$type_imp" != 3 ]
   		then
   			end_line=$((end_line=end_line+i-1))
   		else
   			end_line=$((end_line=end_line+IMP_MAX))
   		fi
   		echo "End Line : $end_line"
   		echo "G$end_line\\n\\n" >> result.txt
		end_line=$((end_line=end_line+1))
		echo "---------------------------------------"  
   		SYN_START=$start_line 
   		SYN_i=$i
   		SYN_MAX=0
	else
   		i=$SYN_i
   		echo "counter value $i" 
   		SYN_MAX=0
	fi
	IMP_MAX=$((IMP_MAX=IMP_MAX+1))
	check="VERIFICATION FAILED"
	
done
"""
	f.write(string2+"\n")

def u32_main(start_syn,max_s,start_imp,max_i,type_imp):
	f.write("start_line=" +str(start_syn)+"\n")
   	f.write("start_imp=" +str(start_imp)+"\n")
   	f.write("type_imp=" +str(type_imp)+"\n")
	f.write("end_line=$start_line"+"\n")
	f.write("while [ $IMP_MAX -le " + str(max_i)+" ]\n")
	f.write("do\n")
	f.write("\twhile [ $SYN_MAX -le " + str(max_s) + " ]")

	string2 = """
 	do # add the lines from synthesised C
   		echo "Looping ... number $i"
   		python verify.py $start_line $i $start_imp $IMP_MAX $PATH1 $PATH2 $type_imp $FOLDER
   		cbmc mapping_function.c --unwind 100 > check.txt
   		echo $(grep -hr "FAILED" check.txt)
   		check=$(grep -hr "SUCCESSFUL" check.txt)
   		echo $check
   		i=$((i=i+1))
   		SYN_MAX=$((SYN_MAX=SYN_MAX+1))
   		if [ "$check" != "VERIFICATION FAILED" -a "$check" != "" ]
		then
			count=$((count=count+1))
   			echo "start line : $end_line"
   			#echo "start line : $end_line" >> result.txt
   			end_line=$start_line
   			end_line=$((end_line=end_line+i-1))
   			echo "End Line : $end_line"
			echo "G$end_line\\n\\n" >> result.txt	
			end_line=$((end_line=end_line+1))
			echo "---------------------------------------"  
   			SYN_START=$start_line 
   			SYN_i=$i
			SYN_MAX=0
			if [ "$count" -ge 4 ]
			then
				count=0
				break
			fi
		fi
	done
	i=0
	SYN_MAX=0
	IMP_MAX=$((IMP_MAX=IMP_MAX+1))
	check="VERIFICATION FAILED"
	
done
"""
	f.write(string2+"\n")

def final_conversions():
	stringf="""
grep -v -e '^$' result.txt  > temp.txt
cat -n temp.txt | sort -uk2 | sort -nk1 | cut -f2- > result1.txt
sed 's/E/\\nE/' result1.txt  > final.txt
rm result.txt
rm result1.txt
rm temp.txt
rm check.txt
echo "-----------Mapping is done successfully--------------------"
"""
	f.write(stringf+"\n")

def equivalence_mapping(start_syn,max_s,start_imp,max_i,file1,file2,type_imp,folder):

    	path1 = file1
    	path2 = file2
    	folder = folder
	header_type(path1,path2,folder)
	# for byte based sbox
	if type_imp == "lookup":
	   byte_main(start_syn,max_s,start_imp,max_i,1)
	# for t-table based implementations
	if type_imp == "t-table":
	  u32_main(start_syn,max_s,start_imp,max_i,2)
	if type_imp == "bitsliced":
		byte_main(start_syn,max_s,start_imp,max_i,3)
	if type_imp == "lookup2":
		byte_main(start_syn,max_s,start_imp,max_i,4)

	final_conversions()
	f.close()
