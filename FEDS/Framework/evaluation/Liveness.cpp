/******************************************************************************
 * Department of Computer Science Engineering, IIT Madras
 *
 * Liveness.cpp
 *
 * "LLVM parser used in FEDS" , C code
 *
 * January 2020
 *
 * Author : Keerthi K
 *****************************************************************************/
#include "llvm/InitializePasses.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/IR/Value.h"
#include "llvm/Pass.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/CommandLine.h"
#include  <iostream>
#include <ctype.h>
#include <iomanip>  
#include <sstream>
#include <deque>
#include <stack>
#include <map>
#include <utility>
#include <vector>
#include <set>
#include <list>
#include "DFA.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

using namespace llvm;
// to start and stop the slicing based on the fault line number
unsigned start_line;
unsigned stop_line;
std::string fun_name;
Instruction * start_instr;
Instruction * stop_instr;
unsigned in1;
unsigned in2;
std::map<std::string,unsigned> itercount;
int flag1 = 1;
int exitflag = 0;

std::map<unsigned,Instruction*> opcounter_map;
std::map<unsigned,Instruction*> temp_map;
std::map<Instruction*,unsigned> Instr_check; 
std::map<std::string,int> sliceblock;
std::map<unsigned,unsigned> Allocset;

//Storing the Line number For the instruction
std::map<unsigned,Instruction*>LineToInstr;
std::map<unsigned,std::set<Instruction*>> LineToInstrset;

typedef std::pair<unsigned, unsigned> INDEX_PAIR;
std::map<Value *,INDEX_PAIR> getindex;

INDEX_PAIR M2_PAIR;

// list for temp_map
std::list<std::map<unsigned,Instruction*>>slice_list; 
//stack of sliceblock and Allocaset(For Function calls)
std::stack<std::map<std::string,int>>sliceblock_stack;
std::stack<std::map<unsigned,unsigned>>Allocset_stack;
std::stack<std::map<unsigned,Instruction*>>temp_stack;
std::stack<std::map<unsigned,Instruction*>>opcounter_stack;
std::stack<std::string>current_stack;

unsigned M1_index;
unsigned getflag = 0;
unsigned type_imp = 0;
Module *Main;

class ReachingInfo : public Info {
   std::set<unsigned> reachingSet;

   std::map<unsigned,std::set<std::list<unsigned>>> indexMap;

  public:
  	ReachingInfo() {}
  	ReachingInfo(unsigned index) {
    		reachingSet.insert(index);
  	}
      	ReachingInfo(std::vector<unsigned> indexVector) {
    		for (unsigned index : indexVector) {
      			reachingSet.insert(index);
    		}
  	}
  	ReachingInfo(std::vector<unsigned> indexVector, std::map<unsigned, std::list<unsigned>> indexList) {
    		for (unsigned index : indexVector) {
      			reachingSet.insert(index);
    		}
    		std::set<std::list<unsigned>> set;
    		for(auto it = indexList.begin(); it != indexList.end(); ++it){
    			std::list<unsigned> list = it->second;
    			set.insert(it->second);
    			indexMap[it->first] = set;
    	 	}
  	}

  	ReachingInfo(std::vector<unsigned> indexVector, std::map<unsigned,std::set<std::list<unsigned>>> indexmap) {
    		for (unsigned index : indexVector) {
      			reachingSet.insert(index);
    		}
    		for (auto index : indexmap) {
      			indexMap.insert(index);
    		}
  	}

        ReachingInfo(ReachingInfo& reachingInfo) : Info(reachingInfo) {
    		reachingSet = std::set<unsigned>(reachingInfo.reachingSet);
    		indexMap = std::map<unsigned,std::set<std::list<unsigned>>>(reachingInfo.indexMap);
    	}
    
    	static bool equals(ReachingInfo * info1, ReachingInfo * info2) {
    		std::vector<unsigned> reachingVector1;
    		std::vector<unsigned> reachingVector2;
    		if (info1 -> reachingSet.size() != info2 -> reachingSet.size()) {
      			return false;
    		}
    		for (auto it1 = info1 -> reachingSet.begin(), it2 = info2 -> reachingSet.begin(); it1 != info1 -> reachingSet.end(); ++it1, ++it2) {
      			reachingVector1.push_back(*it1);
      			reachingVector2.push_back(*it2);
    		}
    		std::sort(reachingVector1.begin(), reachingVector1.end());
    		std::sort(reachingVector2.begin(), reachingVector2.end());
    		for (int i = 0; i < (int) reachingVector1.size(); i++) {
      			if (reachingVector1[i] != reachingVector2[i]) {
        			return false;
      			}
   	    	}
   		return true;
  	}

  	static bool find1(unsigned index, ReachingInfo *info2){
  		for (auto it1 = info2 -> reachingSet.begin(); it1 != info2 -> reachingSet.end();  ++it1) {
  			if(*it1==index){
  			return true;
  			}
    		}
  		
  		return false;
  	}
  	 static bool findmap(unsigned index, std::list<unsigned> list, ReachingInfo *info2){
  		if(info2 -> indexMap.find(index) != info2 -> indexMap.end()){
  	 		std::set<std::list<unsigned>> set = info2 -> indexMap[index];
  	 		if(set.find(list) != set.end()){
  	 			return true;
  	 		}
  	 	 }
  	 	return false;
  	}

  	 static bool findgep(unsigned index, std::list<unsigned> list, ReachingInfo *info2){
  		if(info2 -> indexMap.find(index) != info2 -> indexMap.end()){
  	 		std::set<std::list<unsigned>> set = info2 -> indexMap[index];
  	 		for(auto it = set.begin(); it != set.end(); ++it){
  	 			 std::list<unsigned> list1 = *it;
  	 			 if(*list1.begin()==*list.begin()){
  	 			 	return true;
  	 			 }
  	 		}
  	 	 }
  	 	return false;
  	}


  	static std::set<std::list<unsigned>> findmap1(unsigned index, ReachingInfo *info2){
  		std::set<std::list<unsigned>> set2;
  		if(info2 -> indexMap.find(index) != info2 -> indexMap.end()){
  			std::set<std::list<unsigned>> set = info2->indexMap[index];
  	 			return set;
  	 	 }
  	 	return set2;
  	}

  	static bool find2(std::vector<unsigned> index, ReachingInfo *info2){
  		for (auto it1 = info2 -> reachingSet.begin(); it1 != info2 -> reachingSet.end();  ++it1) {
  			for (auto ii = index.begin(); ii != index.end();  ++ii) {
  				if(*it1==*ii){
  					return true;
  				}
  		        }
    		}
  		
  		return false;
  	}

  	static Info* join(ReachingInfo * info1, ReachingInfo * info2, ReachingInfo * result) {
    		for (auto it = info1 -> reachingSet.begin(); it != info1 -> reachingSet.end(); ++it) {
      			result -> reachingSet.insert(*it);
    		}
    		for (auto it = info2 -> reachingSet.begin(); it != info2 -> reachingSet.end(); ++it) {
      			result -> reachingSet.insert(*it);
    		}
    		return nullptr;
  	}

  	static Info* joinIndex(ReachingInfo * info1, ReachingInfo * info2, ReachingInfo * result) {
    		for (auto it = info1 -> indexMap.begin(); it != info1 -> indexMap.end(); ++it) {
      			result -> indexMap.insert(*it);
    		}
    		for (auto it = info2 -> indexMap.begin(); it != info2 -> indexMap.end(); ++it) {
      			if(result -> indexMap.find(it->first) != result -> indexMap.end()){
      				std::set<std::list<unsigned>> set = info2 -> indexMap[it->first];
      				std::set<std::list<unsigned>> set2 = result -> indexMap[it->first];
      				for (auto it1 = set.begin(); it1 != set.end(); ++it1) {
      					set2.insert(*it1);
    				}
    				result -> indexMap[it->first] = set2;
	
      			}	
      			else{
      				result -> indexMap.insert(*it);
      			}
    		}
    		return nullptr;
  	}

  	void print() {
    		for (auto it = reachingSet.begin(); it != reachingSet.end(); ++it) {
      			errs() << *it << "|";
    		}
    		errs() << "\n";
  	}

  	void printIndex() {
  		for(auto it = indexMap.begin(); it != indexMap.end(); ++it) {
  			errs() << it->first << "-> ";
  			for(auto it1 = it->second.begin(); it1 != it->second.end(); ++it1){
  				std::list<unsigned> list = *it1;
  				for (auto index : list) {
      				errs() << index << ":";
    			}

  			}
  		}
  		errs() << "\n";
  	}

  	void minus(unsigned index) {
    		reachingSet.erase(index);
    	}
    	void minusmap(unsigned index){
    		indexMap.erase(index);
    	}
    	void minusmap(unsigned index, std::list<unsigned> list) {
    		auto it = indexMap.find(index);
    		std::set<std::list<unsigned>> set = indexMap[it->first];

    		if(set.find(list) != set.end()){
  	 		auto it1 = set.find(list);
  	 		set.erase(it1);
  	 	}
  	 	if(set.size()==0){
  	 		indexMap.erase(it);
  	 	}
  	 	else
  	 	{
  	 		indexMap[it->first] = set;
  	    }
    	}

  	void minus(std::vector<unsigned>& minusVector) {
    		for (unsigned index : minusVector){
      			reachingSet.erase(index);
    		}
  	}
};

 template <class Info, bool Direction>
 class LivenessAnalysis : public DataFlowAnalysis<Info, Direction> {
    	typedef std::pair<unsigned, unsigned> Edge;

    	bool map_compare (std::map<unsigned,Instruction*> const &lhs, std::map<unsigned,Instruction*> const &rhs) {
  		// No predicate needed because there is operator== for pairs already.
     		return (lhs.size() == rhs.size() && std::equal(lhs.begin(), lhs.end(),rhs.begin()));
    	}

  	void print_map(std::map<unsigned,Instruction*> M1){
    		for(auto it = M1.begin(); it!=M1.end(); ++it){
        		Instruction *instr = it->second;
        		if(instr){
           		   errs() << *instr << "\n";
        		}
    		}
    		errs() << "\n";

  	}

  	//Finding if a map in the list and the lhs are same  and Return 1 when maps are equal
  	bool matching_map(std::map<unsigned,Instruction*> const &lhs){
     		unsigned num = 0;
     		for(auto it = slice_list.begin(); it!=slice_list.end();it++){
        		num = map_compare(*it,lhs);
        		if(num!=0){
          			return num;
        		}
    		}
    		return num;
  	}

  	// Finding the user operand for the function calls
  	Value* OperandName(Value *operand){
     		StringRef first = "null";
     		Value *pointer;
     		if(dyn_cast<LoadInst>(operand)){
       			LoadInst *SI1 = dyn_cast<LoadInst>(operand);
       			pointer = SI1->getPointerOperand();
       			first = SI1->getPointerOperand()->getName();
       			if(first=="")
        		{
        			pointer = OperandName(pointer);
        		} 
      		}
    		return pointer;
  	}

  	std::list<unsigned> global_index_list;
  	// Fine elementptr for the instruction  
  	Value* GetElementHandle(Value* operand){
  		GEPOperator *GEP = dyn_cast<GEPOperator>(operand);
      		Value* var = GEP ->getOperand(0)->stripInBoundsOffsets();
      		// Skip 0 as it gives the base pointer and find the index
      		for (unsigned i = 2; i < GEP -> getNumOperands(); i++) {
        		if (ConstantInt *CI = dyn_cast<ConstantInt>(GEP->getOperand(i))){
            			unsigned index = CI -> getZExtValue();
            			global_index_list.push_back(index);
         		} 
      		} 
    		return var;
  	}

  void category1(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges,std::vector<Info *> & Infos) {
     	std::map<int,std::string> bname = this->getBlockname();
	std::map<Instruction*, unsigned> InstrToIndex = this -> getInstrToIndex();
	std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
	std::map<GlobalVariable*,unsigned> Global_InstrToIndex = this -> getGlobalInstrToIndex();
  	std::map<unsigned,GlobalVariable*> Global_IndexToInstr = this -> getGlobalIndexToInstr();
	unsigned instrIndex = InstrToIndex[I];
	std::string name =  bname[instrIndex];
      	unsigned flag = 0;
      	unsigned check =0;
    	Value* var;
	std::vector<unsigned> indexVector;
	std::map<unsigned,std::list<unsigned>> indexMap;

    	for (unsigned i = 0; i < I -> getNumOperands(); i++) {
      		Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));
      		if (instr) {
      			indexVector.push_back(InstrToIndex[instr]);
        		if(I->getOperand(i)->getType()->isPointerTy()){
        			if(instr->getOpcode()== Instruction::GetElementPtr){
        				if(isa<GEPOperator>(instr->getOperand(0))){
        				   var = GetElementHandle(instr -> getOperand(0));
        				    if(GlobalVariable * GV = dyn_cast<GlobalVariable>(var)){
              					 indexVector.push_back(Global_InstrToIndex[GV]);
               					 unsigned v = Global_InstrToIndex[GV];
               				 	 indexMap[v] = global_index_list;
               				     	  check = 1;

          				   }			
        				}
        				else{
        					 // added this part for bitsliced implementation
          				   	 Instruction *v_instr = dyn_cast<Instruction>(instr->getOperand(0));
          				   	 indexVector.pop_back();
          				   	 indexVector.push_back(InstrToIndex[v_instr]);

          				}	
   
        			}
        		}
      		}
           	else if(isa<GEPOperator>(I->getOperand(i))){
             		var = GetElementHandle(I -> getOperand(i));
             		if(GlobalVariable * GV = dyn_cast<GlobalVariable>(var)){
               			indexVector.push_back(Global_InstrToIndex[GV]);
               			unsigned v = Global_InstrToIndex[GV];
               			indexMap[v] = global_index_list;
               			flag = 1;

            		 }
             		else if(Instruction *v_instr = dyn_cast<Instruction>(var)){
                		indexVector.push_back(InstrToIndex[instr]);
                		unsigned v = InstrToIndex[v_instr];
                		indexMap[v] = global_index_list;
                		flag = 1;
             		}

              		global_index_list.clear();
          	}  
      	  	else
      		 	indexVector.push_back(0); // Handle initilization of variables
    		}

  		Info *info = new Info();
  		/*Combine all the variables in incoming Edges */
    		for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
      			Edge edge = std::make_pair(*it, instrIndex);
      			Info *edgeInfo = EdgeToInfo[edge];
      			Info::join(info, edgeInfo, info);
      			Info::joinIndex(info, edgeInfo,info);
    		}
    		//Remove the variables after write operation (operand(1)) - add the instruction in opcounter_map
       		if(Info::find1(indexVector[1],info)){   
        		if(flag == 1){
        			std::list<unsigned> L1 = indexMap[indexVector[1]];
        	 		if(Info::findmap(indexVector[1], L1, info)){
        		 		indexVector.erase(indexVector.begin()+1);
        		 		indexMap.erase(indexVector[1]);
    			 		Info *info1 = new Info(indexVector,indexMap); 

    	 		 		info -> minusmap(indexVector[1],L1);
    			 		info -> minus(indexVector[1]); 

    			 		Info::join(info, info1,info);
    			 		Info::joinIndex(info, info1, info);

    			 		Instr_check[I] = 1;
    			 		sliceblock[name] = 1;
    			 		opcounter_map[instrIndex] = I;
        	 		}
           
        		}
        		else{
    				indexVector.erase(indexVector.begin()+1);
    				Info *info1 = new Info(indexVector);  
    				info -> minus(indexVector[1]); 
       				Info::join(info, info1,info);
       				sliceblock[name] = 1;	
       				Instr_check[I] = 1;
			   	opcounter_map[instrIndex] = I;
         		}
    		}
    		else{
    			if(check==1){
    		    		if(Info::find1(indexVector[2],info)){   
    		    			std::list<unsigned> L1 = indexMap[indexVector[2]];
         	  	   		if(Info::findgep(indexVector[2],L1, info)){
		   	 	    		opcounter_map[instrIndex] = I;
		   	 			Instr_check[I] = 1;
       			 			Info *info2 = new Info(indexVector);
       			 			Info::join(info, info2,info);	
       			    			sliceblock[name] = 1;
       			    
		   	   		}
    		   		}
    			}
    		}

    		//Add the reaching info into the outgoing edges
    		for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
      			Info *newInfo = new Info(*info);
      			Infos.push_back(newInfo);
    		}
    		delete info;
    	return;
  }
  void category2(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges, std::vector<Info *> & Infos) {
  	std::map<int,std::string> bname = this->getBlockname();
  	std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
    	std::map<Instruction*,unsigned>InstrToIndex = this -> getInstrToIndex();
    	std::map<GlobalVariable*,unsigned> Global_InstrToindex = this -> getGlobalInstrToIndex();
    	unsigned instrIndex = InstrToIndex[I];
    	std::string name =  bname[instrIndex];
	std::vector<unsigned> indexVector;

    	for (unsigned i = 0; i < I -> getNumOperands(); i++) {
    		Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));
      		if (instr) {
        		indexVector.push_back(InstrToIndex[instr]);
      		}
      		else if(GlobalVariable * GV = dyn_cast<GlobalVariable>(I ->getOperand(i))){ //check if operand is global

     			indexVector.push_back(Global_InstrToindex[GV]);
   			}
    	}
    	/*Combine all the variables in incoming Edges */
    	Info *info = new Info(indexVector); 
    	for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
      		Edge edge(*it, instrIndex);
      		Info *edgeInfo = EdgeToInfo[edge];
      		Info::join(info, edgeInfo, info);
      		Info::joinIndex(info, edgeInfo, info);
    	}
    	/* Handle cases:
	 * Alloca : using Allocset - added during Load operation
         * Default : Handle normally
         */
    	if(I->getOpcode()==Instruction::Alloca){  // need to add alloca only when load instruction is found
    		if(Allocset.find(instrIndex)->second==1){
    			Instr_check[I] = 1;
    			opcounter_map[instrIndex] = I;
    			sliceblock[name] = 1;
    		}
   	}

    	//Add the reaching info into the outgoing edges
    	for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
      		Info *newInfo = new Info(*info);
      		Infos.push_back(newInfo);
    	}
    	delete info;
  	return;
  }

  void category3(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges,std::vector<Info *> & Infos) {
        std::map<int,std::string>	bname = this->getBlockname();
	std::map<Instruction*, unsigned> InstrToIndex = this -> getInstrToIndex();
	std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
	std::map<GlobalVariable*,unsigned> Global_InstrToIndex = this -> getGlobalInstrToIndex();
	unsigned instrIndex = InstrToIndex[I];
	std::string name =  bname[instrIndex];
   	unsigned flag = 0;
    	Value *var;
	std::vector<unsigned> indexVector;
	std::map<unsigned,std::list<unsigned>> indexMap;

    	for (unsigned i = 0; i < I -> getNumOperands(); i++) {

      		Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));
      		if (instr) {
             		indexVector.push_back(InstrToIndex[instr]);
      		}
         	else if(isa<GEPOperator>(I->getOperand(i))){
             		var = GetElementHandle(I -> getOperand(i));
                       
             		if(GlobalVariable * GV = dyn_cast<GlobalVariable>(var)){
               			indexVector.push_back(Global_InstrToIndex[GV]);
               			unsigned v = Global_InstrToIndex[GV];
               			indexMap[v] = global_index_list;
               			flag = 1;
			}
             		else if(Instruction *v_instr = dyn_cast<Instruction>(var)){
                		indexVector.push_back(InstrToIndex[instr]);
                		unsigned v = InstrToIndex[v_instr];
                		indexMap[v] = global_index_list;
                		flag = 1;
             		}
              		global_index_list.clear();
           	}  
    	}

    	/*Combine all the variables in incoming Edges */
  	Info *info = new Info();
    	for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
      		Edge edge = std::make_pair(*it, instrIndex);
      		Info *edgeInfo = EdgeToInfo[edge];
      		Info::join(info, edgeInfo, info);
      		Info::joinIndex(info, edgeInfo, info);
    	}

    	// Find the index of the instruction- Add the operands
	if(Info::find1(instrIndex,info)){
		  if(flag==1){
		  	opcounter_map[instrIndex] = I;
    			Info *info1 = new Info(indexVector,indexMap);
    			Info::join(info, info1,info);
    			Info::joinIndex(info, info1, info);
    			Instr_check[I] = 1;
    			sliceblock[name] = 1;
    			info -> minus(InstrToIndex[I]);
		  }
		  else{
		  	opcounter_map[instrIndex] = I;
    			Info *info1 = new Info(indexVector);
       			Info::join(info, info1,info);	
       			Instr_check[I] = 1;
       			sliceblock[name] = 1;
       			info -> minus(InstrToIndex[I]);
		  }

       	  	if(I->getOpcode()==Instruction::Load){ // find the required alloca
       			unsigned alloc_index = indexVector[0]; 
       			Allocset[alloc_index] = 1;
       	  	}
       }
      //Add the reaching info into the outgoing edges
    	for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
      		Info *newInfo = new Info(*info);
      		Infos.push_back(newInfo);
    	}
    	delete info;
    return;
  }

   void getelement(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges,std::vector<Info *> & Infos) {
            std::map<int,std::string>	bname = this->getBlockname();
	    std::map<Instruction*, unsigned> InstrToIndex = this -> getInstrToIndex();
	    std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
	    std::map<GlobalVariable*,unsigned> Global_InstrToIndex = this -> getGlobalInstrToIndex();
	    unsigned instrIndex = InstrToIndex[I];
	    std::string name =  bname[instrIndex];
   	    unsigned flag = 0;
   	  
    	    Value *var;
	    std::vector<unsigned> indexVector;
	    std::map<unsigned,std::list<unsigned>> indexMap;

    	    for (unsigned i = 0; i < I -> getNumOperands(); i++) {

      		Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));
      		if (instr) {
             		indexVector.push_back(InstrToIndex[instr]);
      		}
         	else if(isa<GEPOperator>(I->getOperand(i))){
             		var = GetElementHandle(I -> getOperand(i));
                       
             		if(GlobalVariable * GV = dyn_cast<GlobalVariable>(var)){
               			indexVector.push_back(Global_InstrToIndex[GV]);
               			unsigned v = Global_InstrToIndex[GV];
               			indexMap[v] = global_index_list;
               			flag = 1;
             		}
             		else if(Instruction *v_instr = dyn_cast<Instruction>(var)){
                		indexVector.push_back(InstrToIndex[instr]);
                		unsigned v = InstrToIndex[v_instr];
                		indexMap[v] = global_index_list;
                		flag = 1;
             		}
              		global_index_list.clear();
           	}  
    	}

    	/*Combine all the variables in incoming Edges */
  	Info *info = new Info();
    	for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
      		Edge edge = std::make_pair(*it, instrIndex);
      		Info *edgeInfo = EdgeToInfo[edge];
      		Info::join(info, edgeInfo, info);
      		Info::joinIndex(info, edgeInfo, info);
    	}
    	// Find the index of the instruction- Add the operands
	if(Info::find1(instrIndex,info)){
		  if(flag==1){
		  	opcounter_map[instrIndex] = I;
    			Info *info1 = new Info(indexVector,indexMap);
    			Info::join(info, info1,info);
    			Info::joinIndex(info, info1, info);
    			 Instr_check[I] = 1;
    			sliceblock[name] = 1;
    			info -> minus(InstrToIndex[I]);
		  }
		  else{
		  	opcounter_map[instrIndex] = I;
    			Info *info1 = new Info(indexVector);
       			Info::join(info, info1,info);	
       			Instr_check[I] = 1;
       			sliceblock[name] = 1;
       			info -> minus(InstrToIndex[I]);
		   }
         }
       //Add the reaching info into the outgoing edges
    	for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
      		Info *newInfo = new Info(*info);
      		Infos.push_back(newInfo);
    	}
    	
    	delete info;
    return;
  }


  void category4(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges, std::vector<Info *> & Infos) {
      std::map<int,std::string> bname = this->getBlockname();
      std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
      std::map<Instruction*,unsigned>InstrToIndex = this -> getInstrToIndex();
      std::map<GlobalVariable*,unsigned> Global_InstrToindex = this -> getGlobalInstrToIndex();

      unsigned instrIndex = InstrToIndex[I];
      std::string name =  bname[instrIndex];

      std::vector<unsigned> indexVector; //store the index of the argument
      std::vector<unsigned> ArgVector;

      unsigned i;
      for ( i = 0; i < I -> getNumOperands() - 1; i++) { // one operand removed : Last parameter is the function name
          Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));
          if (instr) {
          	indexVector.push_back(InstrToIndex[instr]);
          }

          if(dyn_cast<Instruction>(I -> getOperand(i))){
          	Value *operand = dyn_cast<Instruction>(I -> getOperand(i)); // Storing the arguments in Argvector

          	StringRef ptr = operand->getName();
          	if(ptr==""){
            		operand = OperandName(operand);
          	}
          	if(operand){
            		Instruction *ARG = dyn_cast<Instruction>(operand);
            		ArgVector.push_back(InstrToIndex[ARG]);
          	}
          }
      }
      /*Combine all the variables in incoming Edges */
      Info *info = new Info(indexVector); //for the operands in the function call
      for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
          Edge edge = std::make_pair(*it, instrIndex);
          Info *edgeInfo = EdgeToInfo[edge];
          Info::join(info, edgeInfo, info);
          Info::joinIndex(info,edgeInfo,info);
      }
      //check the relevant variable present in the function call
      std::vector<unsigned> fun_instr;
      std::map<unsigned,std::set<std::list<unsigned>>> ArgSet;
      
      //If there is no argument then check if the relevant variables contains Global variables.
      //If yes Slicing Criteria to the function is the Global Variable
      if( i >= 0 ){  
      	 fun_instr.clear();
      	 ArgSet.clear();
      	 //Go to function if global variable is present in the info
      	 for(auto it = Global_InstrToindex.begin(); it!=Global_InstrToindex.end();it++){;
      	   	if(Info::find1(it->second,info)){
      	   	 fun_instr.push_back(it->second); 
      	   	 	//newly added for the index of the array
      	   	  	std::set<std::list<unsigned>> ss = Info::findmap1(it->second, info);
         	 	if(ss.size() > 0){
 				ArgSet[it->second] = ss;
          		}
      	   	}
      	 }
      	 //Check if global variable present in the set of relevant variables
      	 if(!fun_instr.empty()){
      	 	Instr_check[I] = 1;
       		//errs() << "Adding call Instruction\n";
      	 	opcounter_map[instrIndex] = I;
      	 	sliceblock[name] = 1;
      	    	//ADD the current slice into the list
              	slice_list.push_back(opcounter_map);
            	opcounter_stack.push(opcounter_map);
                opcounter_map.clear();
                temp_stack.push(temp_map);
                temp_map.clear();
          	Allocset_stack.push(Allocset);
          	Allocset.clear();
          	sliceblock_stack.push(sliceblock);
          	sliceblock.clear();

          	//Jumping into the called Function
          	CallInst *CI = dyn_cast<CallInst>(I);
          	Function *Fun = dyn_cast<Function>(CI->getOperand(i)); //last operand

          	ReturnInfo rinfo;

          	rinfo = this->runWorklistAlgorithm3(&*Main,&*Fun,fun_instr,ArgSet);

          	fun_instr = rinfo.first;
          	ArgSet = rinfo.second;

          	// remove the non existing variables from the info
          	info = new Info(fun_instr,ArgSet);

          	//ADD the slice for function call into the list and pop the calling function maps from stack
             	slice_list.push_back(opcounter_map);
            	opcounter_map = opcounter_stack.top();
            	opcounter_stack.pop();
            	temp_map =  temp_stack.top();
            	temp_stack.pop();
          	Allocset = Allocset_stack.top();
          	Allocset_stack.pop();
          	sliceblock = sliceblock_stack.top();
          	sliceblock_stack.pop();
      	  }
       }

      //Add the reaching info into the outgoing edges
      for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
          Info *newInfo = new Info(*info);
          Infos.push_back(newInfo);
      }
      delete info;
    return;
  }
  void category5(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges, std::vector<Info *> & Infos) {
      std::map<int,std::string> bname = this->getBlockname();
      std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
      std::map<Instruction*,unsigned>InstrToIndex = this -> getInstrToIndex();
      std::map<GlobalVariable*,unsigned> Global_InstrToIndex = this -> getGlobalInstrToIndex();
      unsigned instrIndex = InstrToIndex[I];
      std::string name =  bname[instrIndex];
      //unsigned flag = 0;

      std::vector<unsigned> indexVector;
      std::map<unsigned,std::list<unsigned>> indexMap;

      for (unsigned i = 0; i < I -> getNumOperands(); i++) {
      		Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));	
         	if (instr) {
          		indexVector.push_back(InstrToIndex[instr]);
      	 	}
         	else if(isa<GEPOperator>(I->getOperand(i))){
             		Value* var = GetElementHandle(I -> getOperand(i));
                       
             		if(GlobalVariable * GV = dyn_cast<GlobalVariable>(var)){
               			indexVector.push_back(Global_InstrToIndex[GV]);
               			unsigned v = Global_InstrToIndex[GV];
               			indexMap[v] = global_index_list;

             		}
             		else if(Instruction *v_instr = dyn_cast<Instruction>(var)){
                		indexVector.push_back(InstrToIndex[instr]);
                		unsigned v = InstrToIndex[v_instr];
                		indexMap[v] = global_index_list;
             		}
              		global_index_list.clear();
           	}   
        }

      /*Combine all the variables in incoming Edges */
      Info *info = new Info(indexVector, indexMap);
    
      for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
      		Edge edge(*it, instrIndex);
      		Info *edgeInfo = EdgeToInfo[edge];
      		Info::join(info, edgeInfo, info);
      		Info::joinIndex(info, edgeInfo,info);
      }

      bool b1 = cast<CallInst>(I)->getCalledFunction()->isIntrinsic();
      //Remove instrinsic Instruction from the ll file
      if(b1==0){
      		Instr_check[I] = 1;
                opcounter_map[instrIndex] = I;
                sliceblock[name] = 1;
       }

    	//Add the reaching info into the outgoing edges
    	for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
      		Info *newInfo = new Info(*info);
      		Infos.push_back(newInfo);
    	}
    	delete info;
  	return;
  }
  void category6(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges, std::vector<Info *> & Infos) {
  	std::map<int,std::string> bname = this->getBlockname();
  	std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
    	std::map<Instruction*,unsigned>InstrToIndex = this -> getInstrToIndex();
    	std::map<GlobalVariable*,unsigned> Global_InstrToindex = this -> getGlobalInstrToIndex();
    	unsigned instrIndex = InstrToIndex[I];
    	std::string name =  bname[instrIndex];

  	std::vector<unsigned> indexVector;
    	for (unsigned i = 0; i < I -> getNumOperands(); i++) {
    		Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));
      		if (instr) {
        		indexVector.push_back(InstrToIndex[instr]);
      		}
    	}

    	/*Combine all the variables in incoming Edges */
    	Info *info = new Info(); 
    	for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
      		Edge edge(*it, instrIndex);
      		Info *edgeInfo = EdgeToInfo[edge];
      		Info::join(info, edgeInfo, info);
      		Info::joinIndex(info, edgeInfo, info);
    	 }
      	if(I->getOpcode()==Instruction::Br){
    	 	 if(opcounter_map.size() > 1){ 
            		for(auto it = opcounter_map.begin(); it != opcounter_map.end();it++){
                		temp_map.insert(*it);
            		}
         	}
         	opcounter_map.clear();
    	 	if(I->getNumOperands()>1){
    	 		  BranchInst *BI = dyn_cast<BranchInst>(I);
    	    	  	  std::string s1 = BI->getSuccessor(0)->getName().str();
		          std::string s2 = BI->getSuccessor(1)->getName().str();
			  if(sliceblock.find(s1)->second==1 || sliceblock.find(s2)->second==1){
				Info *info1 = new Info(indexVector); //If matches then add the operand
              			Info::join(info, info1,info); 
				opcounter_map[instrIndex] = I;  
				sliceblock[name]=1;
			  }
    	  	}
           	else{ 
    	  		opcounter_map[instrIndex] = I;
    	  	    	sliceblock[name]=0;
    	  	}
    	 }
    	//Add the reaching info into the outgoing edges
    	for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
      		Info *newInfo = new Info(*info);
      		Infos.push_back(newInfo);
    	}
    	delete info;
  	return;
  }
  void category7(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges, std::vector<Info *> & Infos) {
  	std::map<int,std::string> bname = this->getBlockname();
    	std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
    	std::map<Instruction*, unsigned> InstrToIndex = this -> getInstrToIndex();
    	std::map<unsigned, Instruction*> IndexToInstr = this ->getIndexToInstr();
    	unsigned instrIndex = InstrToIndex[I];
     	std::string name =  bname[instrIndex];

 	std::vector<unsigned> minusVector;
    	std::map<unsigned, std::vector<unsigned>> labelIndexMap;
    	if (isa<PHINode>(I)) {
      		for (unsigned i = 0; i < I -> getNumOperands(); i++) {
        		PHINode *phiNode = (PHINode *) I;
        		unsigned label = InstrToIndex[(Instruction *) (phiNode -> getIncomingBlock(i) -> getTerminator())];
        		Instruction *instr = dyn_cast<Instruction>(I -> getOperand(i));
        		if (instr) {
          			labelIndexMap[label].push_back(InstrToIndex[instr]);
        		}
      		}
      		Instr_check[I] = 1;
      		minusVector.push_back(InstrToIndex[I]);
    		}
    	Info *info = new Info();

    	for (auto it = IncomingEdges.begin(); it != IncomingEdges.end(); ++it) {
      		Edge edge(*it, instrIndex);
      		Info *edgeInfo = EdgeToInfo[edge];
      		Info::join(info, edgeInfo, info);
      		Info::joinIndex(info, edgeInfo, info);
    	}
    	Instr_check[I] = 1;
     	opcounter_map[instrIndex] = I;
     	sliceblock[name] = 1;

    	info -> minus(minusVector);

    	for (unsigned outIndex : OutgoingEdges) {
      		Info *newInfo = new Info(*info);
      		Info *addInfo = new Info(labelIndexMap[outIndex]);
      		Info::join(newInfo, addInfo, newInfo);
      		Infos.push_back(newInfo);
    	}
    delete info;
    return;
  }

  void skipping(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges, std::vector<Info *> & Infos) {
      std::map<Edge, Info*> EdgeToInfo = this -> getEdgeToInfo();
      std::map<Instruction*,unsigned>InstrToIndex = this -> getInstrToIndex();
    //  unsigned instrIndex = InstrToIndex[I];

      /*Combine all the variables in incoming Edges */
      Info *info = new Info();

       if(type_imp == 2){
            if(I->getOpcode()==Instruction::Call){
        	    if (DILocation *Loc = I->getDebugLoc()) { // Here I is an LLVM instruction
       				unsigned Line = Loc->getLine();       				
       				if (Line > start_line && exitflag != 1){
        				bool b1 = cast<CallInst>(I)->getCalledFunction()->isIntrinsic();
        				StringRef name = cast<CallInst>(I)->getCalledFunction()->getName();
        				if(b1==0){       
            					if(name != "printf"){
            						errs() << *I << "\n";
            						errs() << exitflag << "\n";
       							errs() << Line <<" : " << start_line << "\n";
            						int count = I -> getNumOperands() -1 ;
            						errs() << count << "\n";
              						// livenessAnalysis.runWorklistAlgorithm(&*Main,&*F);
              						CallInst *CI = dyn_cast<CallInst>(I);
          						Function *Fun = dyn_cast<Function>(CI->getOperand(count)); //last operand
							// ReturnInfo rinfo;
					        	this->runWorklistAlgorithm(&*Main,&*Fun);
            					}
          	  			}
          	  		}
        		}
      		}
      	}
      //Add the reaching info into the outgoing edges
      for (int i = 0; i < (int) OutgoingEdges.size(); i++) {
          Info *newInfo = new Info(*info);
          Infos.push_back(newInfo);
      }
      delete info;
    return;
   }
 void flowfunction(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges,std::vector<Info *> & Infos) {
    std::map<int,std::string> bname = this->getBlockname();
    std::map<Instruction*,unsigned>InstrToIndex = this -> getInstrToIndex();
    if(I == start_instr){
    	flag1 = 0;
    }
    else if(I == stop_instr){
    	flag1 = 1;
    	exitflag = 1;
    }
    if(flag1==0){
     unsigned opcode = I -> getOpcode();
     switch (opcode) {
      case Instruction::Add:
      case Instruction::FAdd:
      case Instruction::Sub:
      case Instruction::FSub:
      case Instruction::Mul:
      case Instruction::FMul:
      case Instruction::UDiv:
      case Instruction::SDiv:
      case Instruction::FDiv:
      case Instruction::URem:
      case Instruction::SRem:
      case Instruction::FRem:
      case Instruction::Shl:
      case Instruction::LShr:
      case Instruction::AShr:
      case Instruction::And:
      case Instruction::Or:
      case Instruction::Xor:
      case Instruction::Load:
      case Instruction::ICmp:
      case Instruction::FCmp:
      case Instruction::Select:
            category3(I, IncomingEdges, OutgoingEdges, Infos); 
        break;
      case Instruction::GetElementPtr:
      		getelement(I, IncomingEdges, OutgoingEdges, Infos); 
        break;
      case Instruction::Alloca:
      	    category2(I, IncomingEdges, OutgoingEdges, Infos);
      	 break;
      case Instruction::Br:
            category6(I, IncomingEdges, OutgoingEdges, Infos);
        break;
      case Instruction::Call:
        {
        bool b1 = cast<CallInst>(I)->getCalledFunction()->isIntrinsic();
        StringRef name = cast<CallInst>(I)->getCalledFunction()->getName();
        if(b1==0){       
            if(name != "printf"){
                category4(I, IncomingEdges, OutgoingEdges, Infos);
            }
            else{
                category5(I, IncomingEdges, OutgoingEdges, Infos);
            }
        }
        else{
          category5(I, IncomingEdges, OutgoingEdges, Infos);

        }
        break;
    	}
      case Instruction::Store:
      	   category1(I, IncomingEdges, OutgoingEdges , Infos); 
      	break;
      case Instruction::PHI:
      		category7(I, IncomingEdges, OutgoingEdges, Infos);
      	break;
      default :
           category3(I, IncomingEdges, OutgoingEdges, Infos); 
         break; 
    }
   }
   else{
     skipping(I, IncomingEdges, OutgoingEdges, Infos);
    
   }
  }
  	public:
    	LivenessAnalysis(Info & bottom, Info & initialState) : DataFlowAnalysis<Info, Direction>(bottom, initialState) {}
   		~LivenessAnalysis() {}
};

void printset(std::map<unsigned,std::set<Instruction*>> counter)
	{
     auto ii = counter.begin();
     auto ee = counter.end();
     while (ii != ee) {
       std::set<Instruction*>myset =  counter[ii->first];
       auto i1 =myset.begin();
       auto e1 =myset.end();
       errs() << "========= Line :" << ii->first << "================== \n";
       while (i1 != e1) {
          errs() << **i1 <<"\n";
          i1++;
       }
       ii++;
       errs() << "\n";
     }
    errs() << "\n";
	}
Instruction* findinstruction(unsigned line, std::map<unsigned,std::set<Instruction*>> counter)
	{  
	   Instruction * instr;
	   if(counter.find(line) != counter.end()){
	   	std::set<Instruction*>myset =  counter[line];
       		auto e1 =myset.end();
       		--e1;
       		// print the last instruction
       		errs() << "========= Line :" << line << "================== \n";
            	errs() << **e1 <<"\n";
            	instr = *e1;

	   }
	   else{
	   	errs() << "Sorry!, There is no instruction in this line : " << line << "\n";
	   	exit(0);
	   }
     return instr; 
   }

void mapOneFunction(Function *F) {
  for (inst_iterator I = inst_begin(F); I != inst_end(F); ++I) {
    	Instruction * instr = &*I;
    	if (DILocation *Loc = I->getDebugLoc()) { // Here I is an LLVM instruction
       		unsigned Line = Loc->getLine();
       		//Stores the Last Instructions of the Line
       		LineToInstr[Line] =  instr;
       		std::set<Instruction*> myset;
       		if(LineToInstrset.find(Line)!=LineToInstrset.end()){
          		myset = LineToInstrset[Line];
       		}
        	myset.insert(instr);
        	LineToInstrset[Line] = myset;
        	myset.clear();
      	}
   }
}

void mapOneBasicBlock(BasicBlock *B) {
  for (auto I = B->begin(); I != B->end(); ++I) {
    	Instruction * instr = &*I;
    	if (DILocation *Loc = I->getDebugLoc()) { // Here I is an LLVM instruction
       		unsigned Line = Loc->getLine();
       		//Stores the Last Instructions of the Line
       		LineToInstr[Line] =  instr;
       		std::set<Instruction*> myset;
       		if(LineToInstrset.find(Line)!=LineToInstrset.end()){
          		myset = LineToInstrset[Line];
       		}
        	myset.insert(instr);
        	LineToInstrset[Line] = myset;
        	myset.clear();
      	}
   }
}

size_t split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
    size_t pos = txt.find( ch );
    size_t initialPos = 0;
    strs.clear();

    // Decompose statement
    while( pos != std::string::npos ) {
        strs.push_back( txt.substr( initialPos, pos - initialPos ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, std::min( pos, txt.size() ) - initialPos + 1 ) );

    return strs.size();
}

// for aes main loops
void iterationcountFunction(Function *F) {
	for (Function::iterator bb = F->begin(), be = F->end(); bb != be; ++bb) {
  			std::string str = bb->getName().str();
  			std::vector<std::string> v;

  			 // errs() << str << " : " << "\n";

   			 split( str, v, '.');
   			 if(v.size()==2){
   			 	 if(str=="for.body"){
   			 	 	itercount[str] = 1;
   			 	 }
   			 	 else{
   			 	 	itercount[str] = 2;
   			 	 }
   			 }
   			 for(unsigned int i = 2 ; i < v.size() ; i++){
   			 	 std::string cons = v[i];
   			 	 std::string::const_iterator it = cons.begin();
   			 	 if(std::isdigit(*it)){
   			 	 		int content = std::stoi(v[i]) + 2;
   			 	 		itercount[str] = content;
   			 	 }	
   			 	
   			 }
			 v.clear();		
		 }
}

 
 //global array to store the size(assume only 2D array is used)
 unsigned array_value[3];
 unsigned level;
 
 //Find the array dimesion and the size
 void Array_Size(Type *T){
 	unsigned size;
 	size = cast<ArrayType>(T)->getNumElements();
    	level += 1;
    	array_value[level] = size;
    	array_value[0] = level;
    	if(cast<ArrayType>(T)->getElementType()->isArrayTy()){
    		Type *T1 = cast<ArrayType>(T)->getElementType();
    		Array_Size(T1);
    	}
 }

class LivenessAnalysisPass : public ModulePass {
  public:
    static char ID;
    LivenessAnalysisPass() : ModulePass(ID) {}
    bool runOnModule(Module &M){
    	Main = &M;

      //Print the Line sets in IR
      //  printset(LineToInstrset);
      	unsigned roll;
      //Pass 2 to find the instructions from the set
      //Read the start-line and the end-line of the slicing (changes based on the fault) - skip all the instructions before and after
      	errs() << "Enter your options (Rolled 1 and Unrolled 2) : ";
        std::cin >> roll;

       if(roll == 1){
       		//Pass 1 to find the Line Number from the C code
      		for (Module::iterator F = M.begin(); F != M.end(); ++F){
        		if (!F->isDeclaration()){
           			mapOneFunction(&*F);
        		}
      		}

        	errs() << "Start Line : ";
        	std::cin >> start_line;
        	errs() << "End Line : ";
        	std::cin >> stop_line;
        	errs() << "Type_imp: ";
        	std::cin >> type_imp; // 1 for lookup and 2 for t-table
        
        	start_instr = findinstruction(start_line,LineToInstrset);
        	stop_instr = findinstruction(stop_line,LineToInstrset);

        }
        else if(roll == 2){
       	 	errs() << "Function whose unroll count is given :";
       	 	std::cin >> fun_name;

       	 	errs() << "Enter the iteration count :\n";
         	errs() << "From i = ";
         	std::cin >> in1;
         	errs() << "To i = ";
         	std::cin >> in2;

         	errs() << "Start Line : ";
         	std::cin >> start_line;
         	errs() << "End Line : ";
         	std::cin >> stop_line;

         	//Finding the iteration count instructions
       	 	for (Module::iterator F = M.begin(); F != M.end(); ++F){
       	 		if(F->getName().str() == fun_name){
        			if (!F->isDeclaration()){
           				iterationcountFunction(&*F);
        			}
      	 		}
      	 		for(auto bb = F->begin(); bb != F->end(); ++bb){
      				std::string str = bb->getName().str(); 	
    				if(itercount.find(str) != itercount.end()){
    		 			auto it = itercount.find(str);
    		 			if(it->second == in1){
    		 				mapOneBasicBlock(&*bb);
    		 				start_instr = findinstruction(start_line,LineToInstrset);
    		 				errs() << "start :" << *start_instr << "\n";
    		 			}
    		 			if(it->second == in2){
    		 				mapOneBasicBlock(&*bb);
    		 				stop_instr = findinstruction(stop_line,LineToInstrset);
    		 				errs() << "end : " <<*stop_instr << "\n";
    		 			}
    		 		}

      	 		}
      		}
       }
      //Pass 3 to find the Slice from the Instruction obtained in rolled form
      for (Module::iterator F = M.begin(), E = M.end(); F != E; ++F) {
    		if (!F->isDeclaration()) {
    			 if(F->getName()=="main"){
    			 	ReachingInfo bottom, initialState;
    			 	LivenessAnalysis<ReachingInfo, false> livenessAnalysis(bottom, initialState);
    			 	livenessAnalysis.runWorklistAlgorithm(&*Main,&*F);
          		 for(auto it = opcounter_map.begin(); it != opcounter_map.end();it++){
            		 	temp_map.insert(*it);
           		}

          		slice_list.push_back(temp_map);
           		temp_map.clear();

    		  }
    	  }
    	}
    	  std::map<Instruction *, unsigned> InstrToIndex;
    	  unsigned counter = 1;
    	  unsigned vulnerable = 1;
    	  for (Module::iterator F = M.begin(), E = M.end(); F != E; ++F) {
    	  	InstrToIndex[nullptr] = 0;
    	  	Function *fun = &*F;
			for (inst_iterator I = inst_begin(fun), E = inst_end(fun); I != E; ++I) {
				Instruction * instr = &*I;
				InstrToIndex[instr] = counter;
				if(Instr_check.find(instr)!=Instr_check.end()){
					outs() << counter << " : 1 " << "\n";
					outs() << *instr << "\n";
					vulnerable++;
				}
				else{
					outs() << counter << " :  0" << "\n";
				}
				counter++;
			}
         }

        errs() << "========================Total number of Instructions==============================\n";
        errs() << "Number of Instruction : " << counter << "\n";
        errs() << "Vulnerable Instruction : " << vulnerable << "\n";

    return false;
    }
  };

char LivenessAnalysisPass::ID = 0;
static RegisterPass<LivenessAnalysisPass> X("liveness", "xxx", false, false);
