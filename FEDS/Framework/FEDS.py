#!/usr/bin/env python
import sys, getopt
import subprocess
import os
import os.path
from os import path
from subprocess import call 
import time
import emap as em
import Traceback as tb
import FaultMapping as fm
import IFG as ifg
import FEME_Identification as feme
import fault_evaluation as fe


def check_arguments(folder,code,unrolledcode):
    if not path.exists(folder):
		print folder," Folder is not present\n"
		sys.exit(0)
    if not path.exists(code):
		print code," is not present in Folder", folder,"\n"
		sys.exit(0)
    if not path.exists(unrolledcode):
    	print unrolledcode,"is not present in Folder",folder,"\n"
    	sys.exit(0)

####FEDS framework
if __name__ == '__main__':
	folder = sys.argv[1]
	code = folder + "/" + sys.argv[2]
	unrolledcode = folder + "/" + sys.argv[3]
	check_arguments(folder,code,unrolledcode)

	print "Synthesis the BCS to C code"
	print "Folder (Synthesis include the code)\n"	
	print "Generating Information Flow Graph"

	file1 = folder + "/bcs.c"
	print file1
	print code
	print unrolledcode

	type_imp = ""
	print "---- Details about the Implementation---\n --> 1. lookup-table Implementations (Two-dimension) \n --> 2. t-table Implementations (One-Dimension)\n --> 3. BitSliced Implementation \n --> 4. lookup-table (One-Dimensionnsion))\n"
	val = input("Enter your value: ") 
	if val == 1:
		type_imp = "lookup"
	elif val == 2:
		type_imp = "t-table"
	elif val == 3:
		 print "Assuming to be a BitSliced Implementation\n";
		 type_imp = "bitsliced"
	elif val == 4:
		 type_imp = "lookup2"

	min_sub,max_sub = tb.suboperation_lines(file1)

	print "Start and End of IFG : ", min_sub, "&" , max_sub
	ifg.information_graph(file1,min_sub,max_sub)

	os.system("cp info.txt info1.txt")

	fm.equivalence_mapping(12,10,32,100,code,file1,type_imp,folder)

	start = time.time() 
	print "Generate verification constraints"
	subprocess.call("./Mapping.sh")

	end = time.time()
	print "Fault Mapping Time :", (end - start)


	print "Generate E_Map"
	em.generate_emap()


	command = "mv E_Map.txt " + folder
	os.system(command)

	print "Traceback G values from IFG"
	tb.traceback(folder)

	os.system("rm info1.txt")
	os.system("rm final.txt")
	command = "mv info.txt " + folder
	os.system(command)

 	print "---------Equivalence Mapping completed Successfully---------"
	
	print "FEMA.txt (Folder HLE) contains the output of the HLE Tool"

	
	start = time.time()
	opt,range_exploit, feme_range = feme.feme_main(folder,type_imp)
	if opt == 1:
		print "Need to follow the range\n"
		print range_exploit
		range_list = ifg.determine_line(range_exploit,folder)
		for x in range_list:
			start_line = x[0]
			end_line = x[1]
			if type_imp == "lookup":
				fe.fault_evaluation(unrolledcode,start_line,end_line)
			elif type_imp == "lookup2":
				fe.fault_evaluation_oned(unrolledcode,start_line,end_line)
			print "Determining the exploitable instructions\n"
			subprocess.call("./evaluation.sh")
	else:
		print "Follow the feme expressions\n"
		for x in feme_range:
			start_line = x[0].replace("E","")
			end_line = x[1].replace("E","")
			fe.fault_evaluation_ttable(unrolledcode,start_line,end_line)
			print "Determining the exploitable instructions\n"
			subprocess.call("./evaluation.sh")


	end = time.time()
	print "Fault Evaluation Time :", (end - start)

	os.system("rm o0.ll")
	os.system("rm s0.ll")
	os.system("rm test.ll")
	os.system("rm fault-exploit.c")
	os.system("rm mapping_function.c")
	os.system("rm input.txt")

	print "---------Fault Evaluation completed Successfully ( outs contains the exploitable instructions )---------"

	print "---------completed FEDS Evaluation for",folder,"---------"

#######################################################################################

