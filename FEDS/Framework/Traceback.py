import sys, getopt
import array
import re
import os
import os.path
from os import path

check = []
def arguments(ifg_1,ifg_2):
    if not path.exists(ifg_1):
	print "Information Flow Graph is not present\n"
        sys.exit(0)
    if not path.exists(ifg_2):
	print "info1.txt is not present in the given folder\n"

#modify emap based on traceback output
def modify_emap(element,trace,tracelist,path3):

	f = open(path3, "a")

	print trace, element
	temp_trace = "G"+trace
	temp_element = "G"+str(element)
	with open(path3) as f1:
		for i, line in enumerate(f1):
		    line_str = line.strip()
		    check = line_str.find('-->')
		    if check != -1:
		    	word = line_str.split('-->')
		    	#print word[0], ","
		    	temp = str(word[0]).strip()
		    	if temp == temp_trace:
		    		temp_line = line
		    		add_line = temp_line.replace(temp_trace,temp_element)
		    		print temp_line
		    		print add_line
		    		f.write(add_line)
	f.close()

#Traceback for merged sub-operations
def traceback_info(path3,trace):
	f = open("traceback.txt", "w")

	ifg_1 = "info.txt"
	ifg_2 = "info1.txt"
	arguments(ifg_1,ifg_2)

	element = int(trace.replace("G",""))

	print "Traceback :", element
	t_start = element
	t_end = element

	boundary = -1
	
	temp_flag = 0
	t_list = []
	temp_list = []
	temp_list.append(-1)

	with open(ifg_1) as f1:
	    for i, line in enumerate(f1):
		    line_str = line.strip()
		    #print line_str
		    check = line_str.find('->')
		    if check != -1:
		    	str1 = line_str[:line_str.find('->')]
			if int(str1) >= t_end:
			    str2 = line_str[line_str.find('->')+2:]
			    #print >>f,line_str 
			    x = [m.start() for m in re.finditer(':', str2)]
			    len_x = len(x)
			    del(x[len_x-1]) 
			    for j in x : # Finding all the predecessor
	 		        new1 = str2[j+2:]
				new2 = new1[:new1.find(':')]
				t_list.append(int(new2))
			    t_list.sort()
		 	for r_line in reversed(list(open(ifg_2))):
			    line_rstr = r_line.rstrip()
			    check_r = line_rstr.find('->')
		    	    if check_r != -1:
			        str_r = line_rstr[:line_rstr.find('->')]
				for itr, e in enumerate(t_list):
				    if e == int(str_r) and e >= t_start:
					#print >> f,line_rstr 
					str_r2 = line_rstr[line_rstr.find('->')+2:]
		  		        x_r = [m.start() for m in re.finditer(':', str_r2)]
					len_xr = len(x_r)
			    		del(x_r[len_xr-1])
		     		        for j_r in x_r: # Finding all the predecessor
		  		            new_r1 = str_r2[j_r+2:]
		 			    new_r2 = new_r1[:new_r1.find(':')]
					    if int(new_r2) >= t_start:
					       t_list.append(int(new_r2))
				t_list.sort()
				t_list.reverse()
			#print >> f,t_list
			if t_end in t_list:# and boundary not in t_list:
				new = line_str.split('->')
				new[0] = new[0].strip()
				boundary = int(new[0])
				#print boundary, temp_list
				check =  any(item in t_list for item in temp_list)
				#print check
				if check is False:
					print "yes"
					temp_flag = 1
					print new[0],":", t_list
					temp_list.append(boundary)
					modify_emap(element,new[0],t_list,path3)
				elif temp_flag == 1:
					# stop iteration after find all occurance -- for full round
					break
				#print temp_list					 
		        del t_list[:]
			print >> f, "-----------------------------------------------------"		    
	f1.close();

def suboperation_lines(path1):
	min_sub = 0
	max_sub = 0
	with open(path1) as f1:
		for i, line in enumerate(f1):
			line_str = line.strip()
			check = line_str.find('] =')
			check1 = line_str.find('F')
			if check != -1 and check1 != -1:
				if min_sub == 0:
					min_sub = i+1
				else:
					max_sub = i+1
			else:
				if min_sub > 0 :
					break
	return min_sub,max_sub

def find_mapped(path1,path3):

	min_sub,max_sub = suboperation_lines(path1)
	print min_sub,max_sub

	prev_subop = min_sub
	mapped = [0] * 64 # change for merged
	unmapped = [] 

	with open(path3) as f1:
		for i, line in enumerate(f1):
		    line_str = line.strip()
		    check = line_str.find('-->')
		    if check != -1:
		    	word = line_str.split('-->')
		    	temp = str(word[0]).strip()
		    	index = (temp.replace("G",""))
		    	if int(index) == prev_subop:
		    		prev_subop = prev_subop + 1
		    	else:
		    		for i in range(prev_subop, int(index)):
		    			sub = "G"+str(i)
		    			print "Unmapped", sub
		    			unmapped.append(sub)
		    		prev_subop = int(index) + 1
    	return unmapped,min_sub,max_sub

def check_multple_occurance(path3,unmapped,min_sub,max_sub):
	maximum = int(unmapped[0].replace("G",""))
	minimum = int(unmapped[len(unmapped)-1].replace("G",""))
	print minimum, maximum
	start = 0
	end = 0
	for i in range(min_sub,max_sub,16):
		if minimum >= i and minimum < (i + 16):
			start = i
		elif maximum >= i and maximum < (i + 16):
			end = i+15
			break;

	print start,end 	

	temp_list = []
	file_list = []
	multiple = 0
	# open file in read mode 
	with open(path3) as f1:
		for i, line in enumerate(f1):
		    line_str = line.strip()
		    check = line_str.find('-->')
		    if check != -1:
		    	word = line_str.split('-->')
		    	temp = str(word[0]).strip()
		    	if temp not in temp_list:
		    		temp_list.append(temp)
		    		file_list.append(line_str)
		    	else:
		    		multiple += 1
		    else:
		    	file_list.append(line_str)
	print multiple
	f1.close()

	if multiple > 0:
		ft = open(path3, "w")
		for i in file_list:
			ft.write(i + "\n")	
		ft.close()
		 

def traceback(folder):
	path1 = folder+"/bcs.c"
	path3 = folder+"/E_Map.txt"

	unmapped,min_sub,max_sub = find_mapped(path1,path3)
	unmapped.sort(reverse=True) 
	if len(unmapped) == 0:
		print "All Sub-operations are Mapped\n";
	else:
		print unmapped
		for i in range(len(unmapped)):
			trace = unmapped[i];
			traceback_info(path3,trace)
		temp = folder+"/temp.txt"
		f = open(path3, "r")
		cont = f.readlines() 
		type(cont)
		cont = list(dict.fromkeys(cont)) 
		cont.sort()
		f.close()
		ft = open(path3, "w")
		for i in range(len(cont)):
			ft.write(cont[i])	
		#f.close()
		ft.close()
		check_multple_occurance(path3, unmapped,min_sub,max_sub)
