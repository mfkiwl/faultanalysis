import sys, getopt
import array
import re
import os.path
from os import path

#Generate expression for different implementations
f = open("fault-exploit.c", "w")

def input(index1,index2,imp_type):
	f1 = open("input.txt","w")
	f1.write("1\n")
	f1.write(str(index2)+"\n")
	f1.write(str(index1)+"\n")
	f1.write(str(imp_type)+"\n")
	f1.close()

def take_expressions(start,end,path1):

	# open file in read mode 
	fn = open(path1, 'r') 
  
	# read the content of the file line by line 
	print "Generate C file for analysis\n"
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)):
		#print i, cont[i] 
		if i == start or i == end:
			indexi = int(sys.argv[1])
			indexj = int(sys.argv[2])
			f.write('\tprintf("%x\\n"' + ",state[" + str(indexi) + "][" + str(indexj) + "]);\n")
		f.write(cont[i]) 
	# close the file 
	fn.close()

def take_expressions_oned(start,end,path1):

	# open file in read mode 
	fn = open(path1, 'r') 
  
	# read the content of the file line by line 
	print "Generate C file for analysis\n"
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)):
		if i == start or i == end:
			indexi = int(sys.argv[2])
			f.write('\tprintf("%x\\n"' + ",state[" + str(indexi) + "]);\n")
		f.write(cont[i]) 
	# close the file 
	fn.close()


def take_expressions_ttable(start,end,path1):

	# open file in read mode 
	fn = open(path1, 'r') 
  
	# read the content of the file line by line 
	print "Generate C file for analysis\n"
	cont = fn.readlines() 
	type(cont) 
	for i in range(0, len(cont)):
		if i == start or i == end:
			indexi = int(sys.argv[1])
			f.write('\tprintf("%x\\n"' + ",u" + str(indexi) + ");\n")
		f.write(cont[i]) 
	# close the file 
	fn.close()

if __name__ == '__main__':
	path1=sys.argv[3]
	start = int(sys.argv[4]) - 1 # check of function defined
	end = int(sys.argv[5])
	imp_type = int(sys.argv[6])

	if imp_type == 2:
		take_expressions_ttable(start,end,path1)
	elif imp_type == 1:
		take_expressions(start,end,path1)
	elif imp_type == 4:
		take_expressions_oned(start, end, path1)

	index1 = start + 1
	index2 = end + 2

	input(index1,index2,imp_type)
	f.close()
